export const AuthConfig = () => ({
    jwksUrl: process.env.KEYCLOAK_JWKS_URL,
    clientId: process.env.KEYCLOAK_CLIENT_ID,
    clientSecret: process.env.KEYCLOAK_CLIENT_SECRET,
    adminUser: process.env.KEYCLOAK_ADMIN_USER,
    adminPassword: process.env.KEYCLOAK_ADMIN_PASSWORD,
    realmName: process.env.KEYCLOAK_REALM_NAME,
    baseUrl: process.env.KEYCLOAK_BASE_URL
})

export const MailConfig = () => ({
    mail: {
        host: process.env.MAIL_HOST,
        port: Number(process.env.MAIL_PORT),
        secure: Boolean(process.env.MAIL_SECURE),
        user: process.env.MAIL_USER,
        password: process.env.MAIL_PASSWORD
    }
})

export const DatabaseConfig = () => ({
    database: {
        type: process.env.DB_TYPE,
        host: process.env.DB_HOST,
        port: Number(process.env.DB_PORT),
        username: process.env.DB_USER,
        password: process.env.DB_PASS,
        name: process.env.DB_NAME
    }
})

export const HashConfig = () => ({
    hash: {
        key: process.env.HASH_KEY
    }
})

export const SystemConfig = () => ({
    account: {
        minimum: Number(process.env.ACCOUNT_MINIMUM_VALUE),
        sequenceName: process.env.ACCOUNT_SEQUENCE_NAME
    },
    customer: {
        sequenceName: process.env.CUSTOMER_SEQUENCE_NAME
    },
    branch: {
        clientId: process.env.BRANCH_CLIENT_ID
    }
})