import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CustomerModule } from './customers/customer.module'
import { BranchModule } from './branch/branch.module'
import { AccountModule } from './account/account.module'
import { TransactionModule } from './transaction/transaction.module';
import { Customer } from './customers/entities/customer.entity';
import { Account } from './account/entities/account.entity';
import { RuleModule } from './rule/rule.module';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { AuthConfig, MailConfig, DatabaseConfig, HashConfig, SystemConfig } from './config/configuration';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      useFactory: (configService: ConfigService) => ({
        type: 'postgres',
        host: configService.get<string>('database.host'),
        port: configService.get<number>('database.port'),
        username: configService.get<string>('database.username'),
        password: configService.get<string>('database.password'),
        database: configService.get<string>('database.name'),
        entities: [Customer, Account],
        synchronize: true,
        autoLoadEntities: true
      }),
      inject: [ConfigService]
    }),
    ConfigModule.forRoot({
      isGlobal: true,
      load: [AuthConfig, MailConfig, DatabaseConfig, HashConfig, SystemConfig]
    }),
    CustomerModule,
    BranchModule,
    AccountModule,
    TransactionModule.forRoot(),
    RuleModule,
    AuthModule,
    UserModule
  ],
  controllers: [AppController],
  providers: [
    AppService
  ],
})
export class AppModule {}
