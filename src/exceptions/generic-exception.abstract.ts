export abstract class GenericException {
    private message: string;

    constructor(message: string) {
        this.message = message;
    }

    abstract getErrorCode(): string;
    abstract getDisplayCode(): string;

    public getMessage(): string {
        return this.message;
    }
}