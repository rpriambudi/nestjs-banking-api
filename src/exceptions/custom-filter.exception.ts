import { ExceptionFilter, ArgumentsHost, Catch } from '@nestjs/common';
import { GenericException } from './generic-exception.abstract';
import { ExceptionCode } from './code.exception';

@Catch()
export class CustomFilterException implements ExceptionFilter {
    catch(exception: any, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();

        if (exception instanceof GenericException) {
            const statusCode = ExceptionCode.code[exception.getErrorCode()];
            if (!statusCode) {
                console.log(exception)
                response.status(500).json({errorCode: 'UNKNOWN_EXCEPTION', message: 'Unknown Exception'});
                return;
            }

            response.status(statusCode).json({errorCode: exception.getDisplayCode(), message: exception.getMessage()});
            return;
        }

        response
            .status(500)
            .json({
                statusCode: 500,
                message: exception.message
            })
    }
    
}