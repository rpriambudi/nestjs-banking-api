export class ExceptionCode {
    public static code = {
        "400101": 400,
        "409201": 409,
        "400202": 400,
        "400203": 400,
        "400204": 400,
        "400301": 400,
        "400302": 400,
        "400303": 400,
        "400304": 400,
        "400305": 400,
        "400306": 400,
        "400307": 400,
        "400901": 400,
        "400902": 400,
        "400903": 400,
        "401991": 401,
        "401992": 401,
        "403993": 403,
        "400994": 400,
        "400995": 400,
        "400996": 409
    }
}