import { createParamDecorator } from '@nestjs/common'

export const AuthContext = createParamDecorator((attrribute, request): UserContext => {
    const verifiedObj = request['verifiedObj'];
    return new UserContext(verifiedObj['name'], verifiedObj['email'], verifiedObj['branch_code'], verifiedObj['cifNo']);
});

export class UserContext {
    public name: string;
    public email: string;
    public branchCode: string;
    public cifNo: string;

    constructor(name: string, email:string, branchCode: string, cifNo: string) {
        this.name = name;
        this.email = email;
        this.branchCode = branchCode;
        this.cifNo = cifNo;
    }
}