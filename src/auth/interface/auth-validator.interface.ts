export interface AuthValidator {
    validate(token: string): Promise<object>;
}