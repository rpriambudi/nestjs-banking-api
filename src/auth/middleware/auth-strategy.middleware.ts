import { NestMiddleware, Inject } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import * as rp from 'request-promise';
import { JWKS, JWT } from 'jose';
import { UserContext } from './../decorators/user-context.decorator';
import { InvalidTokenException } from './../exceptions/invalid-token.exception';
import { InvalidRoleException } from './../exceptions/invalid-role.exception';
import { TokenNotFoundException } from './../exceptions/token-not-found.exception';
import { InvalidStrategyException } from './../exceptions/invalid-strategy.exception';

export abstract class AuthStrategyMiddleware implements NestMiddleware {
    private configService: ConfigService;

    constructor(
        @Inject('ConfigService') configService: ConfigService
    ) {
        this.configService = configService;
    }

    abstract getStrategyName(): string;

    abstract getGuards(): string[];

    async use(req: any, res: any, next: () => void) {
        const strategyName = this.getStrategyName();
        const guards = this.getGuards();

        const strategyFunction = this[`run${strategyName}Strategy`]
        if (typeof strategyFunction !== 'function')
            throw new InvalidStrategyException('Selected strategy is invalid');
            
        req['verifiedObj'] = await this[`run${strategyName}Strategy`].apply(this, [req, guards]);
        next();
    }

    private async runJWTStrategy(request: any, guards: string[]) {
        const authorization = request.headers['authorization'];
        if (!authorization)
            throw new TokenNotFoundException('Token not provided.')
            
        let token = authorization.split(' ');
        token = token[1];

        const jwksUrl = this.configService.get<string>('jwksUrl');
        const clientId = this.configService.get<string>('clientId');
        const options = {
            method: 'GET',
            uri: jwksUrl
        }
        const result = await rp(options);
        const keystore = JWKS.asKeyStore(JSON.parse(result));
        let verify: any;
        try {
            verify = JWT.verify(token, keystore);
        } catch (error) {
            throw new InvalidTokenException(error.message);
        }

        const clientData = verify['resource_access'][clientId];
        if (!clientData)
            throw new InvalidRoleException('Insufficient access.');

        const roles: [] = verify['resource_access'][clientId]['roles'];
        if (!roles || roles.length === 0)
            throw new InvalidRoleException('Role not found.');

        let find = false;
        let supportedRole = {}
        for (const guard of guards) {
            supportedRole[guard] = true;
        }

        for (const role of roles) {
            if (supportedRole[role]) {
                find = true;
                break;
            }
        }

        if (!find)
            throw new InvalidRoleException('Insufficient access');

        return verify;
    }
}

export class MockAuthMiddleware extends AuthStrategyMiddleware {
    getStrategyName(): string {
        return 'Mock';
    }    
    
    getGuards(): string[] {
        return [];
    }

    runMockStrategy(req: any, guards: string[]) {
        const mockUserContext: any = {
            name: 'Test Branch',
            email: 'test@mail.co',
            branch_code: '001'
        }
        return mockUserContext;
    }
}