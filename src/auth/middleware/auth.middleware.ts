import { NestMiddleware, Injectable, Inject } from '@nestjs/common';
import { TokenNotFoundException } from './../exceptions/token-not-found.exception';
import { AuthValidator } from './../interface/auth-validator.interface';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
    constructor (
        @Inject('AuthValidation') private readonly authValidation: AuthValidator
    ) {}

    async use(req: any, res: any, next: () => void) {
        const authorization = req.headers['authorization'];
        if (!authorization)
            throw new TokenNotFoundException('Token not provided.')
            
        let token = authorization.split(' ');
        token = token[1];
        
        req['verifiedObj'] = await this.authValidation.validate(token);
        next();
    }
}