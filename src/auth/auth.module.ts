import { Module } from '@nestjs/common';
import { AuthValidatorImpl } from './service/auth-validator.service';

const Keycloak = require('keycloak-connect');
const authFactory = {
    provide: 'Keycloak',
    useFactory: () => {
        const keycloakConfig = {
            clientId: 'nest-bank-api',
            secret: '163ca589-c68d-43c9-b5c6-f694f074af5d',
            bearerOnly: true,
            serverUrl: 'http://192.168.99.100:8080/auth',
            realm: 'nest-bank-api',
            realmPublicKey: 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArl/ftypMmWagDAGna0uOwnPmMsFAXn1mKEPbljRA0msbxbGnwJiCjcrwrUH9CU5/ruJC7S8Br3AafbIZSMGoulZESqKvx3+CPwoygmRp7/XceHaWHlzm3eYtSeE4xwwz70mNllmJVdF4hKs9o9wa9QgA+ECwH5pxBzhR4Zu/k3hsZZeYHsZCicthm5RVJy5khDN1vXybaw/P9qBRZgSl7XdFb7wKsRN+c36dOndgKLKcIkM4yMTdRVvMDxLEUlc33F8kEJmqyt8zxYSMLK01bMDV0Mj3cEbUwktetNkHb2uGs/hFCYPXtLVb/un4Qw+M5YYvVQ5x4ao8ANiz/P6mAwIDAQAB'
        };
        const keycloak = new Keycloak({}, keycloakConfig);
        return keycloak;
    },
}

@Module({
    imports: [AuthValidatorImpl],
    controllers: [],
    providers: [
        authFactory,

    ],
    exports: [authFactory, AuthValidatorImpl]
})

export class AuthModule {}