import { ConfigService } from '@nestjs/config';
import * as rp from 'request-promise';
import { JWKS, JWT } from 'jose';
import { AuthValidator } from './../interface/auth-validator.interface';
import { InvalidTokenException } from './../exceptions/invalid-token.exception';
import { InvalidRoleException } from './../exceptions/invalid-role.exception';

export class AuthValidatorImpl implements AuthValidator {
    private role: object;
    private configService: ConfigService;

    constructor(
        configService: ConfigService,
        roles: string[]
    ) {
        this.configService = configService;
        this.role = {};
        if (roles) {
            for (const roleString of roles) {
                this.role[roleString] = true;
            }
        }
    }

    public async validate(token: string): Promise<object> {
        const jwksUrl = this.configService.get<string>('jwksUrl');
        const clientId = this.configService.get<string>('clientId');
        const options = {
            method: 'GET',
            uri: jwksUrl
        }
        const result = await rp(options);
        const keystore = JWKS.asKeyStore(JSON.parse(result));
        try {
            const verify = JWT.verify(token, keystore);
            const roles: [] = verify['resource_access'][clientId]['roles'];
            if (!roles || roles.length === 0)
                throw new InvalidRoleException('Role not found.');

            let find = false;
            for (const role of roles) {
                if (this.role[role]) {
                    find = true;
                    break;
                }
            }

            if (!find)
                throw new InvalidRoleException('Insufficient access');

            return verify;
        } catch (error) {
            throw new InvalidTokenException(error.message);
        }
    }
}