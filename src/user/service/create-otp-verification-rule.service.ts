import { BusinessRule } from '../interface/business-rule.interface';
import { CreateVerificationDto } from '../dto/create-verification.dto';
import { VerificationTypeEnum } from '../enums/verification-type.enum';
import { InvalidVerificationFormat } from '../exceptions/invalid-verification-format.exception';

export class CreateOtpVerificationRule implements BusinessRule {
    validate(validationData: CreateVerificationDto): Promise<void> {
        return new Promise((resolve) => {
            if (
                validationData.verificationType === VerificationTypeEnum.OTP &&
                (validationData.verificationValue.length < 12 || validationData.verificationValue.length > 12)
            )
                throw new InvalidVerificationFormat('Phone number format not valid.');

            resolve();
        });
    }
}
