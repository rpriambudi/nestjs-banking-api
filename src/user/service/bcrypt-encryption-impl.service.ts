import { ClientVerification } from './../entities/client-verification.entity';
import { EncryptionService } from './../interface/encryption-service.interface';
const bcrypt = require('bcrypt');

export class BcryptEncryptionServiceImpl implements EncryptionService {
    compare(data: string, clientVerification: ClientVerification): boolean {
        const comparison = bcrypt.compareSync(data, clientVerification.verificationValue);
        return comparison;
    }

    encode(data: string): string {
        const encodedData = bcrypt.hashSync(data, 10);
        return encodedData;
    }

    decode(data: string): string {
        throw new Error('Not implemented');
    }
}