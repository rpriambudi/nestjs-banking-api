import { BusinessRule } from '../interface/business-rule.interface';
import { CreateVerificationDto } from '../dto/create-verification.dto';
import { VerificationTypeEnum } from '../enums/verification-type.enum';
import { InvalidVerificationFormat } from '../exceptions/invalid-verification-format.exception';

export class CreatePinVerificationRule implements BusinessRule {
    validate(validationData: CreateVerificationDto): Promise<void> {
        return new Promise((resolve) => {
            if (
                validationData.verificationType === VerificationTypeEnum.PIN &&
                (validationData.verificationValue.length < 4 || validationData.verificationValue.length > 4)
            )
                throw new InvalidVerificationFormat('PIN must be 4 digit long.');

            resolve();
        });
    }
}
