import { ConfigService } from '@nestjs/config';
import * as nodemailer from 'nodemailer';

export class MailSenderService {
    constructor(
        private readonly configService: ConfigService
    ) {}

    async sendMail(sendMailDto: SendMailDto) {
        const mailConfig = {
            host: this.configService.get<string>('mail.host'),
            port: this.configService.get<number>('mail.port'),
            auth: {
                user: this.configService.get<string>('mail.user'),
                pass: this.configService.get<string>("mail.password")
            }
        }
        
        const transporter = nodemailer.createTransport(mailConfig);
        await transporter.sendMail({
            to: sendMailDto.recipient,
            subject: sendMailDto.subject,
            text: sendMailDto.body
        })
    }
}

export class SendMailDto {
    recipient: string;
    subject: string;
    body: string;
}