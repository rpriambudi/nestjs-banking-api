import { Test } from '@nestjs/testing';
import { EncryptionService } from './../interface/encryption-service.interface';
import { AesEncryptionServiceImpl } from './../service/aes-encryption-impl.service';
import { ConfigService } from '@nestjs/config';

describe('ClientVerificationServiceImpl', () => {
    let aesEncryptionService: EncryptionService;
    let configService: ConfigService;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            providers: [
                ConfigService,
                {
                    provide: 'AesEncryptionService',
                    useFactory: (configService) => {
                        return new AesEncryptionServiceImpl(configService);
                    },
                    inject: [ConfigService]
                }
            ]
        }).compile();

        aesEncryptionService = moduleRef.get<EncryptionService>('AesEncryptionService');
        configService = moduleRef.get<ConfigService>(ConfigService);
    })

    describe('encode method', () => {
        it('Positive test, Should return encoded string', async () => {
            jest.spyOn(configService, 'get').mockReturnValueOnce('bVixomrVCLqYSNhuMdd0B7qpjOPcHWaNbdKZIHrXDAI');
            const encoded = aesEncryptionService.encode('test');
            expect(typeof encoded === 'string').toBe(true);
        })
    })

    describe('decode method', () => {
        it('Positive test, Should return original string value', async () => {
            jest.spyOn(configService, 'get').mockReturnValue('bVixomrVCLqYSNhuMdd0B7qpjOPcHWaNbdKZIHrXDAI');
            const encoded: string = 'eyJpdiI6IjgzOEhzTE9mVk1BSnZTVkZjTUx0OFE9PSIsImRhdGEiOiJtWXdQd05ZL3dtdzE0aExoTURWUm1BPT0ifQ==';
            const decoded = aesEncryptionService.decode(encoded);
            expect(decoded).toEqual('test');
        })
    })
});