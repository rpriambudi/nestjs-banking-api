import KeycloakAdminClient from 'keycloak-admin';
import { ConfigService } from '@nestjs/config';
import { v4 as uuid } from 'uuid';
import { MailSenderService, SendMailDto } from './mail-sender.service';
import { CreateUserDto } from './../dto/create-user.dto';
import { RegisterNewUserDto} from './../dto/register-new-user.dto';
import { KeycloakException } from './../exceptions/keycloak-exception';

export class KeycloakAdminService {
    public keycloakAdmin: KeycloakAdminClient;

    constructor (
        private readonly configService: ConfigService,
        private readonly sendMailService: MailSenderService,
    ) {
        this.keycloakAdmin = new KeycloakAdminClient({
            baseUrl: this.configService.get<string>('baseUrl'),
            realmName: this.configService.get<string>('realmName')
        });
    }

    async registerNewUserByClient(registerNewUserDto: RegisterNewUserDto): Promise<{id: string}> {
        try {
            await this.clientAuth();
            return await this.createKeycloakUserByClient(registerNewUserDto);
        } catch(error) {
            if (error.response)
                throw new KeycloakException(error.response.data.errorMessage);

            throw error;
        }
    }

    private async clientAuth(): Promise<void> {
        await this.keycloakAdmin.auth({
            username: this.configService.get<string>('adminUser'),
            password: this.configService.get<string>('adminPassword'),
            grantType: 'password',
            clientId: this.configService.get<string>('clientId'),
            clientSecret: this.configService.get<string>('clientSecret')
        });
    }

    private async createKeycloakUserByClient(registerNewUserDto: RegisterNewUserDto): Promise<{id: string}> {
        const user = await this.keycloakAdmin.users.create({
            username: registerNewUserDto.username,
            email: registerNewUserDto.username,
            enabled: true,
            attributes: {
                userClientId: registerNewUserDto.id
            }
        })

        await this.setKeycloakUserRole(user.id);
        
        if (!registerNewUserDto.password) {
            await this.setKeycloakUserPassword(user.id, registerNewUserDto.username);
        } else {
            await this.keycloakAdmin.users.resetPassword({
                id: user.id,
                credential: {
                    temporary: false,
                    type: 'password',
                    value: registerNewUserDto.password
                }
            });
        }
        return user;
    }

    private async setKeycloakUserRole(id: string): Promise<void> {
        const keycloakClient = await this.keycloakAdmin.clients.find({clientId: this.configService.get<string>('clientId')});
        const keycloakRole = await this.keycloakAdmin.clients.findRole({
            id: keycloakClient[0].id,
            roleName: 'Customer'
        });

        await this.keycloakAdmin.users.addClientRoleMappings({
            id: id,
            clientUniqueId: keycloakClient[0].id,
            roles: [
                {
                    id: keycloakRole.id,
                    name: 'Customer'
                }
            ]
        });
    }

    private async setKeycloakUserPassword(id: string, email: string) {
        const password = uuid();
        await this.keycloakAdmin.users.resetPassword({
            id: id,
            credential: {
                temporary: true,
                type: 'password',
                value: password
            }
        })

        const sendMailDto = new SendMailDto();
        sendMailDto.subject = 'Account Registration for Nest Bank';
        sendMailDto.recipient = email;
        sendMailDto.body = `Hi,\nThis is your account information for initial login.\nusername: ${email}\npassword: ${password}`;
        await this.sendMailService.sendMail(sendMailDto);
    }

}