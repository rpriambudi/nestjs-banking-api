import { Repository } from 'typeorm';
import { ClientVerification } from './../entities/client-verification.entity';
import { VerificationTypeEnum } from './../enums/verification-type.enum';
import { CreateVerificationDto } from './../dto/create-verification.dto';
import { EncryptionService } from './../interface/encryption-service.interface';
import { ClientVerificationService } from './../interface/client-verification-service.interface';

export class ClientVerificationServiceImpl implements ClientVerificationService {
    constructor(
        private readonly clientVerificationRepository: Repository<ClientVerification>,
        private readonly encryptionService: EncryptionService
    ) {}
    async createNewClientVerification(createVerificationDto: CreateVerificationDto): Promise<ClientVerification> {
        let clientVerification = this.clientVerificationRepository.create(createVerificationDto);
        if (clientVerification.verificationType === VerificationTypeEnum.PIN) {
            clientVerification.verificationValue = this.encryptionService.encode(clientVerification.verificationValue)
        }
        
        await this.clientVerificationRepository.save(clientVerification);
        return clientVerification;
    }
}