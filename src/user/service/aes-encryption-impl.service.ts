import { ConfigService } from '@nestjs/config';
import * as crypto from 'crypto';
import { ClientVerification } from './../entities/client-verification.entity';
import { EncryptionService } from './../interface/encryption-service.interface';

export class AesEncryptionServiceImpl implements EncryptionService {
    constructor(
        private readonly configService: ConfigService
    ) {}
    encode(data: string): string {
        const iv = crypto.randomBytes(16);
        const configKey = this.configService.get<string>('hash.key');
        const cipher = crypto.createCipheriv("aes-256-cbc", Buffer.from(configKey, 'base64'), iv);
        let encoded = cipher.update(data, 'utf8', 'base64');
        encoded += cipher.final('base64');
        
        const encodeJson = {
            iv: iv.toString('base64'),
            data: encoded
        }
        const base64Json = Buffer.from(JSON.stringify(encodeJson));
        return base64Json.toString('base64');
    }    
    
    decode(data: string): string {
        const decodeBuffer = Buffer.from(data, 'base64');
        const decodeString = decodeBuffer.toString('utf8');
        const decodeObject: {iv: string, data: string}= JSON.parse(decodeString);
        const configKey = this.configService.get<string>('hash.key');
        const decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(configKey, 'base64'), Buffer.from(decodeObject['iv'], 'base64'));

        let decoded = decipher.update(decodeObject.data, 'base64', 'utf8');
        decoded += decipher.final('utf8');
        return decoded;
    }

    compare(data: string, clientVerification: ClientVerification): boolean {
        throw new Error("Method not implemented.");
    }
    
}