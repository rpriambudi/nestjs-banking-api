import { ClientVerification } from './../entities/client-verification.entity';
import { VerificationTypeEnum } from './../enums/verification-type.enum';
import { ValidateVerificationDto } from './../dto/validate-verification.dto';
import { BusinessRule } from './../interface/business-rule.interface';
import { InvalidVerificationException } from './../exceptions/invalid-verification.exception';
import { EncryptionService } from './../../user/interface/encryption-service.interface';

export class ValidatePinVerificationRule implements BusinessRule {
    constructor(
        private readonly encryptionService: EncryptionService
    ) {}
    async validate(validationData: ValidateVerificationDto): Promise<void> {
        const verification: ClientVerification = validationData.clientVerification;
        if (verification.verificationType !== VerificationTypeEnum.PIN)
            return;

        if (!validationData.challange)
            throw new InvalidVerificationException('Challenge not provided');

        const decodedString = this.encryptionService.decode(verification.verificationValue);
        if (validationData.challange !== decodedString)
            throw new InvalidVerificationException('Verification challenge not match with existing data.')

        return;
    }
    
}