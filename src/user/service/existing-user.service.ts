import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserExistingDto } from './../dto/user-existing.dto';
import { UserClient } from './../entities/user-client.entity';
import { BusinessRule } from './../interface/business-rule.interface';
import { UniqueClientException } from './../exceptions/unique-client.exception';

export class ExistingUserService implements BusinessRule {
    constructor(
        @InjectRepository(UserClient) private readonly userClientRepository: Repository<UserClient>
    ) {}
    async validate(userExistingDto: UserExistingDto): Promise<void> {
        const userClient = await this.userClientRepository.findOne({customerId: userExistingDto.customerId, clientType: userExistingDto.clientType});
        if (userClient)
            throw new UniqueClientException('Client already exists.');
    }
    
}