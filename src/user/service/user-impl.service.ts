import { Repository } from 'typeorm';
import { Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserClient } from './../entities/user-client.entity';
import { RuleContext } from './../context/rule.context';
import { CreateUserDto } from './../dto/create-user.dto';
import { CreateVerificationDto } from './../dto/create-verification.dto';
import { UserExistingDto } from './../dto/user-existing.dto';
import { RegisterNewUserDto } from './../dto/register-new-user.dto';
import { ClientTypeEnum } from './../enums/client-type.enum';
import { CredentialTypeEnum } from './../enums/credential-type.enum';
import { ClientStatusEnum } from './../enums/client-status.enum';
import { UserService } from './../interface/user-service.interface';
import { ClientVerificationService } from './../interface/client-verification-service.interface';
import { KeycloakAdminService } from './keycloak-admin.service';
import { UserClientNotRegisteredException } from './../exceptions/user-client-not-registered.exception';
import { CustomerQueryService } from './../../customers/interfaces/customer-query-service.interface';

export class UserServiceImpl implements UserService {
    constructor(
        @InjectRepository(UserClient) private readonly userClientRepository: Repository<UserClient>,
        @Inject('CustomerQueryService') private readonly customerQueryService: CustomerQueryService,
        @Inject('ClientVerificationService') private readonly clientVerificationService: ClientVerificationService,
        @Inject('KeycloakAdminService') private readonly keycloakAdminService: KeycloakAdminService,
        @Inject('IsClientExistRule') private readonly isClientExistRule: RuleContext,
        @Inject('NewClientVerificationRule') private readonly newClientVerificationRule: RuleContext,
        @Inject('IsCredentialExistsValidation') private readonly isCredentialExistsValidation: RuleContext
    ) {}

    async registerNewUser(createUserDto: CreateUserDto): Promise<UserClient> {
        await this.newClientVerificationRule.validate(createUserDto.clientVerification);
        
        const customer = await this.customerQueryService.findCustomerByCifNo(createUserDto.cifNo);
        const registerNewUserDto: RegisterNewUserDto = new RegisterNewUserDto();
        registerNewUserDto.credentialType = CredentialTypeEnum.Email;
        registerNewUserDto.username = createUserDto.email;

        const userClient = await this.registerNewUserByClient(registerNewUserDto, customer.id, createUserDto.registeredBy);

        if (createUserDto.clientVerification) {
            const clientVerification = createUserDto.clientVerification;
            
            clientVerification.userClient = userClient;
            await this.clientVerificationService.createNewClientVerification(clientVerification);
        }
        return userClient;
    }

    async registerUserByNewCustomer(registerNewUserDto: RegisterNewUserDto): Promise<UserClient> {
        return await this.registerNewUserByClient(registerNewUserDto);
    }

    async registerUserByExistingCustomer(registerNewUserDto: RegisterNewUserDto, customerId: number): Promise<UserClient> {
        return await this.registerNewUserByClient(registerNewUserDto, customerId);
    }

    async registerNewUserClientVerification(cifNo: string, email: string, createVerificationDto: CreateVerificationDto): Promise<UserClient> {
        const customer = await this.customerQueryService.findCustomerByCifNo(cifNo);
        const userClient = await this.userClientRepository.findOne({where: {customerId: customer.id, credentialValue: email}});
        if (!userClient)
            throw new UserClientNotRegisteredException('There is no registered client.')

        createVerificationDto.userClient = userClient;
        await this.clientVerificationService.createNewClientVerification(createVerificationDto);
        return userClient;
    }

    private async registerNewUserByClient(registerNewUserDto: RegisterNewUserDto, customerId?: number, registeredBy?: string): Promise<UserClient> {
        await this.isCredentialExistsValidation.validate(registerNewUserDto);

        const userClient = this.userClientRepository.create();
        if (customerId)
            userClient.customerId = customerId;

        userClient.clientType = ClientTypeEnum.InternetBanking;
        userClient.credentialType = registerNewUserDto.credentialType;
        userClient.credentialValue = registerNewUserDto.username;
        userClient.registeredBy = registeredBy ? registeredBy : ClientTypeEnum.InternetBanking;
        userClient.clientStatus = ClientStatusEnum.Live;
        await this.userClientRepository.save(userClient);

        registerNewUserDto.id = userClient.id;
        const keycloakUser = await this.keycloakAdminService.registerNewUserByClient(registerNewUserDto);

        userClient.credentialId = keycloakUser.id;

        await this.userClientRepository.save(userClient);
        return userClient;
    }
}