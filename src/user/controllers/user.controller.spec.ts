import * as request from 'supertest';
import { Repository } from 'typeorm';
import KeycloakAdminClient from 'keycloak-admin';
import * as nodemailer from 'nodemailer';
import { Test } from '@nestjs/testing';
import {INestApplication, MiddlewareConsumer } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { getRepositoryToken, TypeOrmModule } from '@nestjs/typeorm';
import { CreateUserDto } from './../dto/create-user.dto';
import { ClientStatusEnum } from './../enums/client-status.enum';
import { UserClient } from './../entities/user-client.entity';
import { ClientVerification } from './../entities/client-verification.entity';
import { RegistrationState } from './../entities/sagas/registration-state.entity';
import { UserController } from './user.controller';
import { UserModule } from './../user.module';
import { MockAuthMiddleware } from './../../auth/middleware/auth-strategy.middleware';
import { CustomFilterException } from './../../exceptions/custom-filter.exception';
import { Customer } from './../../customers/entities/customer.entity';
import { IdTypeEnum } from './../../customers/enums/idtype.enum';
import { GenderEnum } from './../../customers/enums/gender.enum';
import { StatusEnum as CustomerStatusEnum } from './../../customers/enums/status.enum';
import { CustomerRepository } from './../../customers/repositories/customer.repository';
import { CustomerModule } from './../../customers/customer.module';
import { Branch } from './../../branch/entities/branch.entity';
import { BranchModule } from './../../branch/branch.module';
import { Account } from './../../account/entities/account.entity';
import { HashConfig } from './../../config/configuration';
jest.mock('keycloak-admin');
jest.mock('nodemailer');

export class MockUserModule extends UserModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(MockAuthMiddleware)
            .forRoutes(UserController)
    }
}


describe('UserController class', () => {
    let app: INestApplication;
    let userClientRepository: Repository<UserClient>;
    let customerRepository: CustomerRepository;
    let keycloakClient: KeycloakAdminClient = new KeycloakAdminClient();

    beforeAll(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [
                TypeOrmModule.forRoot({
                    type: 'postgres',
                    host: '192.168.99.100',
                    port: 5432,
                    username: 'postgres',
                    password: 'postgres',
                    database: 'e2e_test',
                    dropSchema: true,
                    entities: [UserClient, ClientVerification, Customer, Branch, Account, RegistrationState],
                    synchronize: true,
                    logging: false
                }),
                TypeOrmModule.forFeature([CustomerRepository, UserClient, ClientVerification, Branch]),
                MockUserModule,
                CustomerModule,
                BranchModule,
                ConfigModule.forRoot({
                    load: [HashConfig],
                    isGlobal: true 
                })
            ]
        })
        .overrideProvider('KeycloakAdminService')
        .useFactory({
            factory: () => ({
                registerNewUserByClient: jest.fn((createUserDto: CreateUserDto) => {
                    return {id: '1'};
                })
            })
        })
        .compile();

        userClientRepository = moduleRef.get<Repository<UserClient>>(getRepositoryToken(UserClient));
        customerRepository = moduleRef.get<CustomerRepository>(CustomerRepository);

        const testCustomer: Customer = {
            id: 1,
            branchId: 1,
            cifNo: '001321000000000001',
            idType: IdTypeEnum.KTP,
            idNumber: '321000000000001',
            name: 'Test Customer',
            birthDate: '04-19-1992',
            gender: GenderEnum.Male,
            address: 'Test Address',
            motherMaidenName: 'Test Mother',
            status: CustomerStatusEnum.Live,
            registeredBy: 'testteller@mail.co',
            registeredAt: '03-20-2020'
        }
        await customerRepository.save(testCustomer);

        app = moduleRef.createNestApplication();
        app.useGlobalFilters(new CustomFilterException());
        await app.init();
    })

    describe('POST /api/users/register/:cifNo', () => {
        it('Positive Test', () => {
            return request(app.getHttpServer())
                .post('/api/users/register/001321000000000001')
                .send({
                    email: 'test@mail.co',
                    name: 'Test User',
                    clientVerification: {
                        verificationType: 'PIN',
                        verificationValue: '1234'
                    }
                })
                .expect(201)
                .expect({
                    customerId: 1,
                    clientType: 'IB_CLIENT',
                    credentialId: '1',
                    credentialType: 'EMAIL',
                    credentialValue: 'test@mail.co',
                    registeredBy: 'test@mail.co',
                    id: 1,
                    clientStatus: ClientStatusEnum.Live
                })
        })
        it('Negative test, client is exist', () => {
            return request(app.getHttpServer())
                .post('/api/users/register/001321000000000001')
                .send({
                    email: 'test@mail.co',
                    name: 'Test User',
                    clientVerification: {
                        verificationType: 'PIN',
                        verificationValue: '1234'
                    }
                })
                .expect(409)
        })
    })

    afterAll(async () => {
        await app.close();
    });
})