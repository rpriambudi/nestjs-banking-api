import { Controller, Inject, Post, Body, Param } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { UserService } from '../interface/user-service.interface';
import { CreateUserDto } from '../dto/create-user.dto';
import { UserRegistrationDto } from './../dto/user-registration.dto';
import { CreateVerificationDto } from './../dto/create-verification.dto';
import { StartRegistrationCommand } from './../commands/start-registration.command';
import { AssignUserToCustomerCommand } from './../commands/assign-user-to-customer.command';
import { AuthContext, UserContext } from './../../auth/decorators/user-context.decorator';
import { CustomerQueryService } from './../../customers/interfaces/customer-query-service.interface';

@Controller()
export class UserController {
    constructor(
        @Inject('UserService') private readonly userService: UserService,
        @Inject('CustomerQueryService') private readonly customerQueryService: CustomerQueryService,
        private readonly commandBus: CommandBus
    ) {}

    @Post('/api/users/register/:cifNo')
    async registerNewUser(
        @Param('cifNo') cifNo: string,
        @Body('email') email: string, 
        @Body('name') name: string,
        @Body('clientVerification') createVerificationDto: CreateVerificationDto,
        @AuthContext() userContext: UserContext
    ) {
        const createUserDto = new CreateUserDto();
        createUserDto.cifNo = cifNo;
        createUserDto.email = email;
        createUserDto.name = name;
        createUserDto.registeredBy = userContext.email;
        createUserDto.clientVerification = createVerificationDto;

        const user = await this.userService.registerNewUser(createUserDto);
        return user;
    }

    @Post('/api/users/registration')
    async userRegistrationByClient(@Body() userRegistrationDto: UserRegistrationDto) {
        const customer = await this.customerQueryService.findCustomerByIdTypeAndNo(
            userRegistrationDto.customer.idType, 
            userRegistrationDto.customer.idNumber
        );

        if (!customer)
            return await this.commandBus.execute(new StartRegistrationCommand(userRegistrationDto));

        return await this.commandBus.execute(new AssignUserToCustomerCommand(userRegistrationDto.user, customer.id));
    }

    @Post('/api/users/register/:cifNo/verification')
    async registerNewUserClientVerification(
        @Param('cifNo') cifNo: string,
        @Body('email') email: string,
        @Body() createVerificationDto: CreateVerificationDto,
    ) {
        const userClient = await this.userService.registerNewUserClientVerification(cifNo, email, createVerificationDto);
        return userClient;
    }
}