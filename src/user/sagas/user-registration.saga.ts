import { Injectable } from "@nestjs/common";
import { Observable, from } from "rxjs";
import { map, flatMap } from 'rxjs/operators';
import { Saga, ICommand, ofType, } from "@nestjs/cqrs";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { UserClient } from "./../entities/user-client.entity";
import { RegistrationState, StateTypes } from './../entities/sagas/registration-state.entity';
import { RegisterUserCommand } from '../commands/register-user.command';
import { FinishRegistrationCommand } from './../commands/finish-registration.command';
import { RegistrationStartedEvent } from './../events/registration-started.event';
import { UserClientCreatedEvent } from '../events/user-client-created.event';
import { CustomerCreatedEvent } from '../events/customer-created.event';
import { CifNoGeneratedEvent } from '../events/cif-no-generated.event';
import { AccountNoGeneratedEvent } from '../events/account-no-generated.event';
import { AccountGeneratedEvent } from '../events/account-generated.event';
import { GenerateAccountNoCommand } from '../../account/commands/generate-account-no.command';
import { CreateAccountCommand } from '../../account/commands/create-account.command';
import { RegisterCustomerCommand } from '../../customers/commands/register-customer.command';
import { GenerateCifNoCommand } from '../../customers/commands/generate-cif-no.command';
import { RegistrationFailedEvent } from "../events/registration-failed.event";
import { FailRegistrationCommand } from "../commands/fail-registration.command";

@Injectable()
export class UserRegistrationSagas {
    constructor(
        @InjectRepository(RegistrationState) private readonly repository: Repository<RegistrationState>,
        @InjectRepository(UserClient) private readonly userClientRepository: Repository<UserClient>
    ) {}

    @Saga()
    registrationStarted = (events$: Observable<any>): Observable<ICommand> => {
        return events$.pipe(
            ofType(RegistrationStartedEvent),
            flatMap(event => this.createUserClient(event))
        )
    }

    @Saga()
    userClientCreated = (events$: Observable<any>): Observable<ICommand> => {
        return events$
            .pipe(
                ofType(UserClientCreatedEvent),
                flatMap(event => this.generateCifNo(event))
            );
    }

    @Saga()
    cifNoGenerated = (events$: Observable<any>): Observable<ICommand> => {
        return events$
            .pipe(
                ofType(CifNoGeneratedEvent),
                flatMap(event => this.registerCustomer(event))
            )
    }

    @Saga()
    customerRegistered = (events$: Observable<any>): Observable<ICommand> => {
        return events$
            .pipe(
                ofType(CustomerCreatedEvent),
                flatMap(event => this.generateAccountNo(event))
            )
    }

    @Saga()
    accountNoGenerated = (events$: Observable<any>): Observable<ICommand> => {
        return events$
            .pipe(
                ofType(AccountNoGeneratedEvent),
                flatMap((event) => this.generateAccount(event))
            )
    }

    @Saga()
    accountGenerated = (events$: Observable<any>): Observable<ICommand> => {
        return events$
            .pipe(
                ofType(AccountGeneratedEvent),
                map(event => new FinishRegistrationCommand(event.stateId))
            )
    }

    @Saga()
    registrationFailed = (events$: Observable<any>): Observable<ICommand> => {
        return events$.pipe(
            ofType(RegistrationFailedEvent),
            map(event => new FailRegistrationCommand(event.stateId, event.error))
        )
    }

    /**
     * 
     * HELPER METHODS
     * 
     */

    private createUserClient = (event: RegistrationStartedEvent): Observable<RegisterUserCommand> => {
        return from(this.repository.findOne({ id: event.stateId })).pipe(
            flatMap((registrationState) => {
                registrationState.state = StateTypes.RegistrationStart;
                return this.repository.save(registrationState);
            }),
            map(registrationState => {
                return new RegisterUserCommand(registrationState.id, registrationState.data);
            })
        )
    }

    private generateCifNo = (event: UserClientCreatedEvent): Observable<GenerateCifNoCommand> => {
        return from(this.repository.findOne({ id: event.stateId})).pipe(
            flatMap(registrationState => {
                registrationState.state = StateTypes.UserClientCreated;
                registrationState.userClientId = event.userClientId;
                registrationState.description = null;

                return this.repository.save(registrationState);
            }),
            map(registrationState => {
                return new GenerateCifNoCommand(event.stateId, registrationState.data);
            })
        )
    }

    private registerCustomer = (event: CifNoGeneratedEvent): Observable<RegisterCustomerCommand> => {
        return from(this.repository.findOne({ id: event.stateId })).pipe(
            flatMap(registrationState => {
                registrationState.state = StateTypes.CifNoGenerated;
                registrationState.data.cifNo = event.cifNo;
                registrationState.description = null;

                return this.repository.save(registrationState);
            }),
            map(registrationState => {
                return new RegisterCustomerCommand(event.stateId, registrationState.data);
            })
        )
    }

    private generateAccountNo = (event: CustomerCreatedEvent): Observable<GenerateAccountNoCommand> => {
        return from(this.repository.findOne({ id: event.stateId })).pipe(
            flatMap(registrationState => {
                registrationState.state = StateTypes.CustomerRegistered;
                registrationState.data.customerId = event.customerId;
                registrationState.description = null;

                return this.repository.save(registrationState);
            }),
            map(registrationState => {
                from(this.userClientRepository.findOne({ id: registrationState.userClientId })).pipe(
                    map(userClient => {
                        userClient.customerId = event.customerId;

                        return this.userClientRepository.save(userClient);
                    })
                ).subscribe();

                return new GenerateAccountNoCommand(event.stateId, registrationState.data);
            })
        )
    }

    private generateAccount = (event: AccountNoGeneratedEvent): Observable<CreateAccountCommand> => {
        return from(this.repository.findOne({ id: event.stateId })).pipe(
            flatMap(registrationState => {
                registrationState.state = StateTypes.AccountNoGenerated;
                registrationState.data.accountNo = event.accountNo;
                registrationState.description = null;

                return this.repository.save(registrationState);
            }),
            map(registrationState => {
                return new CreateAccountCommand(event.stateId, registrationState.data);
            })
        )
    }

}