import { CommandHandler, ICommandHandler, EventBus } from "@nestjs/cqrs";
import { Inject } from "@nestjs/common";
import { AssignUserToCustomerCommand } from './../assign-user-to-customer.command';
import { AssignUserToCustomerSuccessEvent } from './../../events/assign-user-to-customer-success.event';
import { UserService } from './../../interface/user-service.interface';


@CommandHandler(AssignUserToCustomerCommand)
export class AssignUserToCustomerHandler implements ICommandHandler<AssignUserToCustomerCommand> {
    constructor(
        @Inject('UserService') private readonly userService: UserService,
        private readonly eventBus: EventBus
    ) {}

    async execute(command: AssignUserToCustomerCommand): Promise<any> {
        const { registerNewUserDto, customerId} = command;
        const userClient = await this.userService.registerUserByExistingCustomer(registerNewUserDto, customerId);

        this.eventBus.publish(new AssignUserToCustomerSuccessEvent(userClient.id, customerId));
        return userClient;
    }
}