import { CommandHandler, ICommandHandler, EventBus } from "@nestjs/cqrs";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { StartRegistrationCommand } from './../start-registration.command';
import { RegistrationState, StateTypes } from './../../entities/sagas/registration-state.entity';
import { RegistrationStartedEvent } from './../../events/registration-started.event';

@CommandHandler(StartRegistrationCommand)
export class StartRegistrationHandler implements ICommandHandler<StartRegistrationCommand> {
    constructor(
        @InjectRepository(RegistrationState) private readonly repository: Repository<RegistrationState>,
        private readonly eventBus: EventBus
    ) {}

    async execute(command: StartRegistrationCommand): Promise<any> {
        const { userRegistrationDto } = command;
        const registrationState = this.repository.create();
        registrationState.state = StateTypes.RegistrationStart;
        registrationState.data = userRegistrationDto;

        await this.repository.save(registrationState);
        this.eventBus.publish(new RegistrationStartedEvent(registrationState.id));
        return;
    }
}