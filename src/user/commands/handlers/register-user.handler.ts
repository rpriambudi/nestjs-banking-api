import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler, EventBus } from '@nestjs/cqrs';
import { RegisterUserCommand } from './../register-user.command';
import { UserClient } from './../../entities/user-client.entity';
import { UserService } from './../../interface/user-service.interface';
import { UserClientCreatedEvent } from './../../events/user-client-created.event';
import { RegistrationFailedEvent } from './../../events/registration-failed.event'

@CommandHandler(RegisterUserCommand)
export class RegisterUserHandler implements ICommandHandler<RegisterUserCommand>{
    constructor(
        @Inject('UserService') private readonly userService: UserService,
        private readonly eventBus: EventBus
    ) {}

    async execute(command: RegisterUserCommand): Promise<UserClient> {
        const { userRegistrationDto, stateId } = command;

        try {
            const userClient = await this.userService.registerUserByNewCustomer(userRegistrationDto.user);
            this.eventBus.publish(new UserClientCreatedEvent(stateId, userClient.id));
            return userClient;
        } catch (error) {
            this.eventBus.publish(new RegistrationFailedEvent(stateId, error.message));
        }
    }
}