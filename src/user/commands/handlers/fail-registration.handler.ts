import { CommandHandler, ICommandHandler } from "@nestjs/cqrs";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { FailRegistrationCommand } from './../fail-registration.command';
import { RegistrationState, StateTypes } from "./../../entities/sagas/registration-state.entity";

@CommandHandler(FailRegistrationCommand)
export class FailRegistrationHandler implements ICommandHandler<FailRegistrationCommand> {
    constructor(
        @InjectRepository(RegistrationState) private readonly repository: Repository<RegistrationState>
    ) {}

    async execute(command: FailRegistrationCommand): Promise<any> {
        const { error, stateId } = command;
        const state = await this.repository.findOne({ id: stateId });

        state.state = StateTypes.RegistrationFailed;
        state.description = error;
        await this.repository.save(state);

        return state;
    }
    
}