import { CommandHandler, ICommandHandler } from "@nestjs/cqrs";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { FinishRegistrationCommand } from './../finish-registration.command';
import { RegistrationState, StateTypes } from "./../../entities/sagas/registration-state.entity";

@CommandHandler(FinishRegistrationCommand)
export class FinishRegistrationHandler implements ICommandHandler<FinishRegistrationCommand> {
    constructor(
        @InjectRepository(RegistrationState) private readonly repository: Repository<RegistrationState>
    ) {}

    async execute(command: FinishRegistrationCommand): Promise<any> {
        const { stateId } = command;
        const state = await this.repository.findOne({ id: stateId });

        state.state = StateTypes.RegistrationFinished;
        state.description = 'Registration Finished';
        await this.repository.save(state);

        return state;
    }
}