export class FinishRegistrationCommand {
    constructor(
        public readonly stateId: number
    ) {}
}