import { UserRegistrationDto } from './../dto/user-registration.dto';

export class StartRegistrationCommand {
    constructor(
        public readonly userRegistrationDto: UserRegistrationDto
    ) {}
}