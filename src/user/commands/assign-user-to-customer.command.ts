import { UserRegistrationDto } from './../dto/user-registration.dto';
import { RegisterNewUserDto } from './../dto/register-new-user.dto';

export class AssignUserToCustomerCommand {
    constructor(
        public readonly registerNewUserDto: RegisterNewUserDto,
        public readonly customerId: number
    ) {}
}