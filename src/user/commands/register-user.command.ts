import { UserRegistrationDto } from '../dto/user-registration.dto';

export class RegisterUserCommand {
    constructor(
        public readonly stateId: number,
        public readonly userRegistrationDto: UserRegistrationDto
    ) {}
}