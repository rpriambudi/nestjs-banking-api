export class FailRegistrationCommand {
    constructor(
        public readonly stateId: number,
        public readonly error: string
    ) {}
}