import { Entity, PrimaryGeneratedColumn, Column, JoinColumn, OneToOne } from 'typeorm'
import { UserClient } from './user-client.entity';

@Entity()
export class ClientVerification {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        name: 'verification_type'
    })
    verificationType: string;

    @Column({
        type: 'varchar',
        name: 'verification_value'
    })
    verificationValue: string;

    @Column({
        type: 'timestamp',
        name: 'created_at',
        default: new Date()
    })
    createdAt: Date;

    @OneToOne(type => UserClient, userClient => userClient.clientVerification)
    @JoinColumn({
        name: 'user_client_id'
    })
    userClient: UserClient;
}