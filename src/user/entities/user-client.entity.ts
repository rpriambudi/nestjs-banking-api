import { Entity, PrimaryGeneratedColumn, Column, OneToOne } from 'typeorm';
import { ClientVerification } from './client-verification.entity';
import { ClientStatusEnum } from './../enums/client-status.enum';
import { UserLog } from './user-log.entity';

@Entity()
export class UserClient {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        name: 'customer_id',
        nullable: true
    })
    customerId: number;

    @Column({
        type: 'varchar',
        name: 'client_type'
    })
    clientType: string;

    @Column({
        type: 'varchar',
        name: 'client_status',
        default: ClientStatusEnum.Pending
    })
    clientStatus: string;

    @Column({
        type: 'varchar',
        name: 'credential_id',
        nullable: true
    })
    credentialId: string;

    @Column({
        type: 'varchar',
        name: 'credential_type'
    })
    credentialType: string;

    @Column({
        type: 'varchar',
        name: 'credential_value'
    })
    credentialValue: string;

    @Column({
        type: 'varchar',
        name: 'registered_by'
    })
    registeredBy: string;

    @OneToOne(type => ClientVerification, clientVerification => clientVerification.userClient)
    clientVerification: ClientVerification;
    
}