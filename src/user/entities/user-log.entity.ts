import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn } from "typeorm";
import { UserClient } from "./user-client.entity";


@Entity()
export class UserLog {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        name: 'user_client_id'
    })
    userClientId: number;

    @Column({
        type: 'varchar',
        name: 'user_state'
    })
    userState: string;

    @Column({
        type: 'varchar',
        name: 'user_state_description'
    })
    userStateDescription: string;

    @Column({
        type: 'jsonb',
        name: 'user_state_data',
        nullable: true
    })
    userStateData: any
}