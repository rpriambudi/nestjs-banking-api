export class CustomerCreatedEvent {
    constructor(
        public readonly stateId: number,
        public readonly customerId: number
    ) {}
}