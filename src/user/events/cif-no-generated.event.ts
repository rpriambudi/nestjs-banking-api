export class CifNoGeneratedEvent {
    constructor(
        public readonly stateId: number,
        public readonly cifNo: string
    ) {}
}