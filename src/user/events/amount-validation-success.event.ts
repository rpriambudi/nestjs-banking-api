import { UserRegistrationDto } from './../dto/user-registration.dto';

export class AmountValidationSuccessEvent {
    constructor(
        public readonly userRegistrationDto: UserRegistrationDto
    ) {}
}