export class RegistrationStartedEvent {
    constructor(
        public readonly stateId: number
    ) {}
}