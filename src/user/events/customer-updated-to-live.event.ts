import { UserRegistrationDto } from './../dto/user-registration.dto';

export class CustomerUpdatedToLiveEvent {
    constructor(
        public readonly userRegistrationDto: UserRegistrationDto 
    ) {}
}