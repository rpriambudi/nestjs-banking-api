export class RegistrationFinishedEvent {
    constructor(
        public readonly stateId: number
    ) {}
}