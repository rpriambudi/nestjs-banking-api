import { UserRegistrationDto } from './../dto/user-registration.dto';

export class UserUpdatedToLiveEvent {
    constructor(
        public readonly userRegistrationDto: UserRegistrationDto
    ) {}
}