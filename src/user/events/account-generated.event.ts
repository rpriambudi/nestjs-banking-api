export class AccountGeneratedEvent {
    constructor(
        public readonly stateId: number
    ) {}
}