export class UserClientCreatedEvent {
    constructor(
        public readonly stateId: number,
        public readonly userClientId: number
    ) {}
}