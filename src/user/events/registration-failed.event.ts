export class RegistrationFailedEvent {
    constructor(
        public readonly stateId: number,
        public readonly error: string
    ) {}
}