import { UserRegistrationDto } from './../dto/user-registration.dto';

export class IdTypeValidationFailedEvent {
    constructor(
        public readonly userRegistrationDto: UserRegistrationDto,
        public readonly error: Error
    ) {}
}