import { UserRegistrationDto } from "./../dto/user-registration.dto";

export class IdTypeValidationSuccessEvent {
    constructor(
        public readonly userRegistrationDto: UserRegistrationDto
    ) {}
}