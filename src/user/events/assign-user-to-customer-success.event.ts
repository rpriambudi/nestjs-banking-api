export class AssignUserToCustomerSuccessEvent {
    constructor(
        public readonly userClientId: number,
        public readonly customerId: number
    ) {}
}