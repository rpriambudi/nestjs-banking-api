export class AccountNoGeneratedEvent {
    constructor(
        public readonly stateId: number,
        public readonly accountNo: string
    ) {}
}