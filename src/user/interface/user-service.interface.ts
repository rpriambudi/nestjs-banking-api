import { CreateUserDto } from '../dto/create-user.dto';
import { CreateVerificationDto } from './../dto/create-verification.dto';
import { RegisterNewUserDto } from './../dto/register-new-user.dto';
import { UserClient } from './../entities/user-client.entity';

export interface UserService {
    registerNewUser(createUserDto: CreateUserDto): Promise<UserClient>;
    registerUserByNewCustomer(registerNewUserDto: RegisterNewUserDto): Promise<UserClient>;
    registerUserByExistingCustomer(registerNewUserDto: RegisterNewUserDto, customerId: number): Promise<UserClient>;
    registerNewUserClientVerification(cifNo: string, email: string, createVerificationDto: CreateVerificationDto): Promise<UserClient>;
}