import { ClientVerification } from "./../entities/client-verification.entity";

export interface EncryptionService {
    encode(data: string): string;
    decode(data: string): string;
    compare(data: string, clientVerification: ClientVerification): boolean;
}