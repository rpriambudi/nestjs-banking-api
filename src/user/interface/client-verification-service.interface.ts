import { ClientVerification } from './../entities/client-verification.entity';
import { CreateVerificationDto } from './../dto/create-verification.dto';

export interface ClientVerificationService {
    createNewClientVerification(createVerificationDto: CreateVerificationDto): Promise<ClientVerification>;
}