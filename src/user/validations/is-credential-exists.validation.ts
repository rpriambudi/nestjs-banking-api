import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { UserClient } from './../entities/user-client.entity';
import { RegisterNewUserDto } from './../dto/register-new-user.dto';
import { BusinessRule } from './../interface/business-rule.interface';
import { UniqueClientException } from './../exceptions/unique-client.exception';

export class IsCredentialExistsValidation implements BusinessRule {
    constructor(
        @InjectRepository(UserClient) private readonly userClientRepository: Repository<UserClient>
    ) {}

    async validate(validationData: RegisterNewUserDto): Promise<void> {
        const userClient = await this.userClientRepository.findOne({
            credentialType: validationData.credentialType, 
            credentialValue: validationData.username
        });

        if (userClient)
            throw new UniqueClientException('User Client already exists');
    }
    
}