import { Module, NestModule, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { getRepositoryToken, TypeOrmModule } from '@nestjs/typeorm';
import { CqrsModule } from '@nestjs/cqrs';
import { UserRegistrationSagas } from './sagas/user-registration.saga';
import { RuleContext } from './context/rule.context';
import { ValidateVerificationContext } from './context/validate-verification.context';
import { UserClient } from './entities/user-client.entity';
import { ClientVerification } from './entities/client-verification.entity';
import { UserLog } from './entities/user-log.entity';
import { RegistrationState } from './entities/sagas/registration-state.entity';
import { UserController } from './controllers/user.controller';
import { CustomerServiceAuthMiddleware } from './middleware/customer-service-auth.middleware';
import { UserServiceImpl } from './service/user-impl.service';
import { KeycloakAdminService } from './service/keycloak-admin.service';
import { MailSenderService } from './service/mail-sender.service';
import { ExistingUserService } from './service/existing-user.service';
import { ClientVerificationServiceImpl } from './service/client-verification-impl.service';
import { CreateOtpVerificationRule } from './service/create-otp-verification-rule.service';
import { CreatePinVerificationRule } from './service/create-pin-verification-rule.service';
import { ValidatePinVerificationRule } from './service/validate-pin-verification-rule.service';
import { AesEncryptionServiceImpl } from './service/aes-encryption-impl.service';
import { IsCredentialExistsValidation } from './validations/is-credential-exists.validation';
import { StartRegistrationHandler } from './commands/handlers/start-registration.handler';
import { RegisterUserHandler } from './commands/handlers/register-user.handler';
import { AssignUserToCustomerHandler } from './commands/handlers/assign-user-to-customer.handler';
import { FinishRegistrationHandler } from './commands/handlers/finish-registration.handler';
import { FailRegistrationHandler } from './commands/handlers/fail-registration.handler';
import { CustomerModule } from './../customers/customer.module';

const CommandHandlers = [
    RegisterUserHandler, 
    AssignUserToCustomerHandler, 
    StartRegistrationHandler,
    FinishRegistrationHandler,
    FailRegistrationHandler
];
const EventHandlers = [];

@Module({
    imports: [
        TypeOrmModule.forFeature([UserClient, ClientVerification, RegistrationState]),
        CustomerModule,
        CqrsModule
    ],
    controllers: [UserController],
    providers: [
        {
            provide: 'UserService',
            useFactory: (
                userClientRepository, 
                customerQueryService, 
                clientVerificationService, 
                keycloakAdminService,
                isClientExistRule,
                newClientVerificationRule,
                isCredentialExistsValidation
            ) => {
                return new UserServiceImpl(
                    userClientRepository, 
                    customerQueryService, 
                    clientVerificationService, 
                    keycloakAdminService, 
                    isClientExistRule, 
                    newClientVerificationRule,
                    isCredentialExistsValidation
                );
            },
            inject: [
                getRepositoryToken(UserClient), 
                'CustomerQueryService', 
                'ClientVerificationService', 
                'KeycloakAdminService', 
                'IsClientExistRule',
                'NewClientVerificationRule',
                'IsCredentialExistsValidation'
            ]
        },
        {
            provide: 'KeycloakAdminService',
            useFactory: (configService) => {
                return new KeycloakAdminService(
                    configService, 
                    new MailSenderService(configService)
                );
            },
            inject: [ConfigService]
        },
        { 
            provide: 'ClientVerificationService',
            useFactory: (clientVerificationRepository, encryptionService) => {
                return new ClientVerificationServiceImpl(clientVerificationRepository, encryptionService);
            },
            inject: [getRepositoryToken(ClientVerification), 'EncryptionService']
        },
        {
            provide: 'IsClientExistRule',
            useFactory: (userClientRepository) => {
                return new RuleContext([new ExistingUserService(userClientRepository)]);
            },
            inject: [getRepositoryToken(UserClient)]
        },
        {
            provide: 'IsCredentialExistsValidation',
            useFactory: (userClientRepository) => {
                return new RuleContext([new IsCredentialExistsValidation(userClientRepository)])
            },
            inject: [getRepositoryToken(UserClient)]
        },
        {
            provide: 'NewClientVerificationRule',
            useFactory: () => {
                return new RuleContext([new CreateOtpVerificationRule(), new CreatePinVerificationRule()]);
            }
        },
        {
            provide: 'ValidateVerificationRule',
            useFactory: (userClientRepository, encryptionService) => {
                const ruleList = [
                    new ValidatePinVerificationRule(encryptionService)
                ];
                return new ValidateVerificationContext(
                    userClientRepository,
                    ruleList
                );
            },
            inject: [getRepositoryToken(UserClient), 'EncryptionService']
        },
        {
            provide: "EncryptionService",
            useFactory: (configService) => {
                return new AesEncryptionServiceImpl(configService);
            },
            inject: [ConfigService]
        },
        UserRegistrationSagas,
        ...CommandHandlers,
        ...EventHandlers
    ],
    exports: [
        {
            provide: 'ValidateVerificationRule',
            useFactory: (userClientRepository, encryptionService) => {
                const ruleList = [
                    new ValidatePinVerificationRule(encryptionService)
                ];
                return new ValidateVerificationContext(
                    userClientRepository,
                    ruleList
                );
            },
            inject: [getRepositoryToken(UserClient), 'EncryptionService']
        }
    ]
})

export class UserModule implements NestModule{
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(CustomerServiceAuthMiddleware)
            .exclude(
                {
                    path: '/api/users/registration',
                    method: RequestMethod.POST
                }
            )
            .forRoutes(UserController);
    }
    
}