import { BusinessRule } from './../interface/business-rule.interface';

export class RuleContext implements BusinessRule {
    private ruleList: BusinessRule[];

    constructor(ruleList: BusinessRule[]) {
        this.ruleList = ruleList;
    }

    async validate(validationData: any): Promise<void> {
        for (const ruleObj of this.ruleList) {
            await ruleObj.validate(validationData);
        }
    }
}