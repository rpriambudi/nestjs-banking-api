import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserClient } from './../entities/user-client.entity';
import { BusinessRule } from './../interface/business-rule.interface';
import { ValidateVerificationDto } from './../dto/validate-verification.dto';
import { UserClientNotRegisteredException } from './../exceptions/user-client-not-registered.exception';

export class ValidateVerificationContext implements BusinessRule {
    private ruleList: BusinessRule[];

    constructor(
        @InjectRepository(UserClient) private readonly userClientRepository: Repository<UserClient>,
        ruleList: BusinessRule[]
    ) {
        this.ruleList = ruleList;
    }

    async validate(validationData: ValidateVerificationDto): Promise<void> {
        const userClient = await this.userClientRepository.findOne({
            relations: ['clientVerification'],
            where: {
                customerId: validationData.customerId,
                credentialValue: validationData.email
            }
        });

        if (!userClient.clientVerification)
            throw new UserClientNotRegisteredException('Client verification not yet created.');

        validationData.clientVerification = userClient.clientVerification;
        for (const rule of this.ruleList) {
            await rule.validate(validationData);
        }
    }
    
}