export enum CredentialTypeEnum {
    Email = 'EMAIL',
    Phone = 'PHONE'
}