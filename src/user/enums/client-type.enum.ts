export enum ClientTypeEnum {
    InternetBanking = 'IB_CLIENT',
    MobileBanking = 'MB_CLIENT'
}