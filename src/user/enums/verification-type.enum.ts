export enum VerificationTypeEnum {
    PIN = 'PIN',
    OTP = 'OTP'
}