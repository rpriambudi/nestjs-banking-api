export enum ClientStatusEnum {
    Pending = 'PENDING',
    Live = 'LIVE'
}