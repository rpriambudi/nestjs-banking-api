export class InvalidVerificationException {
    public code: string;
    public customCode: string;
    public message: string;

    constructor(message: string) {
        this.code = 'INVALID_VERIFICATION';
        this.customCode = '400999';
        this.message = message;
    }
}