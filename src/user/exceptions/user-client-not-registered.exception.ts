export class UserClientNotRegisteredException {
    public code: string;
    public customCode: string;
    public message: string;

    constructor(message: string) {
        this.code = 'USER_CLIENT_NOT_REGISTERED';
        this.customCode = '400998';
        this.message = message;
    }
}