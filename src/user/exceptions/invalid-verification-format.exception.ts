export class InvalidVerificationFormat {
    public code: string;
    public customCode: string;
    public message: string;

    constructor(message: string) {
        this.code = 'INVALID_VERIFICATION_FORMAT';
        this.customCode = '400997';
        this.message = message;
    }
}