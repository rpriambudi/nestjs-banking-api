import { GenericException } from './../../exceptions/generic-exception.abstract';

export class KeycloakException extends GenericException {
    getDisplayCode(): string {
        return 'KEYCLOAK_FAILURE';
    }

    getErrorCode(): string {
        return '400995';
    }

    constructor(message: string) {
        super(message);
    }
}