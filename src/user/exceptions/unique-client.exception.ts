import { GenericException } from './../../exceptions/generic-exception.abstract';

export class UniqueClientException extends GenericException {
    getDisplayCode(): string {
        return 'UNIQUE_CLIENT';
    }

    getErrorCode(): string {
        return '400996';
    }
    

    constructor(message: string) {
        super(message);
    }
}