import { AuthStrategyMiddleware } from './../../auth/middleware/auth-strategy.middleware';

export class CustomerServiceAuthMiddleware extends AuthStrategyMiddleware {
    getStrategyName(): string {
        return 'JWT';
    }    
    getGuards(): string[] {
        return ['Customer Service'];
    }
}