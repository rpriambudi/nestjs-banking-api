import { IsNotEmpty } from 'class-validator';
import { RegisterNewUserDto } from './register-new-user.dto';
import { CreateAccountDto } from './../../account/dto/create-account.dto';
import { CreateCustomerDto } from './../../customers/dto/create-customer.dto';

export class UserRegistrationDto {
    @IsNotEmpty()
    user: RegisterNewUserDto;

    @IsNotEmpty()
    customer: CreateCustomerDto;

    customerId: number;

    accountNo: string;

    cifNo: string;
}