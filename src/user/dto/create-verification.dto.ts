import { IsIn, IsNotEmpty } from 'class-validator';
import { VerificationTypeEnum } from './../enums/verification-type.enum';
import { UserClient } from './../entities/user-client.entity';

export class CreateVerificationDto {
    @IsIn(Object.values(VerificationTypeEnum))
    verificationType: string;

    @IsNotEmpty()
    verificationValue: string;

    userClient: UserClient;
}