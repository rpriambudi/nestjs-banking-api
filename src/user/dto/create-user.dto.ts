import { CreateVerificationDto } from './create-verification.dto';

export class CreateUserDto {
    cifNo: string;

    email: string;

    name: string;

    registeredBy: string;

    clientVerification: CreateVerificationDto;
    
    constructor() {}

    getFirstName(): string {
        const splittedName: string[] = this.name.split(' ');
        return splittedName[0];
    }

    getLastName(): string {
        let lastName: string;
        let splittedName: string[] = this.name.split(' ');

        if (splittedName.length <= 1)
            return null;

        lastName = '';
        for (let i = 1; i < splittedName.length; i++) {
            lastName += splittedName[i];
            if (i != (splittedName.length-1))
                lastName += ' '
        }
        return lastName;
    }
}