export class UserExistingDto {
    customerId: number;

    clientType: string;

    constructor(customerId: number, clientType: string) {
        this.customerId = customerId;
        this.clientType = clientType;
    }
}