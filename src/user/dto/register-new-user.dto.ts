import { IsNotEmpty} from 'class-validator';
import { CredentialTypeEnum } from './../enums/credential-type.enum';

export class RegisterNewUserDto {
    @IsNotEmpty()
    credentialType: CredentialTypeEnum;

    @IsNotEmpty()
    username: string;

    @IsNotEmpty()
    password: string;

    id: number;
}