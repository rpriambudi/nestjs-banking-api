import { ClientVerification } from './../entities/client-verification.entity';

export class ValidateVerificationDto {
    customerId: number;
    email: string;
    challange: string;
    clientVerification: ClientVerification;
}