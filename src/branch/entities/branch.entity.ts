import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class Branch {
    @PrimaryGeneratedColumn()
    id: number

    @Column({
        type: 'varchar',
        unique: true,
        name: 'branch_code'
    })
    branchCode: string

    @Column({
        type: 'varchar',
        unique: true,
        name: 'name'
    })
    name: string

    @Column({
        type: 'text',
        unique: false,
        name: 'address'
    })
    address: string
}