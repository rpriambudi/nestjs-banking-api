import { Injectable, Inject } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { Branch } from '../entities/branch.entity'
import { CreateBranchDto } from '../dto/create-branch.dto'
import { BranchService } from '../interfaces/branch-service.interface';
import { BranchNotFoundException } from '../exceptions/branch-not-found.exception'

@Injectable()
export class BranchServiceImpl implements BranchService {
    constructor(
        @InjectRepository(Branch) private readonly branchRepository : Repository<Branch>,
    ) {}

    async createNewBranch(branchDto: CreateBranchDto): Promise<Branch> {
        const branchObj = this.branchRepository.create(branchDto);
        return await this.branchRepository.save(branchObj);
    }

    async findBranchByCode(branchCode: string): Promise<Branch> {
        const branchData = await this.branchRepository.findOne({ branchCode: branchCode})
        if (!branchData)
            throw new BranchNotFoundException('Branch not found. Branch code: ' + branchCode)
        
        return branchData
    }

    async findBranchById(branchId: number): Promise<Branch> {
        return this.branchRepository.findOne(branchId);
    }

    async getBranchIdByCode(branchCode: string): Promise<number> {
        const branchData = await this.branchRepository.findOne({ branchCode: branchCode});
        if (!branchData)
            throw new BranchNotFoundException('Branch not found. Branch code: ' + branchCode);
        
        return branchData.id;
    }

    async adjustBranchAccount(branchCode: string, adjusmentType: string, amount: number): Promise<Branch> {
        throw new Error('Not yet implemented');
    }
}