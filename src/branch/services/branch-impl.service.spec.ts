import { Test } from '@nestjs/testing'
import { Branch } from '../entities/branch.entity'
import { CreateBranchDto } from '../dto/create-branch.dto'
import { BranchServiceImpl } from './branch-impl.service'
import { Repository } from 'typeorm'
import { getRepositoryToken } from '@nestjs/typeorm'
import { BranchNotFoundException } from '../exceptions/branch-not-found.exception'

describe('BranchService test', () => {
    let branchService: BranchServiceImpl
    let repo: Repository<Branch>
    
    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            providers: [
                BranchServiceImpl,
                {
                    provide: getRepositoryToken(Branch),
                    useClass: Repository
                }
            ]
        }).compile();

        branchService = moduleRef.get<BranchServiceImpl>(BranchServiceImpl);
        repo = moduleRef.get<Repository<Branch>>(getRepositoryToken(Branch))
    })

    describe('findById method', () => {
        it('positive test. should returning one branch object', async () => {
            const testBranch: Branch = {
                id: 1,
                branchCode: '001',
                name: 'Test Branch',
                address: 'Test Branch'
            }

            jest.spyOn(repo, 'findOne').mockResolvedValueOnce(testBranch);
            expect(await branchService.findBranchById(1)).toBe(testBranch);
        })
    })

    describe('findByCode method', () => {
        it('positive test. should returning one branch object', async () => {
            const testBranch: Branch = {
                id: 1,
                branchCode: '001',
                name: 'Test Branch',
                address: 'Test Branch'
            }

            jest.spyOn(repo, 'findOne').mockResolvedValue(testBranch);
            expect(await branchService.findBranchByCode('001')).toBe(testBranch);
        })

        it('negative test, branch code not found. should returning null value', async () => {
            jest.spyOn(repo, 'findOne').mockResolvedValueOnce(null);
            try {
                expect(await branchService.findBranchByCode('003')).toBeCalled();
            } catch (error) {
                expect(error instanceof BranchNotFoundException).toBe(true);
            }
        })
    })

    describe('create method', () => {
        it('positive test. should returning one new branch object', async () => {
            const branchDto: CreateBranchDto = {
                branchCode: '001',
                name: 'Test Branch',
                address: 'Test Branch'
            }
            const testBranch: Branch = {
                id: 1,
                branchCode: '001',
                name: 'Test Branch',
                address: 'Test Branch'
            }

            jest.spyOn(repo, 'create').mockReturnValueOnce(testBranch);
            jest.spyOn(repo, 'save').mockResolvedValueOnce(testBranch);
            expect(await branchService.createNewBranch(branchDto)).toBe(testBranch);
        })
    })
})