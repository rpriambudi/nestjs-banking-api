import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication, MiddlewareConsumer } from '@nestjs/common';
import { getRepositoryToken, TypeOrmModule } from '@nestjs/typeorm';
import { Branch } from './../entities/branch.entity';
import { BranchModule } from './../branch.module';
import { BranchController } from './branch.controller';
import { AuthStrategyMiddleware } from './../../auth/middleware/auth-strategy.middleware';
import { ConfigModule } from '@nestjs/config';
import { resolve } from 'dns';

export class MockAuthMiddleware extends AuthStrategyMiddleware {
    constructor() {
        super(null);
    }

    getStrategyName(): string {
        return 'Mock'
    }    
    
    getGuards(): string[] {
        return [];
    }

    runMockStrategy(request: any, guards: string[]) {
        return {};
    }
}

export class MockBranchModule extends BranchModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(MockAuthMiddleware)
            .forRoutes(BranchController)
    }
}

describe('Branch controller', () => {
    let app: INestApplication;

    beforeAll(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [
                TypeOrmModule.forRoot({
                    type: 'sqlite',
                    database: ':memory:',
                    dropSchema: true,
                    entities: [Branch],
                    synchronize: true,
                    logging: false
                }),
                TypeOrmModule.forFeature([Branch]),
                MockBranchModule, 
                ConfigModule.forRoot({isGlobal: true})
            ]
        })
        .compile();

        app = moduleRef.createNestApplication();
        await app.init();
    });

    describe('POST /api/branch endpoints', () => {
        it('Positive test', () => {
            return request(app.getHttpServer())
                .post('/api/branch')
                .send({
                    branchCode: '001',
                    name: 'Test Branch',
                    address: 'Test Address'
                })
                .expect(201)
                .expect({
                    id: 1,
                    branchCode: '001',
                    name: 'Test Branch',
                    address: 'Test Address'
                });
        })
    })
    describe('GET /api/branch/:id endpoints', () => {
        it('Positive test', () => {
            return request(app.getHttpServer())
                .get('/api/branch/1')
                .expect(200)
                .expect({
                    id: 1,
                    branchCode: '001',
                    name: 'Test Branch',
                    address: 'Test Address'
                })
        })
        it('Negative test', () => {
            return request(app.getHttpServer())
                .get('/api/branch/2')
                .expect(200)
                .expect({})
        })
    })

    afterAll(async () => {
        await app.close();
    })
})