import { Controller, Post, Get, Body, Param, Inject } from '@nestjs/common'
import { CreateBranchDto } from '../dto/create-branch.dto'
import { BranchService } from '../interfaces/branch-service.interface'

@Controller()
export class BranchController {
    constructor(@Inject('BranchService') private readonly branchService : BranchService) {}

    @Post('/api/branch')
    async registerNewBranch(@Body() branchDto : CreateBranchDto) {
        const branchData = await this.branchService.createNewBranch(branchDto);
        return branchData;
    }

    @Get('/api/branch/:id')
    async getBranchById(@Param('id') id : number) {
        const branchData = await this.branchService.findBranchById(id);
        return branchData;
    }
}