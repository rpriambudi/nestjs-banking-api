import { IsNotEmpty } from 'class-validator'

export class CreateBranchDto {
    @IsNotEmpty()
    name: string;

    @IsNotEmpty()
    branchCode: string;

    @IsNotEmpty()
    address: string;
}