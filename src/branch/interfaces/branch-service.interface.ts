import { CreateBranchDto } from "../dto/create-branch.dto";
import { Branch } from "../entities/branch.entity";

export interface BranchService {
    createNewBranch(branchDto: CreateBranchDto): Promise<Branch>;
    findBranchByCode(branchCode: string): Promise<Branch>;
    findBranchById(branchId: number): Promise<Branch>;
    getBranchIdByCode(branchCode: string): Promise<number>;
    adjustBranchAccount(branchCode: string, adjusmentType: string, amount: number): Promise<Branch>;
}