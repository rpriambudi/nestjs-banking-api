import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ConfigService } from '@nestjs/config'
import { AuthModule } from './../auth/auth.module'
import { AuthValidatorImpl } from './../auth/service/auth-validator.service';
import { TellerAuthMiddleware } from './middleware/teller-auth.middleware';
import { Branch } from './entities/branch.entity'
import { BranchServiceImpl } from './services/branch-impl.service'
import { BranchController } from './controllers/branch.controller'

@Module({
    imports: [
        TypeOrmModule.forFeature([Branch]),
        AuthModule,
    ],
    controllers: [BranchController],
    providers: [
        {
            provide: 'BranchService',
            useClass: BranchServiceImpl
        },
        {
            provide: 'AuthValidation',
            useFactory: (configService) => {
                return new AuthValidatorImpl(configService, ['Branch']);
            },
            inject: [ConfigService]
        }
    ],
    exports: [
        {
            provide: 'BranchService',
            useClass: BranchServiceImpl
        }
    ]
})

export class BranchModule implements NestModule{
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(TellerAuthMiddleware)
            .forRoutes(BranchController)
    }

}