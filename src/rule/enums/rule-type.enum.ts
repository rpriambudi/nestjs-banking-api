export enum RuleTypeEnum {
    AccountCreate = 'ACCOUNT_CREATE',
    TransferRequest = 'TRANSFER_REQUEST'
}