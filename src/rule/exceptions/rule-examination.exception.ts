export class RuleExaminationException {
    public code: string;
    public customCode: string;
    public message: string;

    constructor(message: string) {
        this.code = 'RULE_EXAMINATION';
        this.customCode = '400803';
        this.message = message;
    }
}