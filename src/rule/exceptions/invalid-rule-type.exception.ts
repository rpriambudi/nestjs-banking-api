export class InvalidRuleTypeException {
    public code: string;
    public customCode: string;
    public message: string;
    
    constructor(message: string) {
        this.code = 'INVALID_RULE_TYPE';
        this.customCode = '400801';
        this.message = message;
    }
}