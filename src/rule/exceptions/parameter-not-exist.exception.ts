export class ParameterNotExistException {
    public code: string;
    public customCode: string;
    public message: string;

    constructor(message: string) {
        this.code = 'PARAMETER_NOT_EXIST';
        this.customCode = '400802';
        this.message = message;
    }
}