import { Controller, Inject, Post, Body } from "@nestjs/common";
import { RuleService } from "../interfaces/rule-service.interface";
import { CreateRuleDto } from "../dto/create-rule.dto";

@Controller()
export class RuleController {
    constructor(
        @Inject('RuleService') private readonly ruleService: RuleService
    ) {}

    @Post('/api/rule')
    async createRule(@Body() createRuleDto: CreateRuleDto) {
        const ruleData = await this.ruleService.create(createRuleDto);
        return ruleData;
    }
}