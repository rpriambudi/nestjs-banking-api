import { CreateRuleDto } from "../dto/create-rule.dto";
import { Rule } from "../entities/rule.entity";

export interface RuleService {
    create(createRuleDto: CreateRuleDto): Promise<Rule>;
    executeRules(ruleType: string, parameters: object): Promise<void>;
    executeRuleObject(rule: string, paramName: string, parameters: object, async: boolean): Promise<void>;
}