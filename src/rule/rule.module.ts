import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Rule } from './entities/rule.entity';
import { RuleController } from './controllers/rule.controller';
import { RuleServiceImpl } from './services/rule-impl.service';

@Module({
    imports: [TypeOrmModule.forFeature([Rule])],
    controllers: [RuleController],
    providers: [
        {
            provide: 'RuleService',
            useClass: RuleServiceImpl
        }
    ],
    exports: [
        {
            provide: 'RuleService',
            useClass: RuleServiceImpl
        }
    ]
})

export class RuleModule {}