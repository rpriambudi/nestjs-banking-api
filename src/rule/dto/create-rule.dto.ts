import { IsNotEmpty, IsIn } from 'class-validator';
import { RuleTypeEnum } from './../enums/rule-type.enum';

export class CreateRuleDto {
    @IsNotEmpty()
    @IsIn(Object.values(RuleTypeEnum))
    type: string;

    @IsNotEmpty()
    function: string;

    @IsNotEmpty()
    paramName: string;

    async: boolean;
}
