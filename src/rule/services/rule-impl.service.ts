import { Rule } from './../entities/rule.entity';
import { CreateRuleDto } from './../dto/create-rule.dto';
import { RuleService } from './../interfaces/rule-service.interface';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RuleTypeEnum } from '../enums/rule-type.enum';
import { InvalidRuleTypeException } from '../exceptions/invalid-rule-type.exception';
import { RuleExaminationException } from './../exceptions/rule-examination.exception';

export class RuleServiceImpl implements RuleService {
    constructor(
        @InjectRepository(Rule) private readonly ruleRepository: Repository<Rule>
    ) {}

    async create(createRuleDto: CreateRuleDto): Promise<Rule> {
        const ruleData = this.ruleRepository.create(createRuleDto);
        await this.ruleRepository.save(ruleData);
        return ruleData;
    }
    
    async executeRules(ruleType: string, parameter: object): Promise<void> {
        const rulesData = await this.getByRuleType(ruleType);
        for (let i = 0; i < rulesData.length; i++) {
            await this.executeRuleObject(rulesData[i].function, rulesData[i].paramName, parameter, rulesData[i].async);
        }
    }

    async executeRuleObject(rule: string,paramName: string, parameters: object, async: boolean): Promise<void> {
        const ruleFunction = new Function(paramName, rule);
        try {
            if (async)
                await ruleFunction(parameters);
            else
                ruleFunction(parameters);
        } catch(error) {
            throw new RuleExaminationException(error.message);
        }
    }

    private async getByRuleType(ruleType: string) {
        const rulesData = await this.ruleRepository.find({ type: ruleType });
        return rulesData;
    }

    
}