import { Test } from '@nestjs/testing';
import { Rule } from './../entities/rule.entity';
import { CreateRuleDto } from './../dto/create-rule.dto';
import { RuleServiceImpl } from './../services/rule-impl.service';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { RuleTypeEnum } from '../enums/rule-type.enum';
import { RuleExaminationException } from './../exceptions/rule-examination.exception';

describe('RuleService test', () => {
    let ruleService: RuleServiceImpl;
    let repository: Repository<Rule>;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            providers: [
                RuleServiceImpl,
                {
                    provide: getRepositoryToken(Rule),
                    useClass: Repository
                }
            ]
        }).compile();

        ruleService = moduleRef.get<RuleServiceImpl>(RuleServiceImpl);
        repository = moduleRef.get<Repository<Rule>>(getRepositoryToken(Rule));
    })

    describe('create method', () => {
        it('Positive test. Should return one valid rule object', async () => {
            const testRuleDto: CreateRuleDto = {
                type: RuleTypeEnum.AccountCreate,
                function: 'testFunction() {}',
                paramName: 'createAccountDto',
                async: null
            }

            const testRule: Rule = {
                id: 1,
                type: RuleTypeEnum.AccountCreate,
                function: 'testFunction() {}',
                paramName: 'createAccountDto',
                async: false
            }

            jest.spyOn(repository, 'create').mockReturnValueOnce(testRule);
            jest.spyOn(repository, 'save').mockResolvedValueOnce(testRule);
            expect(await ruleService.create(testRuleDto)).toBe(testRule);
        })
    })

    describe('executeRuleObject function', () => {
        it('Positive test, sync function. Should execute the function string.', async () => {
            const ruleString: string =  "const balance = createAccountDto.balance;\n" +
                                        "if (balance < 500000)\n" +
                                        "   throw new Error('Balance insufficient')\n";
            const parameter = {balance: 500000};
            await ruleService.executeRuleObject(ruleString, 'createAccountDto', parameter, false);
        })
        it('Positive test, sync function. Should execute the function and throw error, since params not match with provided rule', async () => {
            const ruleString: string =  "const balance = createAccountDto.balance;\n" +
                                        "if (balance < 500000)\n" +
                                        "   throw new Error('Balance insufficient')\n";
            const parameter = {balance: 250000};
            try {
                await ruleService.executeRuleObject(ruleString, 'createAccountDto', parameter, false);
            } catch(error) {
                expect(error instanceof RuleExaminationException).toBe(true);
            }
        })
    })
})