import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm'

@Entity()
export class Rule {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        unique: false,
        name: 'type'
    })
    type: string;

    @Column({
        type: 'text',
        unique: false,
        name: 'function'
    })
    function: string;

    @Column({
        type: 'varchar',
        unique: false,
        name: 'param_name'
    })
    paramName: string;

    @Column({
        type: 'bool',
        unique: false,
        name: 'async',
        default: false
    })
    async: boolean;
}