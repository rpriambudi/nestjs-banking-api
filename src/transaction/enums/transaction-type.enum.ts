export enum TransactionTypeEnum {
    Transfer = 'TRF',
    Withdraw = 'DB',
    Deposit = 'CR'
}