import { Controller, Inject, Post, Body, Request, Param } from '@nestjs/common'
import { TransferService } from '../abstracts/transfer-service.abstract';
import { TellerTransferDto } from '../dto/teller-transfer.dto';
import { UserTransferDto } from '../dto/user-transfer.dto';
import { AuthContext, UserContext } from './../../auth/decorators/user-context.decorator';
import { ValidateVerificationDto } from './../../user/dto/validate-verification.dto';

@Controller()
export class TransferController {
    constructor(
        @Inject('TransferService') private readonly transferService: TransferService,
        @Inject('UserTransferService') private readonly userTransferService: TransferService
    ) {}

    @Post('/api/transfer')
    async createNewTransfer(
        @Body() createTransferDto: TellerTransferDto,
        @AuthContext() userContext: UserContext
    ) {
        createTransferDto.branchCode = userContext.branchCode;
        createTransferDto.registeredBy = userContext.email;
        const transferData = await this.transferService.createNewTransfer(createTransferDto);
        return transferData;
    }

    @Post('/api/transfer/:accountNo/customer')
    async createNewCustomerTransfer(
        @Param('accountNo') accountNo: string,
        @Body('transfer') userTransferDto: UserTransferDto,
        @Body('verification') validateVerificationDto: ValidateVerificationDto,
        @AuthContext() userContext: UserContext
    ) {
        validateVerificationDto.email = userContext.email;
        
        userTransferDto.customerAcctNo = accountNo;
        userTransferDto.registeredBy = userContext.email;
        userTransferDto.cifNo = userContext.cifNo;
        userTransferDto.validateVerificationDto = validateVerificationDto;

        const transferData = await this.userTransferService.createNewTransfer(userTransferDto);
        return transferData;
    }
}