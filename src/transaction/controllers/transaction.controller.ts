import { Controller, Get, Query, Param, Inject, Post, Body, Request } from '@nestjs/common'
import { TransactionService } from './../interfaces/transaction-service.interface'
import { SavingAdjusmentDto } from '../dto/saving-adjustment.dto';
import { AuthContext, UserContext } from './../../auth/decorators/user-context.decorator';

@Controller()
export class TransactionController {
    constructor(
        @Inject('TransactionService') private readonly transactionService: TransactionService
    ) {}

    @Get('/api/transactions/:cifNo')
    async getTransactions(@Param('cifNo') cifNo: string, @Query('dateFrom') dateFrom: string, @Query('dateTo') dateTo: string) {
        const queryParams  = {};
        if (dateFrom)
            queryParams['dateFrom'] = new Date(dateFrom);

        if (dateTo)
            queryParams['dateTo'] = new Date(dateTo);

        const transactionData = await this.transactionService.getTransactionHistories(cifNo, queryParams);
        return transactionData;
    }

    @Post('/api/transactions/withdraw')
    async createNewWithdrawal(
        @Body() savingAdjusmentDto: SavingAdjusmentDto,
        @AuthContext() userContext: UserContext
    ) {
        savingAdjusmentDto.branchCode = userContext.branchCode;
        savingAdjusmentDto.createdBy = userContext.email;
        const transactionData = await this.transactionService.requestWithdrawal(savingAdjusmentDto);
        return transactionData;
    }

    @Post('/api/transactions/deposit')
    async createNewDeposit(
        @Body() savingAdjusmentDto: SavingAdjusmentDto,
        @Body() userContext: UserContext
    ) {
        savingAdjusmentDto.branchCode = userContext.branchCode;
        savingAdjusmentDto.createdBy = userContext.email;
        const transactionData = await this.transactionService.requestDeposit(savingAdjusmentDto);
        return transactionData;
    }
}