export class UserAccountNotExist {
    public code: string;
    public customCode: string;
    public message: string;

    constructor(message: string) {
        this.code = 'USER_ACCOUNT_NOT_EXIST';
        this.customCode = '400904';
        this.message = message;
    }
}