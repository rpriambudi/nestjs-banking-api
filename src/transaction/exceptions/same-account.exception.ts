import { GenericException } from './../../exceptions/generic-exception.abstract';

export class SameAccountException extends GenericException {
    getDisplayCode(): string {
        return 'SAME_ACCOUNT';
    }

    getErrorCode(): string {
        return '400903';
    }

    constructor(message: string) {
        super(message);
    }
}