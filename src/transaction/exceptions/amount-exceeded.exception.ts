import { GenericException } from './../../exceptions/generic-exception.abstract';

export class AmountExceededException extends GenericException {
    getDisplayCode(): string {
        return 'AMOUNT_EXCEEDED';
    }

    getErrorCode(): string {
        return '400901';
    }

    constructor(message: string) {
        super(message);
    }
}