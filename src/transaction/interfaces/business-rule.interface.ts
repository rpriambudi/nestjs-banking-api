import { BaseTransferDto } from "../dto/base-transfer.dto";

export interface BusinessRule {
    validate(createTransferDto: BaseTransferDto): Promise<void>;
}