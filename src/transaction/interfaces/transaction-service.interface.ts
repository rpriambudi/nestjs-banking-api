import { Transaction } from "../entities/transaction.entity";
import { SavingAdjusmentDto } from '../dto/saving-adjustment.dto';

export interface TransactionService {
    requestWithdrawal(savingAdjusmentDto: SavingAdjusmentDto): Promise<Transaction>;
    requestDeposit(savingAdjusmentDto: SavingAdjusmentDto): Promise<Transaction>;
    getTransactionHistories(cifNo: string, queryParams: object): Promise<Transaction[]>;
}