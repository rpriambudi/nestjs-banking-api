import { Test } from '@nestjs/testing';
import { TransferGeneralRule } from './transfer-general-rule.service';
import { BaseTransferDto } from '../dto/base-transfer.dto';
import { Account } from './../../account/entities/account.entity';
import { NegativeAmountException } from '../exceptions/negative-amount.exception';
import { SameAccountException } from './../exceptions/same-account.exception'

describe('TransferGeneralRule class', () => {
    let transferGeneralRule: TransferGeneralRule;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            providers: [TransferGeneralRule]
        }).compile();
        
        transferGeneralRule = moduleRef.get<TransferGeneralRule>(TransferGeneralRule);
    })

    describe('validate method', () => {
        it('Negative test, negative amount. Should throw NegativeAmountException', async () => {
            const transferDto: BaseTransferDto = {
                amount: -100000,
                description: 'Test Transfer',
                requesterAcctNo: '0010000000001',
                destinationAcctNo: '001000000002',
                createdBy: 'Test',
                registeredBy: 'test@mail.co'
            }

            try {
                await transferGeneralRule.validate(transferDto);
            } catch (error) {
                expect(error instanceof NegativeAmountException).toBe(true);
            }
        })
        it ('Negative test, same account. Should throw SameAccountException', async () => {
            const transferDto: BaseTransferDto = {
                amount: 1000000,
                description: 'Test Transfer',
                requesterAcctNo: '0010000000001',
                destinationAcctNo: '0010000000001',
                createdBy: 'Test',
                registeredBy: 'test@mail.co'
            }

            try {
                await transferGeneralRule.validate(transferDto);
            } catch (error) {
                expect(error instanceof SameAccountException).toBe(true);
            }
        })
    })
})