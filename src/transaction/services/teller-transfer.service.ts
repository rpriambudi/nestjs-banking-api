
import { Repository } from "typeorm";
import { TransferService } from "../abstracts/transfer-service.abstract";
import { TellerTransferDto } from '../dto/teller-transfer.dto';
import { Transaction } from '../entities/transaction.entity';
import { AccountService } from "../../account/interfaces/account-service.interface";
import { BranchService } from '../../branch/interfaces/branch-service.interface';
import { BalanceService } from '../../account/interfaces/balance-service.interface';
import { TransferRuleContext } from '../context/transfer-rule.context';

export class TellerTransferService extends TransferService {
    constructor(
        private readonly branchService: BranchService,
        transactionRepository: Repository<Transaction>,
        accountService: AccountService,
        balanceService: BalanceService,
        transferRule: TransferRuleContext
    ) {
        super(transactionRepository, accountService, balanceService, transferRule);
    }

    async createNewTransfer(createTransferDto: TellerTransferDto): Promise<Transaction> {
        const branchData = await this.branchService.findBranchByCode(createTransferDto.branchCode);
        createTransferDto.createdBy = branchData.branchCode;
        return await super.createNewTransfer(createTransferDto);
    }
}