import { TransactionService } from './../interfaces/transaction-service.interface';
import { Transaction } from './../entities/transaction.entity';
import { getRepository, SelectQueryBuilder } from 'typeorm';
import { Injectable, Inject } from '@nestjs/common';
import { v4 as uuid } from 'uuid';
import { SavingAdjusmentDto } from './../dto/saving-adjustment.dto';
import { CustomerQueryService } from './../../customers/interfaces/customer-query-service.interface';
import { AccountService } from './../../account/interfaces/account-service.interface';
import { BalanceService } from './../../account/interfaces/balance-service.interface';
import { BranchService } from './../../branch/interfaces/branch-service.interface';
import { TransactionTypeEnum } from '../enums/transaction-type.enum';
import { Branch } from './../../branch/entities/branch.entity';

@Injectable()
export class TransactionServiceImpl implements TransactionService {
    constructor(
        @Inject('CustomerQueryService') private readonly customerService: CustomerQueryService,
        @Inject('AccountService') private readonly accountService: AccountService,
        @Inject('BalanceService') private readonly balanceService: BalanceService,
        @Inject('BranchService') private readonly branchService: BranchService
    ) {}
    async getTransactionHistories(cifNo: string, queryParams: object): Promise<Transaction[]> {
        const customerData = await this.customerService.findCustomerByCifNo(cifNo);
        const accountData = await this.accountService.findAccountByCustomerId(customerData.id);
        const queryMapping = {
            'dateFrom': this.buildDateFromQuery,
            'dateTo': this.buildDateToQuery
        }
        let result = [];

        for (const account of accountData) {
            const queryBuilder = getRepository(Transaction).createQueryBuilder('transaction');
            this.buildAccountNoQuery(queryBuilder, account.accountNo);

            for (const attribute in queryParams) {
                if (queryParams[attribute]) {
                    queryMapping[attribute](queryBuilder, queryParams[attribute]);
                }
            }
            result = result.concat(await queryBuilder.getMany());
        }
        return result;
    }

    async requestWithdrawal(savingAdjusmentDto: SavingAdjusmentDto) {
        return await this.savingAdjustment(savingAdjusmentDto, TransactionTypeEnum.Withdraw);
    }

    async requestDeposit(savingAdjusmentDto: SavingAdjusmentDto) {
        return await this.savingAdjustment(savingAdjusmentDto, TransactionTypeEnum.Deposit);
    }

    private async savingAdjustment(savingAdjusmentDto: SavingAdjusmentDto, type: string) {
        const accountNo = savingAdjusmentDto.accountNo;
        const branchCode = savingAdjusmentDto.branchCode;
        const amount = savingAdjusmentDto.amount;
        const transactionRepository = getRepository(Transaction);
        const adjusmentType = {
            'DB': ['-', '+'],
            'CR': ['+', '-']
        }

        let customerData: object;
        let branchData: Branch;
        let transactionData: Transaction;
        try {
            if (type === TransactionTypeEnum.Withdraw) {
                customerData = await this.balanceService.substractAccountBalance(accountNo, amount);
            } else if (type === TransactionTypeEnum.Deposit) {
                customerData = await this.balanceService.addAccountBalance(accountNo, amount);
            }
            branchData = await this.adjustBranchAccount(branchCode, type, amount);
            transactionData = transactionRepository.create({
                transactionId: uuid(),
                transactionType: type,
                requesterAcctNo: accountNo,
                amount: amount,
                createdBy: branchData.branchCode,
                registeredBy: savingAdjusmentDto.createdBy
            });
            await transactionRepository.save(transactionData);

            return transactionData;
        } catch (error) {
            if (type === TransactionTypeEnum.Withdraw) {
                if (customerData)
                    await this.balanceService.addAccountBalance(accountNo, amount);

                if (branchData)
                    await this.adjustBranchAccount(branchCode, TransactionTypeEnum.Deposit, amount);
            } else if (type === TransactionTypeEnum.Deposit) {
                if (customerData)
                    await this.balanceService.substractAccountBalance(accountNo, amount);

                if (branchData)
                    await this.adjustBranchAccount(branchCode, TransactionTypeEnum.Withdraw, amount);
            }
            
            throw error;
        }
    }

    private async adjustBranchAccount(branchCode: string, adjusmentType: string, amount: number) {
        const branchData = await this.branchService.findBranchByCode(branchCode);
        const customerData = await this.customerService.findCustomerByCifNo(branchCode);
        const accountData = await this.accountService.findAccountByCustomerId(customerData.id);
        branchData['accountNo'] = accountData[0].accountNo;
        
        if (adjusmentType === TransactionTypeEnum.Withdraw) {
            await this.balanceService.substractAccountBalance(accountData[0].accountNo, amount);
        } else if (adjusmentType === TransactionTypeEnum.Deposit) {
            await this.balanceService.addAccountBalance(accountData[0].accountNo, amount);
        }
        return branchData;
    }

    private buildAccountNoQuery(queryBuilder: SelectQueryBuilder<Transaction>, accountNo: string): void {
        queryBuilder.andWhere('(requester_acct_no = :acctNo or destination_acct_no = :acctNo)', { acctNo: accountNo });
        return;
    }

    private buildDateFromQuery(queryBuilder: SelectQueryBuilder<Transaction>, dateFrom: Date): void {
        queryBuilder.andWhere('created_at >= :dateFrom', { dateFrom: dateFrom });
        return;
    }

    private buildDateToQuery(queryBuilder: SelectQueryBuilder<Transaction>, dateTo: Date): void {
        queryBuilder.andWhere('created_at <= :dateTo', { dateTo: dateTo });
        return;
    }
}