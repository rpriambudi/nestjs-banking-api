import { Repository } from 'typeorm';
import { UserTransferDto } from './../dto/user-transfer.dto';
import { UserAccountDto } from './../dto/user-account.dto';
import { BaseTransferDto } from './../dto/base-transfer.dto';
import { Transaction } from './../entities/transaction.entity';
import { TransferService } from '../abstracts/transfer-service.abstract';
import { TransferRuleContext } from './../context/transfer-rule.context';
import { CustomerQueryService } from './../../customers/interfaces/customer-query-service.interface';
import { AccountService } from './../../account/interfaces/account-service.interface';
import { BalanceService } from './../../account/interfaces/balance-service.interface';
import { ValidateVerificationDto } from './../../user/dto/validate-verification.dto';
import { ValidateVerificationContext } from './../../user/context/validate-verification.context';

export class UserTransferService extends TransferService {
    constructor(
        private readonly customerService: CustomerQueryService,
        private readonly customerValidation: TransferRuleContext,
        private readonly validateVerificationContext: ValidateVerificationContext,
        transferRepository: Repository<Transaction>,
        accountService: AccountService,
        balanceService: BalanceService,
        transactionValidation: TransferRuleContext,
    ) {
        super(transferRepository, accountService, balanceService, transactionValidation);
    }

    async createNewTransfer(userTransferDto: UserTransferDto): Promise<Transaction> {
        const customer = await this.customerService.findCustomerByCifNo(userTransferDto.cifNo);
        const verificationDto: ValidateVerificationDto = userTransferDto.validateVerificationDto;
        verificationDto.customerId = customer.id;

        await this.validateVerificationContext.validate(verificationDto);
        await this.customerValidation.validate(new UserAccountDto(customer.id, userTransferDto.customerAcctNo));

        const transferDto:BaseTransferDto = UserTransferDto.toBaseTransfer(userTransferDto);
        return await super.createNewTransfer(transferDto);
    }
}