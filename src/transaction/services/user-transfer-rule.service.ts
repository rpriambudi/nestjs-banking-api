import { BusinessRule } from '../interfaces/business-rule.interface';
import { UserAccountDto } from './../dto/user-account.dto';
import { UserAccountNotExist } from './../exceptions/user-account-not-exist.exception';
import { AccountService } from './../../account/interfaces/account-service.interface';

export class UserTransferRule implements BusinessRule {
    constructor(
        private readonly accountService: AccountService
    ) {}

    async validate(userAccountDto: UserAccountDto): Promise<void> {
        const userAccounts = await this.accountService.findAccountByCustomerId(userAccountDto.customerId);
        if (!userAccounts || userAccounts.length === 0)
            throw new UserAccountNotExist('Customer does not have any account.');

        let exist = false;
        for (const userAccount of userAccounts) {
            if (userAccount.accountNo === userAccountDto.customerAcctNo) {
                exist = true;
                break;
            }
        }

        if (!exist)
            throw new UserAccountNotExist('Customer account does not match.');
    }

}