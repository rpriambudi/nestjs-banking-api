import { Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TellerTransferService } from './teller-transfer.service';
import { AccountService } from '../../account/interfaces/account-service.interface';
import { AccountServiceImpl } from '../../account/services/account-impl.service';
import { BalanceService } from '../../account/interfaces/balance-service.interface';
import { BalanceServiceImpl } from '../../account/services/balance-impl.service';
import { Transaction } from '../entities/transaction.entity';
import { TellerTransferDto } from '../dto/teller-transfer.dto';
import { BranchServiceImpl } from '../../branch/services/branch-impl.service';
import { BranchService } from '../../branch/interfaces/branch-service.interface';
import { Branch } from '../../branch/entities/branch.entity';
import { Account } from '../../account/entities/account.entity';
import { StatusEnum } from '../../account/enums/status.enum';
import { TransferRuleContext } from '../context/transfer-rule.context';
import { TransactionTypeEnum } from '../enums/transaction-type.enum';
jest.mock('./../../account/services/account-impl.service');
jest.mock('./../../branch/services/branch-impl.service');
jest.mock('./../context/transfer-rule.context');
jest.mock('./../../account/services/balance-impl.service');

function generateCommonObject(): object {
    const transferDto: TellerTransferDto = {
        branchCode: '001',
        amount: 250000,
        requesterAcctNo: '0010000000001',
        destinationAcctNo: '001000000002',
        description: 'Test Transfer',
        createdBy: '001',
        registeredBy: 'Test@mail.co'
    }
    const testBranch: Branch = {
        id: 1,
        branchCode: '001',
        name: 'Test Branch',
        address: 'Test Branch'
    }
    const transferData: Transaction = {
        id: 1,
        transactionType: TransactionTypeEnum.Transfer,
        transactionId: '2sdadasdasd',
        amount: 250000,
        requesterAcctNo: '0010000000001',
        destinationAcctNo: '0010000000002',
        description: 'Test Transfer',
        createdBy: testBranch.name,
        registeredBy: transferDto.registeredBy,
        createdAt: new Date()
    }

    return {transferDto, testBranch, transferData}
}

describe('TellerTransferService class', () => {
    let transferService: TellerTransferService;
    let transferRule: TransferRuleContext;
    let accountService: AccountService;
    let branchService: BranchService;
    let balanceService: BalanceService;
    let repository: Repository<Transaction>;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            providers: [
                {
                    provide: 'AccountService',
                    useClass: AccountServiceImpl
                },
                {
                    provide: 'BranchService',
                    useClass: BranchServiceImpl
                },
                {
                    provide: 'TransferService',
                    useFactory: (transactionRepository, accountService, balanceService, branchService, transferRule) => {
                        return new TellerTransferService(branchService, transactionRepository, accountService, balanceService, transferRule);
                    },
                    inject: [getRepositoryToken(Transaction), 'AccountService', 'BalanceService', 'BranchService', 'TransferRule']
                },
                {
                    provide: getRepositoryToken(Transaction),
                    useClass: Repository
                },
                {
                    provide: 'TransferRule',
                    useClass: TransferRuleContext
                },
                {
                    provide: 'BalanceService',
                    useClass: BalanceServiceImpl
                }
            ]
        }).compile();

        repository = moduleRef.get<Repository<Transaction>>(getRepositoryToken(Transaction));
        transferService = moduleRef.get<TellerTransferService>('TransferService');
        accountService = moduleRef.get<AccountService>('AccountService');
        branchService = moduleRef.get<BranchService>('BranchService');
        transferRule = moduleRef.get<TransferRuleContext>('TransferRule');
        balanceService = moduleRef.get<BalanceService>('BalanceService');
    })

    describe('create method', () => {
        it('Positive test. should return one valid Transaction object', async () => {
            const commonObject = generateCommonObject();
            const transferData = commonObject['transferData'];
            const transferDto = commonObject['transferDto'];
            const testBranch = commonObject['testBranch'];

            jest.spyOn(repository, 'create').mockReturnValueOnce(transferData);
            jest.spyOn(repository, 'save').mockResolvedValueOnce(transferData);
            jest.spyOn(branchService, 'findBranchByCode').mockResolvedValueOnce(testBranch);
            jest.spyOn(transferRule, 'validate').mockImplementationOnce(async () => {
                return;
            });
            jest.spyOn(balanceService, 'substractAccountBalance').mockResolvedValueOnce({
                id: 1,
                accountNo: '0100000000001',
                openedAt: 'KCP Thamrin',
                balance: 250000,
                status: StatusEnum.Live,
                branchId: 1,
                customerId: 1
            });
            jest.spyOn(balanceService, 'addAccountBalance').mockResolvedValueOnce({
                id: 2,
                accountNo: '0100000000002',
                openedAt: 'KCP Thamrin',
                balance: 550000,
                status: StatusEnum.Live,
                branchId: 1,
                customerId: 1
            });
            jest.spyOn(accountService, 'findAccountByAccountNo').mockImplementation(async (accountNo: string): Promise<Account> => {
                const testAccount: Account = {
                    id: 1,
                    accountNo: '0100000000001',
                    openedAt: 'KCP Thamrin',
                    balance: 500000,
                    status: StatusEnum.Live,
                    branchId: 1,
                    customerId: 1
                }
                const testAccount2: Account = {
                    id: 2,
                    accountNo: '0100000000002',
                    openedAt: 'KCP Thamrin',
                    balance: 300000,
                    status: StatusEnum.Live,
                    branchId: 1,
                    customerId: 1
                }

                if (accountNo === '0100000000001')
                    return testAccount;
                else 
                    return testAccount2;
            });

            expect(await transferService.createNewTransfer(transferDto)).toBe(transferData);
        })
        it('Negative test, amount is negative. Should throw NegativeAmountException', async () => {
            const commonObject = generateCommonObject();
            const transferData = commonObject['transferData'];
            const transferDto = commonObject['transferDto'];
            const testBranch = commonObject['testBranch'];
        })
    })    
})