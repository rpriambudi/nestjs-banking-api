import { Repository } from 'typeorm';
import { v4 as uuid } from 'uuid';
import { Transaction } from "../entities/transaction.entity";
import { BaseTransferDto } from '../dto/base-transfer.dto';
import { TransactionTypeEnum } from '../enums/transaction-type.enum';
import { TransferRuleContext } from '../context/transfer-rule.context';
import { AccountService } from '../../account/interfaces/account-service.interface';
import { BalanceService } from '../../account/interfaces/balance-service.interface';

export abstract class TransferService {
    constructor(
        private readonly transactionRepository: Repository<Transaction>,
        private readonly accountService: AccountService,
        private readonly balanceService: BalanceService,
        private readonly transferRule: TransferRuleContext
    ) {}

    async createNewTransfer(createTransferDto: BaseTransferDto): Promise<Transaction> {
        const requesterAcct = await this.accountService.findAccountByAccountNo(createTransferDto.requesterAcctNo);
        const destinationAcct = await this.accountService.findAccountByAccountNo(createTransferDto.destinationAcctNo);
        await this.transferRule.validate(createTransferDto);

        let updatedRequesterAcct = null;
        let updatedDestinationAcct = null;

        try {
            updatedRequesterAcct = 
                await this
                        .balanceService
                        .substractAccountBalance(createTransferDto.requesterAcctNo, createTransferDto.amount);
            updatedDestinationAcct = 
                await this
                        .balanceService
                        .addAccountBalance(createTransferDto.destinationAcctNo, createTransferDto.amount);

            const transactionData = this.transactionRepository.create(createTransferDto);
            transactionData.transactionId = uuid();
            transactionData.transactionType = TransactionTypeEnum.Transfer;

            await this.transactionRepository.save(transactionData);
            return transactionData;
        } catch (error) {
            if (updatedRequesterAcct) {
                await this
                        .balanceService
                        .updateAccountBalance(createTransferDto.requesterAcctNo, requesterAcct.balance);
            }

            if (updatedDestinationAcct) {
                await this
                        .balanceService
                        .updateAccountBalance(createTransferDto.destinationAcctNo, destinationAcct.balance);
            }

            throw error;
        }
    }
}