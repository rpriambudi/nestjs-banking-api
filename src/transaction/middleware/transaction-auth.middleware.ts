import { AuthStrategyMiddleware } from './../../auth/middleware/auth-strategy.middleware';

export class TransactionAuthMiddleware extends AuthStrategyMiddleware {
    getStrategyName(): string {
        return 'JWT';
    }

    getGuards(): string[] {
        return ['Teller'];
    }
}