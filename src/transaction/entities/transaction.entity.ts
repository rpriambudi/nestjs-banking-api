import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class Transaction {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        unique: true,
        name: 'transaction_id'
    })
    transactionId: string;

    @Column({
        type: 'varchar',
        unique: false,
        name: 'transaction_type'
    })
    transactionType: string

    @Column({
        type: 'varchar',
        unique: false,
        name: 'requester_acct_no'
    })
    requesterAcctNo: string;

    @Column({
        type: 'bigint',
        unique: false,
        name: 'amount'
    })
    amount: number

    @Column({
        type: 'varchar',
        unique: false,
        nullable: true,
        name: 'destination_acct_no'
    })
    destinationAcctNo: string

    @Column({
        type: 'text',
        unique: false,
        nullable: true,
        name: 'description'
    })
    description: string

    @Column({
        type: 'varchar',
        unique: false,
        name: 'created_by'
    })
    createdBy: string;

    @Column({
        type: 'varchar',
        unique: false,
        name: 'registered_by',
        default: 'teller@mail.co'
    })
    registeredBy: string;

    @Column({
        type: 'timestamp',
        unique: false,
        name: 'created_at',
        default: new Date()
    })
    createdAt: Date;
}