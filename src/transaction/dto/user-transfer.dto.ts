import { IsIn } from 'class-validator';
import { BaseTransferDto } from './base-transfer.dto';
import { ClientTypeEnum } from './../../user/enums/client-type.enum';
import { ValidateVerificationDto } from './../../user/dto/validate-verification.dto';

export class UserTransferDto extends BaseTransferDto {
    @IsIn(Object.values(ClientTypeEnum))
    clientType: string;

    cifNo: string;

    customerAcctNo: string;

    validateVerificationDto: ValidateVerificationDto;

    static toBaseTransfer(userTransferDto: UserTransferDto): BaseTransferDto {
        const baseTransferDto = new BaseTransferDto();
        baseTransferDto.amount = userTransferDto.amount;
        baseTransferDto.destinationAcctNo = userTransferDto.destinationAcctNo;
        baseTransferDto.requesterAcctNo = userTransferDto.customerAcctNo;
        baseTransferDto.description = userTransferDto.description;
        baseTransferDto.createdBy = userTransferDto.clientType;
        baseTransferDto.registeredBy = userTransferDto.registeredBy;

        return baseTransferDto;
    }
}