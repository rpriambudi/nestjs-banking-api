import { BaseTransferDto } from './base-transfer.dto';

export class UserAccountDto extends BaseTransferDto{
    customerId: number;

    customerAcctNo: string;

    constructor(customerId: number, customerAcctNo: string) {
        super();
        this.customerId = customerId;
        this.customerAcctNo = customerAcctNo;
    }
}