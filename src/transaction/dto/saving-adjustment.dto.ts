import { IsNotEmpty, IsPositive } from 'class-validator';

export class SavingAdjusmentDto {
    @IsNotEmpty()
    accountNo: string;

    @IsNotEmpty()
    @IsPositive()
    amount: number;

    branchCode: string;

    createdBy: string;
}