import { BaseTransferDto } from './base-transfer.dto';

export class TellerTransferDto extends BaseTransferDto {
    branchCode: string
}