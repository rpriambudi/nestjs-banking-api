import { DynamicModule, NestModule, MiddlewareConsumer, RequestMethod } from '@nestjs/common'
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import { Transaction } from './entities/transaction.entity';
import { BranchModule } from './../branch/branch.module';
import { AccountModule } from './../account/account.module';
import { TellerTransferService } from './services/teller-transfer.service';
import { TransferController } from './controllers/transfer.controller';
import { TransactionServiceImpl } from './services/transaction-impl.service';
import { TransactionController } from './controllers/transaction.controller';
import { CustomerModule } from './../customers/customer.module';
import { TransferRuleContext } from './context/transfer-rule.context';
import { TransferGeneralRule } from './services/transfer-general-rule.service';
import { TransferAccountRule } from './services/transfer-account-rule.service';
import { BusinessRule } from './interfaces/business-rule.interface';
import { AuthModule } from './../auth/auth.module';
import { TransactionAuthMiddleware } from './middleware/transaction-auth.middleware';
import { CustomerAuthMiddleware } from './middleware/customer-auth.middleware';
import { UserTransferService } from './services/user-transfer.service';
import { UserTransferRule } from './services/user-transfer-rule.service';
import { UserModule } from './../user/user.module';

export class TransactionModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(TransactionAuthMiddleware)
            .forRoutes(TransactionController)

        consumer
            .apply(TransactionAuthMiddleware)
            .exclude(
                {
                    path: "/api/transfer/:accountNo/customer",
                    method: RequestMethod.POST
                }
            )
            .forRoutes(TransferController)

        consumer
            .apply(CustomerAuthMiddleware)
            .forRoutes(
                {
                    path: "/api/transfer/:accountNo/customer",
                    method: RequestMethod.POST
                }
            )
    }

    static forRoot(): DynamicModule {
        return {
            module: TransactionModule,
            imports: [
                TypeOrmModule.forFeature([Transaction]),
                BranchModule,
                AccountModule,
                CustomerModule,
                AuthModule,
                UserModule
            ],
            controllers: [TransferController, TransactionController],
            providers:[
                {
                    provide: 'TransactionService',
                    useClass: TransactionServiceImpl
                },
                {
                    provide: 'TransferService',
                    useFactory: (transactionRepository, accountService, balanceService, branchService) => {
                        const ruleList: BusinessRule[] = [
                            new TransferAccountRule(),
                            new TransferGeneralRule()
                        ];

                        return new TellerTransferService(
                            branchService, 
                            transactionRepository,
                            accountService, 
                            balanceService, 
                            new TransferRuleContext(ruleList)
                        );
                    },
                    inject: [getRepositoryToken(Transaction), 'AccountService', 'BalanceService', 'BranchService']
                },
                {
                    provide: 'UserTransferService',
                    useFactory: (
                        transactionRepository, 
                        accountService, 
                        balanceService, 
                        customerQueryService,
                        validateVerificationRule,
                        ) => {
                        const generalRule: BusinessRule[] = [
                            new TransferAccountRule(),
                            new TransferGeneralRule()
                        ];
                        return new UserTransferService(
                            customerQueryService,
                            new TransferRuleContext([new UserTransferRule(accountService)]),
                            validateVerificationRule,
                            transactionRepository,
                            accountService,
                            balanceService,
                            new TransferRuleContext(generalRule)
                        );
                    },
                    inject: [
                        getRepositoryToken(Transaction), 
                        'AccountService', 
                        'BalanceService', 
                        'CustomerQueryService', 
                        'ValidateVerificationRule'
                    ]
                }
            ]
        }
    }
}