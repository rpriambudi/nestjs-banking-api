import { Injectable } from '@nestjs/common';
import { BusinessRule } from '../interfaces/business-rule.interface';
import { BaseTransferDto } from '../dto/base-transfer.dto';

@Injectable()
export class TransferRuleContext implements BusinessRule {
    private ruleList: BusinessRule[];

    constructor(ruleList: BusinessRule[]) {
        this.ruleList = ruleList;
    }

    async validate(createTransferDto: BaseTransferDto) {
        for (const ruleObj of this.ruleList) {
            await ruleObj.validate(createTransferDto);
        }
    }
}