import { Injectable, Inject } from '@nestjs/common'
import { Customer } from './../entities/customer.entity';
import { BusinessRule } from '../interfaces/business-rule.interface';

@Injectable()
export class CreateRuleContext implements BusinessRule{
    private ruleList: BusinessRule[];

    constructor(ruleList: BusinessRule[]) {
        this.ruleList = ruleList;
    }

    async validate(customerData: Customer): Promise<void> {
        for (const ruleObj of this.ruleList) {
            await ruleObj.validate(customerData);
        }
    }
}