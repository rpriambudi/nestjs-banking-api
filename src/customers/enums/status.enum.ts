export enum StatusEnum {
    Live = 'LIVE',
    Blacklisted = 'BLACKLISTED',
    OnApproval = 'ON_APPROVAL',
    Pending = 'PENDING'
}