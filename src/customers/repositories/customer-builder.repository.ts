import { CustomerRepository } from './customer.repository';
import { Customer } from './../entities/customer.entity';
import { CreateCustomerDto } from './../dto/create-customer.dto';
import { StatusEnum } from './../enums/status.enum';
import { CifNoGenerator } from './../interfaces/cif-no-generator.interface';
import { BusinessRule } from './../interfaces/business-rule.interface';
import { BranchService } from './../../branch/interfaces/branch-service.interface';

export class CustomerBuilderRepository {
    constructor(
        private readonly customerRepository: CustomerRepository,
        private readonly branchService: BranchService,
        private readonly cifNoGenerator: CifNoGenerator,
        private readonly newCustomerValidation: BusinessRule
    ) {}

    async buildNewCustomerData(createCustomerDto: CreateCustomerDto): Promise<Customer> {
        const branchData = await this.branchService.findBranchByCode(createCustomerDto.branchCode);
        const customerData = this.customerRepository.create(createCustomerDto);
        await this.newCustomerValidation.validate(customerData);

        customerData.branchId = branchData.id;
        customerData.status = StatusEnum.Live;
        customerData.cifNo = await this.cifNoGenerator.generateNewNumber(branchData.branchCode, customerData.idNumber);
        return customerData;
    }
}