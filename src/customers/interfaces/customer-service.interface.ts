import { CreateCustomerDto } from "../dto/create-customer.dto";
import { Customer } from "../entities/customer.entity";

export interface CustomerService {
    createNewCustomer(customerDto: CreateCustomerDto): Promise<Customer>;
    blacklistCustomer(cifNo: string): Promise<Customer>;
    unblacklistCustomer(cifNo: string): Promise<Customer>;
    approveCustomer(cifNo: string): Promise<Customer>;
}