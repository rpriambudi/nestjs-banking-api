import { Customer } from './../entities/customer.entity';

export interface CustomerQueryService {
    searchCustomer(queryParam: object): Promise<Customer[]>;
    findCustomerByCifNo(cifNo: string): Promise<Customer>;
    findCustomerByIdTypeAndNo(idType: string, idNumber: string): Promise<Customer>;
    findValidCustomerBlacklistData(cifNo: string): Promise<Customer>;
    findValidCustomerUnblacklistData(cifNo: string): Promise<Customer>;
    findValidCustomerApproveData(cifNo: string): Promise<Customer>;
}