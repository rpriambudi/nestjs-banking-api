import { Customer } from "../entities/customer.entity";

export interface BusinessRule {
    validate(customerData: Customer): Promise<void>;
}