import { Customer } from './../entities/customer.entity';
import { Account } from './../../account/entities/account.entity';
import { CreateAccountDto } from './../../account/dto/create-account.dto';
import { CheckBalanceDto } from './../../account/dto/check-balance.dto';

export interface CustomerAccountService {
    getCustomerBalance(cifNo: string): Promise<CheckBalanceDto[]>;
    createNewCustomerAccount(cifNo: string, createAccountDto: CreateAccountDto): Promise<Account>;
    blockCustomerAccounts(customerData: Customer): Promise<Account[]>;
    unblockCustomerAccounts(customerData: Customer): Promise<Account[]>;
}