import { UserRegistrationDto } from './../../user/dto/user-registration.dto';

export class GenerateCifNoCommand {
    constructor(
        public readonly stateId: number,
        public readonly userRegistrationDto: UserRegistrationDto
    ) {}
}