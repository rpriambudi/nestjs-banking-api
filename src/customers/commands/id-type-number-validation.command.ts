import { UserRegistrationDto } from '../../user/dto/user-registration.dto';

export class IdTypeNumberValidationCommand {
    constructor(
        public readonly userRegistrationDto: UserRegistrationDto
    ) {}
}