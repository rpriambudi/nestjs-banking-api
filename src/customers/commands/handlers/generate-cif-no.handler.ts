import { CommandHandler, ICommandHandler, EventBus } from "@nestjs/cqrs";
import { ConfigService } from "@nestjs/config";
import { Inject } from "@nestjs/common";
import { GenerateCifNoCommand } from './../generate-cif-no.command';
import { CifNoGenerator } from './../../interfaces/cif-no-generator.interface';
import { CifNoGeneratedEvent } from './../../../user/events/cif-no-generated.event';
import { RegistrationFailedEvent } from "./../../../user/events/registration-failed.event";

@CommandHandler(GenerateCifNoCommand)
export class GenerateCifNoHandler implements ICommandHandler<GenerateCifNoCommand> {
    constructor(
        @Inject('CifNoGenerator') private readonly cifNoGenerator: CifNoGenerator,
        private readonly eventBus: EventBus,
        private readonly configService: ConfigService
    ) {}

    async execute(command: GenerateCifNoCommand): Promise<any> {
        const { userRegistrationDto, stateId } = command;
        const customer = userRegistrationDto.customer;
        const branchCode = this.configService.get<string>('branch.clientId');

        try {
            const cifNo = await this.cifNoGenerator.generateNewNumber(branchCode, customer.idNumber);
            this.eventBus.publish(new CifNoGeneratedEvent(stateId, cifNo));
        } catch (error) {
            this.eventBus.publish(new RegistrationFailedEvent(stateId, error.message));
        }
        
    }

}