import { CommandHandler, ICommandHandler, EventBus } from "@nestjs/cqrs";
import { InjectRepository } from "@nestjs/typeorm";
import { SetCustomerToLiveCommand } from './../set-customer-to-live.command';
import { Customer } from './../../entities/customer.entity';
import { StatusEnum } from './../../enums/status.enum';
import { CustomerRepository } from './../../repositories/customer.repository';
import { CustomerUpdatedToLiveEvent } from './../../../user/events/customer-updated-to-live.event';

@CommandHandler(SetCustomerToLiveCommand)
export class SetCustomerToLiveHandler implements ICommandHandler<SetCustomerToLiveCommand> {
    constructor(
        @InjectRepository(Customer) private readonly customerRepository: CustomerRepository,
        private readonly eventBus: EventBus
    ) {}

    async execute(command: SetCustomerToLiveCommand): Promise<any> {
        const { cifNo, userRegistrationDto } = command;
        const customer = await this.customerRepository.findOne({id: userRegistrationDto.customerId});

        customer.cifNo = cifNo;
        customer.status = StatusEnum.Live;
        await this.customerRepository.save(customer);

        this.eventBus.publish(new CustomerUpdatedToLiveEvent(userRegistrationDto));
    }
    
}