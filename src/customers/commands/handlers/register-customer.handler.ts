import { CommandHandler, ICommandHandler, EventBus } from "@nestjs/cqrs";
import { InjectRepository } from "@nestjs/typeorm";
import { RegisterCustomerCommand } from './../register-customer.command';
import { Customer } from "./../../entities/customer.entity";
import { StatusEnum } from "./../../enums/status.enum";
import { CustomerRepository } from './../../repositories/customer.repository';
import { ClientTypeEnum } from './../../../user/enums/client-type.enum';
import { CustomerCreatedEvent } from './../../../user/events/customer-created.event';
import { RegistrationFailedEvent } from './../../../user/events/registration-failed.event';
import { ConfigService } from "@nestjs/config";

@CommandHandler(RegisterCustomerCommand)
export class RegisterCustomerHandler implements ICommandHandler<RegisterCustomerCommand> {
    constructor(
        @InjectRepository(Customer) private readonly customerRepository: CustomerRepository,
        private readonly configService: ConfigService,
        private readonly eventBus: EventBus
    ) {}

    async execute(command: RegisterCustomerCommand): Promise<any> {
        const { userRegistrationDto, stateId } = command;
        const customer: Customer = this.customerRepository.create(userRegistrationDto.customer);
        const branchId = Number(this.configService.get<string>('branch.clientId'));

        try {
            customer.cifNo = userRegistrationDto.cifNo;
            customer.branchId = branchId;
            customer.status = StatusEnum.Live;
            customer.registeredBy = ClientTypeEnum.InternetBanking;

            await this.customerRepository.save(customer);
            this.eventBus.publish(new CustomerCreatedEvent(stateId, customer.id));
        } catch (error) {
            this.eventBus.publish(new RegistrationFailedEvent(stateId, error.message));
        }
    }

}