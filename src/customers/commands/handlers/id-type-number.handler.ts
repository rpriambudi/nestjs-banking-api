import { CommandHandler, ICommandHandler, EventBus } from "@nestjs/cqrs";
import { Inject } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { IdTypeNumberValidationCommand } from './../id-type-number-validation.command';
import { Customer } from './../../entities/customer.entity';
import { CustomerRepository } from './../../repositories/customer.repository';
import { CreateRuleContext } from './../../context/create-rule.context';
import { IdTypeValidationSuccessEvent } from './../../../user/events/id-type-validation-success.event';
import { IdTypeValidationFailedEvent } from './../../../user/events/id-type-validation-failed.event';

@CommandHandler(IdTypeNumberValidationCommand)
export class IdTypeNumberHandler implements ICommandHandler<IdTypeNumberValidationCommand>{
    constructor(
        @InjectRepository(Customer) private readonly customerRepository: CustomerRepository,
        @Inject('NewCustomerValidation') private readonly newCustomerValidation: CreateRuleContext,
        private readonly eventBus: EventBus
    ) {}

    async execute(command: IdTypeNumberValidationCommand): Promise<any> {
        const { userRegistrationDto } = command;
        
        try {
            const customer = this.customerRepository.create(userRegistrationDto.customer);
            await this.newCustomerValidation.validate(customer);

            this.eventBus.publish(new IdTypeValidationSuccessEvent(userRegistrationDto));
        } catch (error) {
            this.eventBus.publish(new IdTypeValidationFailedEvent(userRegistrationDto, error));
        }
    }

}