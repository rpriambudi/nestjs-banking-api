import { UserRegistrationDto } from './../../user/dto/user-registration.dto';

export class SetCustomerToLiveCommand {
    constructor(
        public readonly userRegistrationDto: UserRegistrationDto,
        public readonly cifNo: string
    ) {}
}