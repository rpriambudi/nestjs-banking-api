import { UserRegistrationDto } from './../../user/dto/user-registration.dto';

export class RegisterCustomerCommand {
    constructor(
        public readonly stateId: number,
        public readonly userRegistrationDto: UserRegistrationDto
    ) {}
}