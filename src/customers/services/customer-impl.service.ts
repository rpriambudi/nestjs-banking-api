import { Injectable } from '@nestjs/common'
import { Customer } from '../entities/customer.entity'
import { CreateCustomerDto } from '../dto/create-customer.dto'
import { CustomerService } from './../interfaces/customer-service.interface'
import { CustomerQueryService } from './../interfaces/customer-query-service.interface'
import { CustomerAccountService } from './../interfaces/customer-account-service.interface'
import { CustomerRepository } from './../repositories/customer.repository'
import { StatusEnum } from './../enums/status.enum'
import { CustomerBuilderRepository } from './../repositories/customer-builder.repository'
import { CreateAccountDto } from './../../account/dto/create-account.dto'

@Injectable()
export class CustomerServiceImpl implements CustomerService {
    constructor(
        private readonly customerRepository : CustomerRepository,
        private readonly customerQueryService: CustomerQueryService,
        private readonly customerAccountService : CustomerAccountService,
        private readonly customerBuilderRepository: CustomerBuilderRepository
    ) {}

    async createNewCustomer(customerDto : CreateCustomerDto): Promise<Customer> {
        const customerData = await this.customerBuilderRepository.buildNewCustomerData(customerDto);
        await this.customerRepository.save(customerData);

        const accountDto: CreateAccountDto = customerDto.account;
        accountDto.customerId = customerData.id;
        accountDto.branchCode = customerDto.branchCode;

        try {
            await this.customerAccountService.createNewCustomerAccount(customerData.cifNo, accountDto);
            return customerData;
        } catch(error) {
            await this.customerRepository.delete(customerData.id);
            throw error;
        }
    }

    async blacklistCustomer(cifNo: string): Promise<Customer> {
        const customerData = await this.customerQueryService.findValidCustomerBlacklistData(cifNo);

        await this.customerAccountService.blockCustomerAccounts(customerData);
        customerData.status = StatusEnum.Blacklisted;
        await this.customerRepository.save(customerData);

        return customerData;
    }

    async unblacklistCustomer(cifNo: string): Promise<Customer> {
        const customerData = await this.customerQueryService.findValidCustomerUnblacklistData(cifNo);

        await this.customerAccountService.unblockCustomerAccounts(customerData);
        customerData.status = StatusEnum.OnApproval;
        await this.customerRepository.save(customerData);

        return customerData;
    }

    async approveCustomer(cifNo: string): Promise<Customer> {
        const customerData = await this.customerQueryService.findValidCustomerApproveData(cifNo);

        customerData.status = StatusEnum.Live;
        await this.customerRepository.save(customerData);
        return customerData;
    }
}