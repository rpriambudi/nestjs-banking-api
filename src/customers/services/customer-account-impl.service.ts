import { Customer } from './../entities/customer.entity';
import { CreateRuleContext } from './../context/create-rule.context';
import { CustomerAccountService } from './../interfaces/customer-account-service.interface';
import { CustomerQueryService } from './../interfaces/customer-query-service.interface';
import { Account } from './../../account/entities/account.entity';
import { CreateAccountDto } from './../../account/dto/create-account.dto';
import { CheckBalanceDto } from './../../account/dto/check-balance.dto';
import { AccountService } from './../../account/interfaces/account-service.interface';
import { BalanceService } from './../../account/interfaces/balance-service.interface';

export class CustomerAccountServiceImpl implements CustomerAccountService {
    constructor(
        private readonly customerQueryService: CustomerQueryService,
        private readonly accountService: AccountService,
        private readonly balanceService: BalanceService,
        private readonly newCustomerAccountRule: CreateRuleContext
    ) {}

    async getCustomerBalance(cifNo: string): Promise<CheckBalanceDto[]> {
        const customerData = await this.customerQueryService.findCustomerByCifNo(cifNo);
        const balanceData = await this.balanceService.checkBalanceByCustomerId(customerData.id);
        return balanceData;
    }

    async createNewCustomerAccount(cifNo: string, createAccountDto: CreateAccountDto): Promise<Account> {
        const customerData = await this.customerQueryService.findCustomerByCifNo(cifNo);
        await this.newCustomerAccountRule.validate(customerData);

        createAccountDto.customerId = customerData.id;
        return await this.accountService.createNewAccount(createAccountDto);
    }

    async blockCustomerAccounts(customerData: Customer): Promise<Account[]> {
        const accountData: Account[] = await this.accountService.findAccountByCustomerId(customerData.id);

        for (const account of accountData) {
            try {
                await this.accountService.blockAccount(account.accountNo);
            } catch (error) {
                console.log(`Failed to unblock account ${account.accountNo}. ${error.message}`);
                continue;
            }
        }
        return accountData;
    }

    async unblockCustomerAccounts(customerData: Customer): Promise<Account[]> {
        const accountData: Account[] = await this.accountService.findAccountByCustomerId(customerData.id);

        for (const account of accountData) {
            try {
                await this.accountService.unblockAccount(account.accountNo);
            } catch (error) {
                console.log(`Failed to unblock account ${account.accountNo}. ${error.message}`);
                continue;
            }
        }
        return accountData;
    }
}