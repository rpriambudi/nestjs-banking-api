import { Test } from '@nestjs/testing';
import { Customer } from '../entities/customer.entity';
import { StatusEnum } from '../enums/status.enum';
import { GenderEnum } from '../enums/gender.enum';
import { IdTypeEnum } from '../enums/idtype.enum';
import { CreateCustomerDto } from '../dto/create-customer.dto'
import { CustomerRepository } from './../repositories/customer.repository';
import { CustomerBuilderRepository } from './../repositories/customer-builder.repository';
import { CustomerQueryService } from './../interfaces/customer-query-service.interface';
import { CustomerAccountService } from './../interfaces/customer-account-service.interface';
import { CustomerServiceImpl } from './customer-impl.service';
import { CustomerQueryServiceImpl } from './customer-query-impl.service';
import { CustomerAccountServiceImpl } from './customer-account-impl.service';
import { CustomerNotFoundException } from './../exceptions/customer-not-found.exception';
import { InvalidStatusException } from './../exceptions/invalid-status.exception';
import { DuplicateIdTypeAndNumberException } from './../exceptions/duplicate-id-type-and-number.exception';
import { Account } from '../../account/entities/account.entity';
import { StatusEnum as AccountStatusEnum } from './../../account/enums/status.enum';
import { BranchNotFoundException } from '../../branch/exceptions/branch-not-found.exception';
import { getRepositoryToken } from '@nestjs/typeorm';
import { async } from 'rxjs/internal/scheduler/async';
jest.mock('./../repositories/customer.repository');
jest.mock('./../services/customer-query-impl.service');
jest.mock('./../services/customer-account-impl.service');
jest.mock('./../repositories/customer-builder.repository');


describe('CustomerServiceImpl class', () => {
    let customerService: CustomerServiceImpl;
    let customerRepository: CustomerRepository;
    let customerQueryService: CustomerQueryService;
    let customerAccountService: CustomerAccountService;
    let customerBuilderRepository: CustomerBuilderRepository;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            providers: [
                {
                    provide: 'CustomerService',
                    useFactory: (customerRepository, customerQueryService, customerAccountService, customerBuilderRepository) => {
                        return new CustomerServiceImpl(
                            customerRepository, 
                            customerQueryService, 
                            customerAccountService, 
                            customerBuilderRepository
                        );
                    },
                    inject: [CustomerRepository, 'CustomerQueryService', 'CustomerAccountService', 'CustomerBuilderRepository']
                },
                {
                    provide: getRepositoryToken(Customer),
                    useClass: CustomerRepository
                },
                {
                    provide: 'CustomerQueryService',
                    useClass: CustomerQueryServiceImpl
                },
                {
                    provide: 'CustomerAccountService',
                    useClass: CustomerAccountServiceImpl
                },
                {
                    provide: 'CustomerBuilderRepository',
                    useClass: CustomerBuilderRepository
                }
            ]
        }).compile();

        customerService = moduleRef.get<CustomerServiceImpl>('CustomerService');
        customerRepository = moduleRef.get<CustomerRepository>(getRepositoryToken(Customer));
        customerQueryService = moduleRef.get<CustomerQueryService>('CustomerQueryService');
        customerAccountService = moduleRef.get<CustomerAccountService>('CustomerAccountService');
        customerBuilderRepository = moduleRef.get<CustomerBuilderRepository>('CustomerBuilderRepository');
    })

    describe('createNewCustomer method', () => {
        it('Positive test. Should return one valid customer object', async () => {
            const customerDto: CreateCustomerDto = {
                address: 'Test Address',
                birthDate: '19-04-1993',
                branchCode: '001',
                gender: GenderEnum.Male,
                idType: IdTypeEnum.KTP,
                idNumber: "3215031904930001",
                motherMaidenName: "Test",
                name: "Rachmat Priambudi",
                registeredBy: "CS1",
                account: {
                    balance: 1000000,
                    branchCode: '001',
                    customerId: 1
                }
            }
            const testCustomer: Customer = {
                id: 1,
                cifNo: '00132100000001',
                address: 'Test Address',
                birthDate: '19-04-1993',
                branchId: 1,
                gender: GenderEnum.Male,
                idType: IdTypeEnum.KTP,
                idNumber: "3215031904930001",
                motherMaidenName: "Test",
                name: "Rachmat Priambudi",
                registeredBy: "CS1",
                registeredAt: '03-03-2020',
                status: StatusEnum.OnApproval
            }
            const testAccount: Account = {
                id: 1,
                accountNo: '00101000000001',
                openedAt: 'KCP Thamrin',
                balance: 500000,
                status: StatusEnum.Live,
                branchId: 1,
                customerId: 1
            }

            jest.spyOn(customerBuilderRepository, 'buildNewCustomerData').mockResolvedValueOnce(testCustomer);
            jest.spyOn(customerRepository, 'save').mockResolvedValueOnce(testCustomer);
            jest.spyOn(customerAccountService, 'createNewCustomerAccount').mockResolvedValueOnce(testAccount);
            expect(await customerService.createNewCustomer(customerDto)).toBe(testCustomer);
        })
        it('Negative test, branch not found. Should throw BranchNotFoundException', async () => {
            const customerDto: CreateCustomerDto = {
                address: 'Test Address',
                birthDate: '19-04-1993',
                branchCode: '002',
                gender: GenderEnum.Male,
                idType: IdTypeEnum.KTP,
                idNumber: "3215031904930001",
                motherMaidenName: "Test",
                name: "Rachmat Priambudi",
                registeredBy: "CS1",
                account: {
                    balance: 1000000,
                    branchCode: '001',
                    customerId: 1
                }
            }

            jest.spyOn(customerBuilderRepository, 'buildNewCustomerData').mockImplementationOnce(():Promise<Customer> => {
                throw new BranchNotFoundException('Branch not found. Branch code: ' + '002');
            });

            try {
                expect(await customerService.createNewCustomer(customerDto)).toBeCalled();
            } catch (error) {
                expect(error instanceof BranchNotFoundException).toBe(true);
                expect(customerRepository.save).toHaveBeenCalledTimes(0);
                expect(customerAccountService.createNewCustomerAccount).toHaveBeenCalledTimes(0);
            }
        })
        it('Negative test, id type and number combination already exist. Should throw DuplicateIdTypeAndNumberException', async () => {
            const customerDto: CreateCustomerDto = {
                address: 'Test Address',
                birthDate: '19-04-1993',
                branchCode: '002',
                gender: GenderEnum.Male,
                idType: IdTypeEnum.KTP,
                idNumber: "3215031904930001",
                motherMaidenName: "Test",
                name: "Rachmat Priambudi",
                registeredBy: "CS1",
                account: {
                    balance: 1000000,
                    branchCode: '001',
                    customerId: 1
                }
            }

            jest.spyOn(customerBuilderRepository, 'buildNewCustomerData').mockImplementationOnce(():Promise<Customer> => {
                throw new DuplicateIdTypeAndNumberException('Id type and number already exists');
            });

            try {
                expect(await customerService.createNewCustomer(customerDto)).toBeCalled();
            } catch (error) {
                expect(error instanceof DuplicateIdTypeAndNumberException).toBe(true);
                expect(customerRepository.save).toHaveBeenCalledTimes(0);
                expect(customerAccountService.createNewCustomerAccount).toHaveBeenCalledTimes(0);
            }
        })

    })

    // describe('findByCifNo method', () => {
    //     it('Positive test. Should return one valid customer data', async () => {
    //         const testCustomer: Customer = {
    //             id: 1,
    //             cifNo: '00132100000001',
    //             address: 'Test Address',
    //             birthDate: '19-04-1993',
    //             branchId: 1,
    //             gender: GenderEnum.Male,
    //             idType: IdTypeEnum.KTP,
    //             idNumber: "3215031904930001",
    //             motherMaidenName: "Test",
    //             name: "Rachmat Priambudi",
    //             registeredBy: "CS1",
    //             registeredAt: '03-03-2020',
    //             status: StatusEnum.OnApproval
    //         }

    //         jest.spyOn(customerRepository, 'findOne').mockResolvedValueOnce(testCustomer);
    //         expect(await customerService.findCustomerByCifNo('00132100000001')).toBe(testCustomer);
    //     })
    //     it('Negative test, cifNo not found. Should throw CustomerNotFoundException.', async () => {
    //         jest.spyOn(customerRepository, 'findOne').mockResolvedValueOnce(null);
    //         try {
    //             expect(await customerService.findCustomerByCifNo('00132100000002')).toBeCalled();
    //         } catch (error) {
    //             expect(error instanceof CustomerNotFoundException).toBe(true);
    //         }
    //     })
    // })

    describe('blacklistCustomer method', () => {
        it('Positive test. Should return valid customer object', async () => {
            const testCustomer: Customer = {
                id: 1,
                cifNo: '00132100000001',
                address: 'Test Address',
                birthDate: '19-04-1993',
                branchId: 1,
                gender: GenderEnum.Male,
                idType: IdTypeEnum.KTP,
                idNumber: "3215031904930001",
                motherMaidenName: "Test",
                name: "Rachmat Priambudi",
                registeredBy: "CS1",
                registeredAt: '03-03-2020',
                status: StatusEnum.Live
            }
            const testBlacklistedCustomer: Customer = {
                id: 1,
                cifNo: '00132100000001',
                address: 'Test Address',
                birthDate: '19-04-1993',
                branchId: 1,
                gender: GenderEnum.Male,
                idType: IdTypeEnum.KTP,
                idNumber: "3215031904930001",
                motherMaidenName: "Test",
                name: "Rachmat Priambudi",
                registeredBy: "CS1",
                registeredAt: '03-03-2020',
                status: StatusEnum.Blacklisted
            }
            const testAccount: Account[] = [
                {
                    id: 1,
                    accountNo: '00101000000001',
                    openedAt: 'KCP Thamrin',
                    balance: 500000,
                    status: AccountStatusEnum.Blocked,
                    branchId: 1,
                    customerId: 1
                },
                {
                    id: 2,
                    accountNo: '00101000000002',
                    openedAt: 'KCP Thamrin',
                    balance: 500000,
                    status: AccountStatusEnum.Blocked,
                    branchId: 1,
                    customerId: 1
                }
            ]

            jest.spyOn(customerQueryService, 'findValidCustomerBlacklistData').mockResolvedValueOnce(testCustomer);
            jest.spyOn(customerAccountService, 'blockCustomerAccounts').mockResolvedValueOnce(testAccount);
            jest.spyOn(customerRepository, 'save').mockResolvedValueOnce(testBlacklistedCustomer);

            expect(await customerService.blacklistCustomer('00132100000001')).toStrictEqual(testBlacklistedCustomer);
            expect(customerQueryService.findValidCustomerBlacklistData).toHaveBeenCalledTimes(1);
            expect(customerAccountService.blockCustomerAccounts).toHaveBeenCalledTimes(1);
        });
        it('Negative test, customer already blacklisted. Should throw InvalidStatusException', async () => {
            jest.spyOn(customerQueryService, 'findValidCustomerBlacklistData').mockImplementationOnce(async () => {
                throw new InvalidStatusException('Customer already blacklisted.')
            });

            try {
                await customerService.blacklistCustomer('00132100000001');
            } catch(error) {
                expect(error instanceof InvalidStatusException).toBe(true);
                expect(customerQueryService.findValidCustomerBlacklistData).toHaveBeenCalledTimes(1);
                expect(customerAccountService.blockCustomerAccounts).toHaveBeenCalledTimes(0);
                expect(customerRepository.save).toHaveBeenCalledTimes(0);
            }
        })
        it('Negative test, cifNo not found. Should throw CustomerNotFoundException', async () => {
            jest.spyOn(customerQueryService, 'findValidCustomerBlacklistData').mockImplementationOnce(async () => {
                throw new CustomerNotFoundException('Customer not found.')
            });

            try {
                await customerService.blacklistCustomer('00132100000001');
            } catch (error) {
                expect(error instanceof CustomerNotFoundException).toBe(true);
                expect(customerQueryService.findValidCustomerBlacklistData).toHaveBeenCalledTimes(1);
                expect(customerAccountService.blockCustomerAccounts).toHaveBeenCalledTimes(0);
            }
        })
    })

    describe('unblacklistCustomer method', () => {
        it('Positive test. Should return valid customer object', async () => {
            const testCustomer: Customer = {
                id: 1,
                cifNo: '00132100000001',
                address: 'Test Address',
                birthDate: '19-04-1993',
                branchId: 1,
                gender: GenderEnum.Male,
                idType: IdTypeEnum.KTP,
                idNumber: "3215031904930001",
                motherMaidenName: "Test",
                name: "Rachmat Priambudi",
                registeredBy: "CS1",
                registeredAt: '03-03-2020',
                status: StatusEnum.Blacklisted
            }
            const unblacklistCustomer = Object.assign({}, testCustomer);
            unblacklistCustomer.status = StatusEnum.OnApproval;

            const testAccount: Account[] = [
                {
                    id: 1,
                    accountNo: '00101000000001',
                    openedAt: 'KCP Thamrin',
                    balance: 500000,
                    status: AccountStatusEnum.Blocked,
                    branchId: 1,
                    customerId: 1
                },
                {
                    id: 2,
                    accountNo: '00101000000002',
                    openedAt: 'KCP Thamrin',
                    balance: 500000,
                    status: AccountStatusEnum.Blocked,
                    branchId: 1,
                    customerId: 1
                }
            ]

            jest.spyOn(customerQueryService, 'findValidCustomerUnblacklistData').mockResolvedValueOnce(testCustomer);
            jest.spyOn(customerAccountService, 'unblockCustomerAccounts').mockResolvedValueOnce(testAccount);
            jest.spyOn(customerRepository, 'save').mockResolvedValueOnce(unblacklistCustomer);

            expect(await customerService.unblacklistCustomer('00132100000001')).toStrictEqual(unblacklistCustomer);
            expect(customerQueryService.findValidCustomerUnblacklistData).toHaveBeenCalledTimes(1);
            expect(customerAccountService.unblockCustomerAccounts).toHaveBeenCalledTimes(1);
            expect(customerRepository.save).toHaveBeenCalledTimes(1);
        })
        it('Negative test, customer already unblacklisted. Should throw InvalidStatusException', async () => {
            jest.spyOn(customerQueryService, 'findValidCustomerUnblacklistData').mockImplementationOnce(async () => {
                throw new InvalidStatusException('Customer already unblacklisted');
            });

            try {
                await customerService.unblacklistCustomer('00132100000001');
            } catch(error) {
                expect(error instanceof InvalidStatusException).toBe(true);
                expect(customerQueryService.findValidCustomerUnblacklistData).toHaveBeenCalledTimes(1);
                expect(customerAccountService.unblockCustomerAccounts).toHaveBeenCalledTimes(0);
                expect(customerRepository.save).toHaveBeenCalledTimes(0);
            }
        })
        it('Negative test, cifNo not found. Should throw CustomerNotFoundException', async () => {
            jest.spyOn(customerQueryService, 'findValidCustomerUnblacklistData').mockImplementationOnce(async () => {
                throw new CustomerNotFoundException('Customer not found.')
            });

            try {
                await customerService.unblacklistCustomer('00132100000001');
            } catch (error) {
                expect(error instanceof CustomerNotFoundException).toBe(true);
                expect(customerQueryService.findValidCustomerUnblacklistData).toHaveBeenCalledTimes(1);
                expect(customerAccountService.unblockCustomerAccounts).toHaveBeenCalledTimes(0);
                expect(customerRepository.save).toHaveBeenCalledTimes(0);
            }
        })
    })

    describe('approveCustomer method', () => {
        it('Positive test. Should return one valid customer object', async () => {
            const testCustomer: Customer = {
                id: 1,
                cifNo: '00132100000001',
                address: 'Test Address',
                birthDate: '19-04-1993',
                branchId: 1,
                gender: GenderEnum.Male,
                idType: IdTypeEnum.KTP,
                idNumber: "3215031904930001",
                motherMaidenName: "Test",
                name: "Rachmat Priambudi",
                registeredBy: "CS1",
                registeredAt: '03-03-2020',
                status: StatusEnum.OnApproval
            }
            const approvedCustomer: Customer = Object.assign({}, testCustomer);
            approvedCustomer.status = StatusEnum.Live;

            jest.spyOn(customerQueryService, 'findValidCustomerApproveData').mockResolvedValueOnce(testCustomer);
            jest.spyOn(customerRepository, 'save').mockResolvedValueOnce(approvedCustomer);

            expect(await customerService.approveCustomer('00132100000001')).toStrictEqual(approvedCustomer);
        })
        it('Negative test, customer already approved. Should throw InvalidStatusException', async () => {
            jest.spyOn(customerQueryService, 'findValidCustomerApproveData').mockImplementationOnce(async () => {
                throw new InvalidStatusException('Customer already approved');
            });

            try {
                await customerService.approveCustomer('00132100000002');
            } catch (error) {
                expect(error instanceof InvalidStatusException).toBe(true);
                expect(customerQueryService.findValidCustomerApproveData).toHaveBeenCalledTimes(1);
                expect(customerRepository.save).toHaveBeenCalledTimes(0);
            }
        })
        it('Negative test, cifNo not found. Should throw CustomerNotFoundException', async () => {
            jest.spyOn(customerQueryService, 'findValidCustomerApproveData').mockImplementationOnce(async () => {
                throw new CustomerNotFoundException('Customer not found.')
            })

            try {
                await customerService.approveCustomer('00132100000002');
            } catch (error) {
                expect(error instanceof CustomerNotFoundException).toBe(true);
                expect(customerQueryService.findValidCustomerApproveData).toHaveBeenCalledTimes(1);
                expect(customerRepository.save).toHaveBeenCalledTimes(0);
            }
        })
    })
})