import { Customer } from './../entities/customer.entity';
import { CustomerQueryService } from './../interfaces/customer-query-service.interface';
import { BusinessRule } from './../interfaces/business-rule.interface';
import { CustomerRepository } from './../repositories/customer.repository';
import { CustomerNotFoundException } from './../exceptions/customer-not-found.exception';
import { BranchService } from './../../branch/interfaces/branch-service.interface';

export class CustomerQueryServiceImpl implements CustomerQueryService {
    constructor(
        private readonly customerRepository: CustomerRepository,
        private readonly branchService: BranchService,
        private readonly blacklistRule: BusinessRule,
        private readonly unblacklistRule: BusinessRule,
        private readonly approveRule: BusinessRule
    ) {}

    async findCustomerByCifNo(cifNo: string): Promise<Customer> {
        const customerData = await this.customerRepository.findOne({cifNo: cifNo});
        if (!customerData)
            throw new CustomerNotFoundException('Customer not found.');
            
        return customerData;
    }

    async findCustomerByIdTypeAndNo(idType: string, idNumber: string): Promise<Customer> {
        return await this.customerRepository.findOne({ idType: idType, idNumber: idNumber});
    }

    async searchCustomer(queryParam: object): Promise<Customer[]> {
        if (queryParam['branchCode']) {
            const branchData = await this.branchService.findBranchByCode(queryParam['branchCode']);
            delete queryParam['branchCode'];
            queryParam['branchId'] = branchData.id;
        }

        const customerData = await this.customerRepository.searchQuery(queryParam);
        return customerData;
    }

    async findValidCustomerBlacklistData(cifNo: string): Promise<Customer> {
        const customerData = await this.findCustomerByCifNo(cifNo);
        await this.blacklistRule.validate(customerData);
        return customerData;
    }

    async findValidCustomerUnblacklistData(cifNo: string): Promise<Customer> {
        const customerData = await this.findCustomerByCifNo(cifNo);
        await this.unblacklistRule.validate(customerData);
        return customerData;
    }

    async findValidCustomerApproveData(cifNo: string): Promise<Customer> {
        const customerData = await this.findCustomerByCifNo(cifNo);
        await this.approveRule.validate(customerData);
        return customerData;
    }
}