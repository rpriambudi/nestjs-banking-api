import { Test } from '@nestjs/testing';
import { Customer } from './../entities/customer.entity';
import { GenderEnum } from './../enums/gender.enum';
import { StatusEnum } from './../enums/status.enum';
import { IdTypeEnum } from './../enums/idtype.enum';
import { CustomerAccountService } from './../interfaces/customer-account-service.interface';
import { CustomerAccountServiceImpl } from './../services/customer-account-impl.service';
import { CustomerQueryService } from './../interfaces/customer-query-service.interface';
import { CustomerQueryServiceImpl } from './../services/customer-query-impl.service';
import { BusinessRule } from './../interfaces/business-rule.interface';
import { CreateRuleContext } from './../context/create-rule.context';
import { CustomerNotFoundException } from './../exceptions/customer-not-found.exception';
import { InvalidStatusException } from './../exceptions/invalid-status.exception';
import { Account } from './../../account/entities/account.entity';
import { CheckBalanceDto } from './../../account/dto/check-balance.dto';
import { CreateAccountDto } from './../../account/dto/create-account.dto';
import { StatusEnum as AccountStatusEnum } from './../../account/enums/status.enum';
import { AccountService } from './../../account/interfaces/account-service.interface';
import { AccountServiceImpl } from './../../account/services/account-impl.service';
import { BalanceService } from './../../account/interfaces/balance-service.interface';
import { BalanceServiceImpl } from './../../account/services/balance-impl.service';
jest.mock('./../context/create-rule.context');
jest.mock('./../services/customer-query-impl.service');
jest.mock('./../../account/services/account-impl.service');
jest.mock('./../../account/services/balance-impl.service');


describe('CustomerAccountServiceImpl class', () => {
    let customerAccountService: CustomerAccountService;
    let customerQueryService: CustomerQueryService;
    let accountService: AccountService;
    let balanceService: BalanceService;
    let newCustomerAccountRule: BusinessRule;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            providers: [
                {
                    provide: 'CustomerAccountService',
                    useFactory: (customerQueryService, accountService, balanceService, newCustomerAccountRule) => {
                        return new CustomerAccountServiceImpl(customerQueryService, accountService, balanceService, newCustomerAccountRule);
                    },
                    inject: ['CustomerQueryService', 'AccountService', 'BalanceService', 'NewCustomerAccountRule']
                },
                {
                    provide: 'CustomerQueryService',
                    useClass: CustomerQueryServiceImpl
                },
                {
                    provide: 'AccountService',
                    useClass: AccountServiceImpl
                },
                {
                    provide: 'BalanceService',
                    useClass: BalanceServiceImpl
                },
                {
                    provide: 'NewCustomerAccountRule',
                    useClass: CreateRuleContext
                }
            ]
        }).compile();

        customerAccountService = moduleRef.get<CustomerAccountServiceImpl>('CustomerAccountService');
        customerQueryService = moduleRef.get<CustomerQueryServiceImpl>('CustomerQueryService');
        accountService = moduleRef.get<AccountService>('AccountService');
        balanceService = moduleRef.get<BalanceService>('BalanceService');
        newCustomerAccountRule = moduleRef.get<BusinessRule>('NewCustomerAccountRule');
    })

    describe('createNewCustomerAccount method', () => {
        it('Positive test. Should return one valid account data', async () => {
            const testCustomer: Customer = {
                id: 1,
                cifNo: '00132100000001',
                address: 'Test Address',
                birthDate: '19-04-1993',
                branchId: 1,
                gender: GenderEnum.Male,
                idType: IdTypeEnum.KTP,
                idNumber: "3215031904930001",
                motherMaidenName: "Test",
                name: "Rachmat Priambudi",
                registeredBy: "CS1",
                registeredAt: '03-03-2020',
                status: StatusEnum.OnApproval
            }
            const accountDto: CreateAccountDto = {
                balance: 500000,
                branchCode: '001',
                customerId: 1
            }
            const testAccount: Account = {
                id: 1,
                accountNo: '00101000000001',
                openedAt: 'KCP Thamrin',
                balance: 500000,
                status: StatusEnum.Live,
                branchId: 1,
                customerId: 1,
            }

            jest.spyOn(customerQueryService, 'findCustomerByCifNo').mockResolvedValueOnce(testCustomer);
            jest.spyOn(accountService, 'createNewAccount').mockResolvedValueOnce(testAccount);

            expect(await customerAccountService.createNewCustomerAccount('00132100000001', accountDto)).toBe(testAccount);
        })
        it('Negative test, cifNo not found. should throw CustomerNotFoundException', async () => {
            const accountDto: CreateAccountDto = {
                balance: 500000,
                branchCode: '001',
                customerId: 1
            }

            jest.spyOn(customerQueryService, 'findCustomerByCifNo').mockImplementationOnce(async () => {
                throw new CustomerNotFoundException('Customer not found');
            })

            try {
                await customerAccountService.createNewCustomerAccount('00132100000001', accountDto);
            } catch (error) {
                expect(error instanceof CustomerNotFoundException).toBe(true);
                expect(accountService.createNewAccount).toHaveBeenCalledTimes(0);
            }
        })
        it('Negative test, customer is blacklisted. Should throw InvalidStatusException', async () => {
            const testCustomer: Customer = {
                id: 1,
                cifNo: '00132100000001',
                address: 'Test Address',
                birthDate: '19-04-1993',
                branchId: 1,
                gender: GenderEnum.Male,
                idType: IdTypeEnum.KTP,
                idNumber: "3215031904930001",
                motherMaidenName: "Test",
                name: "Rachmat Priambudi",
                registeredBy: "CS1",
                registeredAt: '03-03-2020',
                status: StatusEnum.Blacklisted
            };
            const accountDto: CreateAccountDto = {
                balance: 500000,
                branchCode: '001',
                customerId: 1
            }

            jest.spyOn(customerQueryService, 'findCustomerByCifNo').mockResolvedValueOnce(testCustomer);
            jest.spyOn(newCustomerAccountRule, 'validate').mockImplementationOnce(async () => {
                throw new InvalidStatusException('Customer is blacklisted.');
            });
            try {
                await customerAccountService.createNewCustomerAccount('00132100000001', accountDto);
            } catch (error) {
                expect(error instanceof InvalidStatusException).toBe(true);
                expect(accountService.createNewAccount).toHaveBeenCalledTimes(0);
            }
        })
        it('Negative test, customer is not approved yet. Should throw InvalidStatusException', async () => {
            const testCustomer: Customer = {
                id: 1,
                cifNo: '00132100000001',
                address: 'Test Address',
                birthDate: '19-04-1993',
                branchId: 1,
                gender: GenderEnum.Male,
                idType: IdTypeEnum.KTP,
                idNumber: "3215031904930001",
                motherMaidenName: "Test",
                name: "Rachmat Priambudi",
                registeredBy: "CS1",
                registeredAt: '03-03-2020',
                status: StatusEnum.OnApproval
            };
            const accountDto: CreateAccountDto = {
                balance: 500000,
                branchCode: '001',
                customerId: 1
            }

            jest.spyOn(customerQueryService, 'findCustomerByCifNo').mockResolvedValueOnce(testCustomer);
            jest.spyOn(newCustomerAccountRule, 'validate').mockImplementationOnce(async () => {
                throw new InvalidStatusException('Customer is not approved.');
            });
            try {
                await customerAccountService.createNewCustomerAccount('00132100000001', accountDto);
            } catch (error) {
                expect(error instanceof InvalidStatusException).toBe(true);
                expect(accountService.createNewAccount).toHaveBeenCalledTimes(0);
            }
        })
    })

    describe('getCustomerBalance method', () => {
        it('Positive test. Should return customer account balance objects', async () => {
            const testCustomer: Customer = {
                id: 1,
                cifNo: '00132100000001',
                address: 'Test Address',
                birthDate: '19-04-1993',
                branchId: 1,
                gender: GenderEnum.Male,
                idType: IdTypeEnum.KTP,
                idNumber: "3215031904930001",
                motherMaidenName: "Test",
                name: "Rachmat Priambudi",
                registeredBy: "CS1",
                registeredAt: '03-03-2020',
                status: StatusEnum.Live
            }
            const testBalance: CheckBalanceDto[] = [
                new CheckBalanceDto('00132100000001', 1000000),
                new CheckBalanceDto('00132100000002', 1000000)
            ];

            jest.spyOn(customerQueryService, 'findCustomerByCifNo').mockResolvedValueOnce(testCustomer);
            jest.spyOn(balanceService, 'checkBalanceByCustomerId').mockResolvedValueOnce(testBalance);
            
            expect(await customerAccountService.getCustomerBalance('00132100000001')).toBe(testBalance);
        })
        it('Negative test, cifNo not found. Should throw CustomerNotFoundException', async () => {
            jest.spyOn(customerQueryService, 'findCustomerByCifNo').mockImplementationOnce(async (): Promise<Customer> => {
                throw new CustomerNotFoundException('Customer not found.');
            })

            try {
                expect(await customerAccountService.getCustomerBalance('00132100000001')).toBeCalled();
            } catch (error) {
                expect(error instanceof CustomerNotFoundException).toBe(true);
                expect(balanceService.checkBalanceByCustomerId).toHaveBeenCalledTimes(0);
            }
        })
    })

    describe('blockCustomerAccounts method', () => {
        it('Positive test, all successfully blocked. Should return list of valid accounts', async () => {
            const testCustomer: Customer = {
                id: 1,
                cifNo: '00132100000001',
                address: 'Test Address',
                birthDate: '19-04-1993',
                branchId: 1,
                gender: GenderEnum.Male,
                idType: IdTypeEnum.KTP,
                idNumber: "3215031904930001",
                motherMaidenName: "Test",
                name: "Rachmat Priambudi",
                registeredBy: "CS1",
                registeredAt: '03-03-2020',
                status: StatusEnum.Live
            }
            const testAccounts: Account[] = [
                {
                    id: 1,
                    accountNo: '0010000000001',
                    balance: 5000000,
                    branchId: 1,
                    customerId: 1,
                    openedAt: 'Test Branch',
                    status: AccountStatusEnum.Live
                },
                {
                    id: 2,
                    accountNo: '0010000000002',
                    balance: 5000000,
                    branchId: 1,
                    customerId: 1,
                    openedAt: 'Test Branch',
                    status: AccountStatusEnum.Live
                }
            ];
            const testBlockedAccounts: Account[] = [
                {
                    id: 1,
                    accountNo: '0010000000001',
                    balance: 5000000,
                    branchId: 1,
                    customerId: 1,
                    openedAt: 'Test Branch',
                    status: AccountStatusEnum.Blocked
                },
                {
                    id: 2,
                    accountNo: '0010000000002',
                    balance: 5000000,
                    branchId: 1,
                    customerId: 1,
                    openedAt: 'Test Branch',
                    status: AccountStatusEnum.Blocked
                }
            ];

            jest.spyOn(accountService, 'findAccountByCustomerId').mockResolvedValueOnce(testAccounts);
            jest.spyOn(accountService, 'blockAccount').mockImplementation(async (accountNo: string) => {
                if (accountNo === '0010000000001') {
                    testAccounts[0].status = AccountStatusEnum.Blocked;
                    return testAccounts[0];
                }

                if (accountNo === '0010000000002') {
                    testAccounts[1].status = AccountStatusEnum.Blocked;
                    return testAccounts[1];
                }
            })

            expect(await customerAccountService.blockCustomerAccounts(testCustomer)).toEqual(testBlockedAccounts);
            expect(accountService.blockAccount).toHaveBeenCalledTimes(2);
        })
        it('Positive test, one of the account is already blocked. Should return list of valid accounts', async () => {
            const testCustomer: Customer = {
                id: 1,
                cifNo: '00132100000001',
                address: 'Test Address',
                birthDate: '19-04-1993',
                branchId: 1,
                gender: GenderEnum.Male,
                idType: IdTypeEnum.KTP,
                idNumber: "3215031904930001",
                motherMaidenName: "Test",
                name: "Rachmat Priambudi",
                registeredBy: "CS1",
                registeredAt: '03-03-2020',
                status: StatusEnum.Live
            }
            const testAccounts: Account[] = [
                {
                    id: 1,
                    accountNo: '0010000000001',
                    balance: 5000000,
                    branchId: 1,
                    customerId: 1,
                    openedAt: 'Test Branch',
                    status: AccountStatusEnum.Live
                },
                {
                    id: 2,
                    accountNo: '0010000000002',
                    balance: 5000000,
                    branchId: 1,
                    customerId: 1,
                    openedAt: 'Test Branch',
                    status: AccountStatusEnum.Blocked
                }
            ];
            const testBlockedAccounts: Account[] = [
                {
                    id: 1,
                    accountNo: '0010000000001',
                    balance: 5000000,
                    branchId: 1,
                    customerId: 1,
                    openedAt: 'Test Branch',
                    status: AccountStatusEnum.Blocked
                },
                {
                    id: 2,
                    accountNo: '0010000000002',
                    balance: 5000000,
                    branchId: 1,
                    customerId: 1,
                    openedAt: 'Test Branch',
                    status: AccountStatusEnum.Blocked
                }
            ];

            jest.spyOn(accountService, 'findAccountByCustomerId').mockResolvedValueOnce(testAccounts);
            jest.spyOn(accountService, 'blockAccount').mockImplementation(async (accountNo: string) => {
                if (accountNo === '0010000000001') {
                    testAccounts[0].status = AccountStatusEnum.Blocked;
                    return testAccounts[0];
                }
            })

            expect(await customerAccountService.blockCustomerAccounts(testCustomer)).toEqual(testBlockedAccounts);
            expect(accountService.blockAccount).toHaveBeenCalledTimes(2);
        })
    })

    describe('unblockCustomerAccounts method', () => {
        it('Positive test, all successfully unblocked. Should return list of valid accounts', async () => {
            const testCustomer: Customer = {
                id: 1,
                cifNo: '00132100000001',
                address: 'Test Address',
                birthDate: '19-04-1993',
                branchId: 1,
                gender: GenderEnum.Male,
                idType: IdTypeEnum.KTP,
                idNumber: "3215031904930001",
                motherMaidenName: "Test",
                name: "Rachmat Priambudi",
                registeredBy: "CS1",
                registeredAt: '03-03-2020',
                status: StatusEnum.Live
            }
            const testAccounts: Account[] = [
                {
                    id: 1,
                    accountNo: '0010000000001',
                    balance: 5000000,
                    branchId: 1,
                    customerId: 1,
                    openedAt: 'Test Branch',
                    status: AccountStatusEnum.Blocked
                },
                {
                    id: 2,
                    accountNo: '0010000000002',
                    balance: 5000000,
                    branchId: 1,
                    customerId: 1,
                    openedAt: 'Test Branch',
                    status: AccountStatusEnum.Blocked
                }
            ];
            const testUnblockedAccounts: Account[] = [
                {
                    id: 1,
                    accountNo: '0010000000001',
                    balance: 5000000,
                    branchId: 1,
                    customerId: 1,
                    openedAt: 'Test Branch',
                    status: AccountStatusEnum.Live
                },
                {
                    id: 2,
                    accountNo: '0010000000002',
                    balance: 5000000,
                    branchId: 1,
                    customerId: 1,
                    openedAt: 'Test Branch',
                    status: AccountStatusEnum.Live
                }
            ];

            jest.spyOn(accountService, 'findAccountByCustomerId').mockResolvedValueOnce(testAccounts);
            jest.spyOn(accountService, 'unblockAccount').mockImplementation(async (accountNo: string) => {
                if (accountNo === '0010000000001') {
                    testAccounts[0].status = AccountStatusEnum.Live;
                    return testAccounts[0];
                }

                if (accountNo === '0010000000002') {
                    testAccounts[1].status = AccountStatusEnum.Live;
                    return testAccounts[1];
                }
            })

            expect(await customerAccountService.unblockCustomerAccounts(testCustomer)).toEqual(testUnblockedAccounts);
            expect(accountService.unblockAccount).toHaveBeenCalledTimes(2);
        })
        it('Positive test, one of the account is already unblocked. Should return list of valid accounts', async () => {
            const testCustomer: Customer = {
                id: 1,
                cifNo: '00132100000001',
                address: 'Test Address',
                birthDate: '19-04-1993',
                branchId: 1,
                gender: GenderEnum.Male,
                idType: IdTypeEnum.KTP,
                idNumber: "3215031904930001",
                motherMaidenName: "Test",
                name: "Rachmat Priambudi",
                registeredBy: "CS1",
                registeredAt: '03-03-2020',
                status: StatusEnum.OnApproval
            }
            const testAccounts: Account[] = [
                {
                    id: 1,
                    accountNo: '0010000000001',
                    balance: 5000000,
                    branchId: 1,
                    customerId: 1,
                    openedAt: 'Test Branch',
                    status: AccountStatusEnum.Blocked
                },
                {
                    id: 2,
                    accountNo: '0010000000002',
                    balance: 5000000,
                    branchId: 1,
                    customerId: 1,
                    openedAt: 'Test Branch',
                    status: AccountStatusEnum.Live
                }
            ];
            const testUnblockedAccounts: Account[] = [
                {
                    id: 1,
                    accountNo: '0010000000001',
                    balance: 5000000,
                    branchId: 1,
                    customerId: 1,
                    openedAt: 'Test Branch',
                    status: AccountStatusEnum.Live
                },
                {
                    id: 2,
                    accountNo: '0010000000002',
                    balance: 5000000,
                    branchId: 1,
                    customerId: 1,
                    openedAt: 'Test Branch',
                    status: AccountStatusEnum.Live
                }
            ];

            jest.spyOn(accountService, 'findAccountByCustomerId').mockResolvedValueOnce(testAccounts);
            jest.spyOn(accountService, 'unblockAccount').mockImplementation(async (accountNo: string) => {
                if (accountNo === '0010000000001') {
                    testAccounts[0].status = AccountStatusEnum.Live;
                    return testAccounts[0];
                }
            })

            expect(await customerAccountService.unblockCustomerAccounts(testCustomer)).toEqual(testUnblockedAccounts);
            expect(accountService.unblockAccount).toHaveBeenCalledTimes(2);
        })
    })
})