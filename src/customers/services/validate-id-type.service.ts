import { Injectable } from '@nestjs/common';
import { BusinessRule } from './../interfaces/business-rule.interface';
import { CustomerRepository } from './../repositories/customer.repository';
import { Customer } from '../entities/customer.entity';
import { DuplicateIdTypeAndNumberException } from '../exceptions/duplicate-id-type-and-number.exception';

@Injectable()
export class ValidateIdTypeRule implements BusinessRule {
    constructor(
        private readonly customerRepository: CustomerRepository
    ){}

    async validate(customerData: Customer): Promise<void> {
        const idType = customerData.idType;
        const idNumber = customerData.idNumber;
        const existingData = await this.customerRepository.findOne({ idType: idType, idNumber: idNumber });
        if (existingData)
            throw new DuplicateIdTypeAndNumberException('Duplicate ID type and number. ID type: ' + idType + ' ID No: ' + idNumber);
        
        return;
    }
}