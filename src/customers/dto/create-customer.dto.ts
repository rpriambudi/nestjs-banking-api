import { IsNotEmpty, IsIn, IsDateString } from 'class-validator';
import { CreateAccountDto } from '../../account/dto/create-account.dto';
import { IdTypeEnum } from '../enums/idtype.enum'
import { GenderEnum } from '../enums/gender.enum'

export class CreateCustomerDto {
    @IsNotEmpty()
    name: string;

    @IsNotEmpty()
    address: string;

    @IsNotEmpty()
    @IsDateString()
    birthDate: string;

    @IsNotEmpty()
    @IsIn(Object.values(IdTypeEnum))
    idType: string;

    @IsNotEmpty()
    idNumber: string;

    @IsNotEmpty()
    @IsIn(Object.values(GenderEnum))
    gender: string;

    @IsNotEmpty()
    motherMaidenName: string;

    @IsNotEmpty()
    account: CreateAccountDto

    registeredBy: string

    branchCode: string
}