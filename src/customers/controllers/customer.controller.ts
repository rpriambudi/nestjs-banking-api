import { Controller, Post, Body, Inject, Get, Param, Query, Request } from '@nestjs/common'
import { CreateCustomerDto } from '../dto/create-customer.dto'
import { CreateAccountDto } from './../../account/dto/create-account.dto'
import { CustomerService } from '../interfaces/customer-service.interface'
import { UserContext, AuthContext } from './../../auth/decorators/user-context.decorator'
import { CustomerQueryService } from '../interfaces/customer-query-service.interface'
import { CustomerAccountService } from '../interfaces/customer-account-service.interface'

@Controller()
export class CustomerController {
    constructor(
        @Inject('CustomerService') private readonly customerService : CustomerService,
        @Inject('CustomerQueryService') private readonly customerQueryService: CustomerQueryService,
        @Inject('CustomerAccountService') private readonly customerAccountService: CustomerAccountService
    ) {}

    @Get('/api/customer/:cifNo/accounts')
    async requestCustomerBalance(@Param('cifNo') cifNo: string) {
        const balanceData: object[] = await this.customerAccountService.getCustomerBalance(cifNo);
        return balanceData;
    }

    @Get('/api/customer/search')
    async requestCustomerSearch(
        @AuthContext() userContext: UserContext,
        @Query('cifNo') cifNo?: string, 
        @Query('name') name?: string,
    ) {
        const queryParam = {
            branchCode: userContext.branchCode
        }

        if (cifNo)
            queryParam['cifNo'] = cifNo;
        if (name)
            queryParam['name'] = name;

        const customerData = await this.customerQueryService.searchCustomer(queryParam);
        return customerData;
    }

    @Post('/api/customer')
    async registerNewCustomer(
        @Body() customerDto : CreateCustomerDto,
        @AuthContext() userContext: UserContext
    ) {
        customerDto.branchCode = userContext.branchCode;
        customerDto.registeredBy = userContext.email;
        const customerData = await this.customerService.createNewCustomer(customerDto);
        return customerData
    }

    @Post('/api/customer/:cifNo/account')
    async registerNewCustomerAccount(
        @Param('cifNo') cifNo: string, 
        @Body() createAccountDto: CreateAccountDto,
        @AuthContext() userContext: UserContext
    ) {
        createAccountDto.branchCode = userContext.branchCode;
        const customerData = await this.customerAccountService.createNewCustomerAccount(cifNo, createAccountDto);
        return customerData;
    }

    @Post('/api/customer/:cifNo/blacklist')
    async requestBlacklistCustomer(@Param('cifNo') cifNo: string) {
        const customerData = await this.customerService.blacklistCustomer(cifNo);
        return customerData;
    }

    @Post('/api/customer/:cifNo/unblacklist')
    async requestUnblacklistCustomer(@Param('cifNo') cifNo: string) {
        const customerData = await this.customerService.unblacklistCustomer(cifNo);
        return customerData;
    }

    @Post('/api/customer/:cifNo/approve')
    async requestApproveCustomer(@Param('cifNo') cifNo: string) {
        const customerData = await this.customerService.approveCustomer(cifNo);
        return customerData;
    }
}