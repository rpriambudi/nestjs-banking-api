import { GenericException } from './../../exceptions/generic-exception.abstract';

export class InvalidStatusException extends GenericException {
    getDisplayCode(): string {
        return 'INVALID_STATUS';
    }

    getErrorCode(): string {
        return '400203';
    }

    constructor(message: string) {
        super(message);
    }
}