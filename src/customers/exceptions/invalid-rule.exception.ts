
import { GenericException } from './../../exceptions/generic-exception.abstract';
export class InvalidRuleException extends GenericException {
    getDisplayCode(): string {
        return 'INVALID_RULE';
    }
    getErrorCode(): string {
        return '400204';
    }
    

    constructor(message: string) {
        super(message);
    }
}