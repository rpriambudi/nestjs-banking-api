import { GenericException } from './../../exceptions/generic-exception.abstract';

export class DuplicateIdTypeAndNumberException extends GenericException {
    getDisplayCode(): string {
        return 'DUPLICATE_ID_TYPE_AND_NUMBER';
    }

    getErrorCode(): string {
        return '409201';
    }
    
    constructor(message: string) {
        super(message);
    }
}