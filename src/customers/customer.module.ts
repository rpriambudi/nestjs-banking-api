import { Module, MiddlewareConsumer, NestModule, RequestMethod } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { TypeOrmModule } from '@nestjs/typeorm'
import { CqrsModule } from '@nestjs/cqrs'
import { CustomerController } from './controllers/customer.controller'
import { CustomerServiceImpl } from './services/customer-impl.service'
import { BranchModule } from './../branch/branch.module'
import { AccountModule } from './../account/account.module'
import { AuthModule } from './../auth/auth.module'
import { CustomerRepository } from './repositories/customer.repository'
import { ValidateIdTypeRule } from './services/validate-id-type.service'
import { CreateRuleContext } from './context/create-rule.context'
import { BlacklistStatusRule, UnblacklistStatusRule, ApproveStatusRule, ApprovalStatusRule } from './services/status-rule.service'
import { CustomerAuthMiddleware } from './middleware/customer-auth.middleware';
import { TellerAuthMiddleware } from './middleware/teller-auth.middleware';
import { CustomerQueryServiceImpl } from './services/customer-query-impl.service'
import { CustomerAccountServiceImpl } from './services/customer-account-impl.service'
import { SequenceCifNoGenerator } from './services/cif-no-generator-impl.service'
import { CustomerBuilderRepository } from './repositories/customer-builder.repository'
import { RegisterCustomerHandler } from './commands/handlers/register-customer.handler'
import { IdTypeNumberHandler } from './commands/handlers/id-type-number.handler'
import { GenerateCifNoHandler } from './commands/handlers/generate-cif-no.handler'
import { SetCustomerToLiveHandler } from './commands/handlers/set-customer-to-live.handler'

const CommandHandlers = [RegisterCustomerHandler, IdTypeNumberHandler, GenerateCifNoHandler, SetCustomerToLiveHandler]

@Module({
    imports: [
        TypeOrmModule.forFeature([CustomerRepository]),
        CqrsModule,
        BranchModule,
        AccountModule,
        AuthModule
    ],
    controllers: [CustomerController],
    providers: [
        {
            provide: 'CustomerService',
            useFactory: (customerRepository, customerQueryService, customerAccountService, customerBuilderRepository) => {
                return new CustomerServiceImpl(
                    customerRepository,
                    customerQueryService,
                    customerAccountService,
                    customerBuilderRepository
                );
            },
            inject: [CustomerRepository, 'CustomerQueryService', 'CustomerAccountService', 'CustomerBuilderRepository']
        },
        {
            provide: 'CustomerQueryService',
            useFactory: (customerRepository, branchService, blacklistRule, unblacklistRule, approveRule) => {
                return new CustomerQueryServiceImpl(
                    customerRepository,
                    branchService,
                    blacklistRule,
                    unblacklistRule,
                    approveRule
                );
            },
            inject: [CustomerRepository, 'BranchService', 'BlacklistRule', 'UnblacklistRule', 'ApproveRule']
        },
        {
            provide: 'CustomerAccountService',
            useFactory: (customerQueryService, accountService, balanceService, newCustomerAccountRule) => {
                return new CustomerAccountServiceImpl(
                    customerQueryService,
                    accountService,
                    balanceService,
                    newCustomerAccountRule
                );
            },
            inject: ['CustomerQueryService', 'AccountService', 'BalanceService', 'NewCustomerAccountRule']
        },
        {
            provide: 'CustomerBuilderRepository',
            useFactory: (customerRepository, branchService, cifNoGenerator, newCustomerValidation) => {
                return new CustomerBuilderRepository(customerRepository, branchService, cifNoGenerator, newCustomerValidation);
            },
            inject: [CustomerRepository, 'BranchService', 'CifNoGenerator', 'NewCustomerValidation']
        },
        {
            provide: 'NewCustomerValidation',
            useFactory: (customerRepository) => {
                return new CreateRuleContext([new ValidateIdTypeRule(customerRepository)]);
            },
            inject: [CustomerRepository]
        },
        {
            provide: 'NewCustomerAccountRule',
            useFactory: () => {
                return new CreateRuleContext([new BlacklistStatusRule(), new ApprovalStatusRule()])
            }
        },
        {
            provide: 'BlacklistRule',
            useFactory: () => {
                return new CreateRuleContext([new BlacklistStatusRule()])
            }
        },
        {
            provide: 'UnblacklistRule',
            useFactory: () => {
                return new CreateRuleContext([new UnblacklistStatusRule()])
            }
        },
        {
            provide: 'ApproveRule',
            useFactory: () => {
                return new CreateRuleContext([new ApproveStatusRule()])
            }
        },
        {
            provide: 'CifNoGenerator',
            useFactory: (customerRepository, configService) => {
                return new SequenceCifNoGenerator(customerRepository, configService);
            },
            inject: [CustomerRepository, ConfigService]
        },
        ...CommandHandlers
    ],
    exports: [
        {
            provide: 'CustomerService',
            useFactory: (customerRepository, customerQueryService, customerAccountService, customerBuilderRepository) => {
                return new CustomerServiceImpl(
                    customerRepository,
                    customerQueryService,
                    customerAccountService,
                    customerBuilderRepository
                );
            },
            inject: [CustomerRepository, 'CustomerQueryService', 'CustomerAccountService', 'CustomerBuilderRepository']
        },
        {
            provide: 'CustomerQueryService',
            useFactory: (customerRepository, branchService, blacklistRule, unblacklistRule, approveRule) => {
                return new CustomerQueryServiceImpl(
                    customerRepository,
                    branchService,
                    blacklistRule,
                    unblacklistRule,
                    approveRule
                );
            },
            inject: [CustomerRepository, 'BranchService', 'BlacklistRule', 'UnblacklistRule', 'ApproveRule']
        },
        {
            provide: 'CustomerAccountService',
            useFactory: (customerQueryService, accountService, balanceService, createAccountRule) => {
                return new CustomerAccountServiceImpl(
                    customerQueryService,
                    accountService,
                    balanceService,
                    createAccountRule
                );
            },
            inject: ['CustomerQueryService', 'AccountService', 'BalanceService', 'CreateAccountRule']
        }
    ]
})

export class CustomerModule implements NestModule{
    async configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(CustomerAuthMiddleware)
            .forRoutes(
                {
                    path: '/api/customer/:cifNo/accounts',
                    method: RequestMethod.GET
                }
            )
        consumer
            .apply(TellerAuthMiddleware)
            .exclude(
                {
                    path: '/api/customer/:cifNo/accounts',
                    method: RequestMethod.GET
                }
            )
            .forRoutes(CustomerController)
    }
    
}