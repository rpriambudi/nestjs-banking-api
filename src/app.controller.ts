import { Controller, Get, Query, Post, Body } from '@nestjs/common';
import { AppService } from './app.service';
import * as rp from 'request-promise';
import {  JWKS, JWT } from 'jose';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('/api/auth')
  async handleAuthRedirect(@Query('code') code: string) {
    const options = {
      method: 'POST',
      uri: 'http://192.168.99.100:8080/auth/realms/nest-bank-api/protocol/openid-connect/token',
      form: {
        grant_type: 'authorization_code',
        client_id: 'nest-bank-api',
        client_secret: '163ca589-c68d-43c9-b5c6-f694f074af5d',
        code: code
      }
    }
    try {
      const result = await rp(options);
      return result;
    } catch (error) {
      console.log(error);
    }
  }

  @Post('/api/auth/decode')
  async handleTokenDecode(@Body('token') token: string) {
    const options = {
      method: 'GET',
      uri: 'http://192.168.99.100:8080/auth/realms/nest-bank-api/protocol/openid-connect/certs'
    }
    const result = await rp(options);
    const keystore = JWKS.asKeyStore(JSON.parse(result));
    try {
      const verified = JWT.verify(token, keystore);
      console.log(verified);
    } catch (error) {
      console.log(error);
    }
  }
}
