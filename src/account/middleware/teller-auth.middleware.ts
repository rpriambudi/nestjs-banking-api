import { AuthStrategyMiddleware } from './../../auth/middleware/auth-strategy.middleware';

export class TellerAuthMiddleware extends AuthStrategyMiddleware {
    getStrategyName(): string {
        return 'JWT';
    }

    getGuards(): string[] {
        return ['Teller'];
    }
}