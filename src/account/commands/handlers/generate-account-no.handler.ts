import { CommandHandler, ICommandHandler, EventBus } from "@nestjs/cqrs";
import { Inject } from "@nestjs/common";
import { GenerateAccountNoCommand } from './../generate-account-no.command';
import { AccountNoGenerator } from './../../interfaces/account-no-generator.interface';
import { AccountNoGeneratedEvent } from './../../../user/events/account-no-generated.event';
import { RegistrationFailedEvent } from './../../../user/events/registration-failed.event';
import { ConfigService } from "@nestjs/config";

@CommandHandler(GenerateAccountNoCommand)
export class GenerateAccountNoHandler implements ICommandHandler<GenerateAccountNoCommand>{
    constructor(
        @Inject('AccountNoGenerator') private readonly accountNoGenerator: AccountNoGenerator,
        private readonly configService: ConfigService,
        private readonly eventBus: EventBus
    ) {}

    async execute(command: GenerateAccountNoCommand): Promise<any> {
        const { stateId } = command;
        const branchCode = this.configService.get<string>('branch.clientId');

        try {
            const accountNo = await this.accountNoGenerator.generateNewNumber(branchCode);
            this.eventBus.publish(new AccountNoGeneratedEvent(stateId, accountNo));
        } catch (error) {
            this.eventBus.publish(new RegistrationFailedEvent(stateId, error.message));
        }
    }
}