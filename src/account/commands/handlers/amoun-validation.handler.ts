import { CommandHandler, ICommandHandler, EventBus } from "@nestjs/cqrs";
import { Inject } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { AmountValidationCommand } from './../amount-validation.command';
import { Account } from './../../entities/account.entity';
import { CreateRuleContext } from './../../context/create-rule.context';
import { AccountRepository } from './../../repositories/account.repository';
import { CreateAccountDto } from "./../../dto/create-account.dto";
import { AmountValidationSuccessEvent } from './../../../user/events/amount-validation-success.event';

@CommandHandler(AmountValidationCommand)
export class AmountValidationHandler implements ICommandHandler<AmountValidationCommand> {
    constructor(
        @InjectRepository(Account) private readonly accountRepository: AccountRepository,
        @Inject('CreateRule') private readonly createAccountRule: CreateRuleContext,
        private readonly eventBus: EventBus
    ) {}

    async execute(command: AmountValidationCommand): Promise<any> {
        const { userRegistrationDto } = command;
        const account = this.accountRepository.create();

        try {
            await this.createAccountRule.validate(account);

            this.eventBus.publish(new AmountValidationSuccessEvent(userRegistrationDto));
            return;
        } catch(error) {
            this.eventBus.publish(null);
            return;
        }
    }

}