import { CommandHandler, ICommandHandler, EventBus } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { ConfigService } from '@nestjs/config';
import { CreateAccountCommand } from './../create-account.command';
import { Account } from './../../entities/account.entity';
import { StatusEnum } from './../../enums/status.enum';
import { AccountRepository } from './../../repositories/account.repository';
import { ClientTypeEnum } from './../../../user/enums/client-type.enum';
import { AccountGeneratedEvent } from './../../../user/events/account-generated.event';
import { RegistrationFailedEvent } from './../../../user/events/registration-failed.event';

@CommandHandler(CreateAccountCommand)
export class CreateAccountHandler implements ICommandHandler<CreateAccountCommand> {
    constructor(
        @InjectRepository(Account) private readonly accountRepository: AccountRepository,
        private readonly configService: ConfigService,
        private readonly eventBus: EventBus
    ) {}
    
    async execute(command: CreateAccountCommand): Promise<any> {
        const { userRegistrationDto, stateId } = command;
        const branchId = Number(this.configService.get<string>('branch.clientId'));

        try {
            const account = this.accountRepository.create();
            account.branchId = branchId;
            account.customerId = userRegistrationDto.customerId;
            account.accountNo = userRegistrationDto.accountNo;
            account.openedAt = ClientTypeEnum.InternetBanking;
            account.status = StatusEnum.Live;
            account.balance = 0;

            await this.accountRepository.save(account);
            this.eventBus.publish(new AccountGeneratedEvent(stateId));
        } catch (error) {
            this.eventBus.publish(new RegistrationFailedEvent(stateId, error.message));
        }
    }
    
}