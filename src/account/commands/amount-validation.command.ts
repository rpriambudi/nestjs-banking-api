import { UserRegistrationDto } from '../../user/dto/user-registration.dto';

export class AmountValidationCommand {
    constructor(
        public readonly userRegistrationDto: UserRegistrationDto
    ) {}
}