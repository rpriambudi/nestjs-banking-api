import { UserRegistrationDto } from './../../user/dto/user-registration.dto';

export class CreateAccountCommand {
    constructor(
        public readonly stateId: number,
        public readonly userRegistrationDto: UserRegistrationDto
    ) {}
}