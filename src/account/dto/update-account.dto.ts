import { IsNotEmpty, IsIn, validate } from 'class-validator';
import { StatusEnum } from './../enums/status.enum';
import { InputValidationException } from './../exceptions/input-validation.exception';
import { Account } from './../entities/account.entity';

export class UpdateAccountDto {
    constructor(updateDto: object) {
        Object.assign(this, updateDto);
        this.validateDto();
    }

    @IsNotEmpty()
    @IsIn(Object.values(StatusEnum))
    status: string;

    validateDto() {
        if (!this.status)
            throw new InputValidationException('Invalid account status');

        let exists = false;
        Object.values(StatusEnum).reduce((statusEnum, currentEnum) => {
            if (currentEnum === this.status)
                exists = true;
            
            return currentEnum;
        }, null)

        if (!exists)
            throw new InputValidationException('Invalid account status');
    }

    toAccountObject(account?: Account): Account {
        let accountObj = new Account();
        if (account)
            accountObj = account;

        for (const attribute of Object.keys(this)) {
            accountObj[attribute] = this[attribute];
        }
        return accountObj;
    }
}