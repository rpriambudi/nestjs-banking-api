export class CheckBalanceDto {
    private accountNo: string;
    private balance: number;

    constructor(accountNo: string, balance: number) {
        this.accountNo = accountNo;
        this.balance = balance;
    }
}