import { IsNotEmpty, IsPositive} from 'class-validator'

export class CreateAccountDto {
    @IsNotEmpty()
    @IsPositive()
    balance: number

    branchCode: string

    customerId: number
}