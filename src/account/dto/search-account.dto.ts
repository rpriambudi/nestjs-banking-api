export class SearchAccountDto {
    accountNo: string;

    branchCode: string;

    branchId: number;

    openedAt: string;

    status: string;
}