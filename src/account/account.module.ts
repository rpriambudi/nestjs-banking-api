import { Module, NestModule, MiddlewareConsumer, RequestMethod } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ConfigService } from '@nestjs/config'
import { CqrsModule } from '@nestjs/cqrs'
import { AccountController } from './controllers/account.controller'
import { AccountRepository } from './repositories/account.repository'
import { CreateRuleContext } from './context/create-rule.context'
import { AccountServiceImpl } from './services/account-impl.service'
import { BalanceServiceImpl } from './services/balance-impl.service'
import { SequenceAccountNoGeneratorImpl } from './services/sequence-account-no-generator.impl'
import { BlockedStatusRule, ClosedStatusRule, UnblockStatusRule } from './services/status-rule.service'
import { PositiveBalanceRule, MinimumBalanceRule } from './services/balance-rule.service'
import { AmountValidationHandler } from './commands/handlers/amoun-validation.handler'
import { GenerateAccountNoHandler } from './commands/handlers/generate-account-no.handler'
import { CreateAccountHandler } from './commands/handlers/create-account.handler'
import { CustomerAuthMiddleware } from './middleware/customer-auth.middleware'
import { TellerAuthMiddleware } from './middleware/teller-auth.middleware'
import { BranchModule } from './../branch/branch.module'
import { AuthModule } from './../auth/auth.module'

const CommandHandlers = [AmountValidationHandler, GenerateAccountNoHandler, CreateAccountHandler];

@Module({
    imports: [
        TypeOrmModule.forFeature([AccountRepository]),
        CqrsModule,
        BranchModule,
        AuthModule
    ],
    controllers: [AccountController],
    providers: [
        {
            provide: 'AccountService',
            useFactory: (accountRepository, branchService, createRule, blockRule, unblockRule, closeRule, accountNoGenerator) => {
                return new AccountServiceImpl(accountRepository, branchService, createRule, blockRule, unblockRule, closeRule, accountNoGenerator);
            },
            inject: [AccountRepository, 'BranchService', 'CreateRule', 'BlockRule', 'UnblockRule', 'CloseRule', 'AccountNoGenerator']
        },
        {
            provide: 'BalanceService',
            useFactory: (accountRepository, configService, accountService) => {
                const ruleValidation = new CreateRuleContext([new PositiveBalanceRule(), new MinimumBalanceRule(configService)]);
                return new BalanceServiceImpl(accountRepository, accountService, ruleValidation);
            },
            inject: [AccountRepository, ConfigService, 'AccountService']
        },
        {
            provide: 'CreateRule',
            useFactory: (configService) => {
                return new CreateRuleContext([new PositiveBalanceRule(), new MinimumBalanceRule(configService)]);
            },
            inject: [ConfigService]
        },
        {
            provide: 'BlockRule',
            useFactory: () => {
                return new CreateRuleContext([new BlockedStatusRule()]);
            }
        },
        {
            provide: 'UnblockRule',
            useFactory: () => {
                return new CreateRuleContext([new UnblockStatusRule()]);
            }
        },
        {
            provide: 'CloseRule',
            useFactory: () => {
                return new CreateRuleContext([new ClosedStatusRule()]);
            }
        },
        {
            provide: 'AccountNoGenerator',
            useFactory: (accountRepository, configService) => {
                return new SequenceAccountNoGeneratorImpl(accountRepository, configService);
            },
            inject: [AccountRepository, ConfigService]
        },
        ...CommandHandlers
    ],
    exports: [
        {
            provide: 'AccountService',
            useClass: AccountServiceImpl
        },
        {
            provide: 'BalanceService',
            useClass: BalanceServiceImpl
        }
    ]
})

export class AccountModule implements NestModule{
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(TellerAuthMiddleware)
            .exclude(
                { path: '/api/account/:accountNo/balance', method: RequestMethod.GET }
            )
            .forRoutes(AccountController)

        consumer
            .apply(CustomerAuthMiddleware)
            .forRoutes(
                {path: '/api/account/:accountNo/balance', method: RequestMethod.GET }
            )
    }
    
}