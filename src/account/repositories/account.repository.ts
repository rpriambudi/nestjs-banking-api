import { EntityRepository, Raw } from 'typeorm';
import { PostgresRepository } from './../../repositories/postgres.repository';
import { Account } from './../entities/account.entity';
import { SearchAccountDto } from './../dto/search-account.dto';

@EntityRepository(Account)
export class AccountRepository extends PostgresRepository<Account> {
    async searchQuery(queryParam: SearchAccountDto): Promise<Account[]> {
        const findOptions = {}
        for (const attribute of Object.keys(queryParam)) {
            if (attribute === 'branchId') {
                findOptions[attribute] = queryParam[attribute];
            } else {
                findOptions[attribute] = Raw(alias => {
                    return `lower(${alias}) like lower('%${queryParam[attribute]}%')`
                });
            }
        }
        return await this.find(findOptions);
    }
}