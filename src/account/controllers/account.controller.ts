import { Controller, Post, Inject, Param, Get, Query, Request } from '@nestjs/common';
import { AccountService } from './../interfaces/account-service.interface';
import { BalanceService } from './../interfaces/balance-service.interface';
import { CheckBalanceDto } from './../dto/check-balance.dto';
import { Account } from './../entities/account.entity';
import { SearchAccountDto } from './../dto/search-account.dto';
import { UserContext, AuthContext } from './../../auth/decorators/user-context.decorator';

@Controller()
export class AccountController {
    constructor(
        @Inject('AccountService') private readonly accountService: AccountService,
        @Inject('BalanceService') private readonly balanceService: BalanceService
    ) {}

    @Get('/api/account/:accountNo/balance')
    async requestAccountBalance(@Param('accountNo') accountNo: string) {
        const balanceData: CheckBalanceDto = await this.balanceService.checkBalanceByAccountNo(accountNo);
        return balanceData;
    }

    @Get('/api/account/search')
    async requestAccountSearch(
        @Query('accountNo') accountNo: string,
        @Query('openedAt') openedAt: string,
        @Query('status') status: string,
        @AuthContext() userContext: UserContext
    ) {
        const searchAccountDto = new SearchAccountDto();
        searchAccountDto.branchCode = userContext.branchCode;
        if (accountNo)
            searchAccountDto.accountNo = accountNo;

        if (openedAt)
            searchAccountDto.openedAt = openedAt;

        if (status)
            searchAccountDto.status = status;
        
        const accountData = await this.accountService.searchAccount(searchAccountDto);
        return accountData;
    }

    @Post('/api/account/:accountNo/block')
    async requestBlockAccount(@Param('accountNo') accountNo: string) {
        const accountData: Account = await this.accountService.blockAccount(accountNo);
        return accountData;
    }

    @Post('/api/account/:accountNo/unblock')
    async requestUnblockAccount(@Param('accountNo') accountNo: string) {
        const accountData: Account = await this.accountService.unblockAccount(accountNo);
        return accountData;
    }

    @Post('/api/account/:accountNo/close')
    async requestCloseAccount(@Param('accountNo') accountNo: string) {
        const accountData: Account = await this.accountService.closeAccount(accountNo);
        return accountData;
    }
}
