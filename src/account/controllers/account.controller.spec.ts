import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication, MiddlewareConsumer } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { getRepositoryToken, TypeOrmModule } from '@nestjs/typeorm';
import { AccountModule } from './../account.module';
import { Account } from './../entities/account.entity';
import { StatusEnum } from './../enums/status.enum';
import { CreateAccountDto } from './../dto/create-account.dto';
import { SearchAccountDto } from './../dto/search-account.dto';
import { AccountRepository } from './../repositories/account.repository';
import { AccountController } from './../controllers/account.controller';
import { MinimumBalanceException } from './../exceptions/minimum-balance.exception';
import { NegativeBalanceException } from './../exceptions/negative-balance.exception';
import { AuthStrategyMiddleware } from './../../auth/middleware/auth-strategy.middleware';
import { BranchModule } from './../../branch/branch.module';
import { Branch } from './../../branch/entities/branch.entity';
import { BranchServiceImpl } from './../../branch/services/branch-impl.service';
import { CustomFilterException } from './../../exceptions/custom-filter.exception';
import { BlockedStatusRule } from '../services/status-rule.service';
import { Repository } from 'typeorm';

export class MockAuthMiddleware extends AuthStrategyMiddleware {
    getStrategyName(): string {
        return 'Mock';
    }    
    
    getGuards(): string[] {
        return [];
    }

    runMockStrategy(request: any, guards: string[]) {
        const mockUserContext:object = {
            name: 'Test Branch',
            email: 'test@mail.co',
            branch_code: '001'
        }
        return mockUserContext;
    }
}

export class MockAccountModule extends AccountModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(MockAuthMiddleware)
            .forRoutes(AccountController)
    }
}


describe('AccountController class', () => {
    let app: INestApplication;
    let accountRepository: AccountRepository;
    let branchRepository: Repository<Branch>;

    beforeAll(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [
                TypeOrmModule.forRoot({
                    type: 'sqlite',
                    database: ':memory:',
                    dropSchema: true,
                    entities: [Branch, Account],
                    synchronize: true,
                    logging: false
                }),
                TypeOrmModule.forFeature([AccountRepository, Branch]),
                MockAccountModule, 
                BranchModule, 
                ConfigModule.forRoot({isGlobal: true})
            ]
        })
        .compile();

        app = moduleRef.createNestApplication();
        app.useGlobalFilters(new CustomFilterException());
        accountRepository = moduleRef.get<AccountRepository>(AccountRepository);
        branchRepository = moduleRef.get<Repository<Branch>>(getRepositoryToken(Branch));

        const accountData = new Account();
        accountData.id = 1;
        accountData.branchId = 1;
        accountData.customerId = 1;
        accountData.accountNo = '0010000000000000001';
        accountData.balance = 10000000;
        accountData.openedAt = 'Test Branch';
        accountData.status = StatusEnum.Live;
        await accountRepository.save(accountData);

        const accountData3 = new Account();
        accountData3.id = 3;
        accountData3.branchId = 1;
        accountData3.customerId = 1;
        accountData3.accountNo = '0010000000000000003';
        accountData3.balance = 10000000;
        accountData3.openedAt = 'Test Branch';
        accountData3.status = StatusEnum.Live;
        await accountRepository.save(accountData3);

        const accountData4 = new Account();
        accountData4.id = 4;
        accountData4.branchId = 1;
        accountData4.customerId = 1;
        accountData4.accountNo = '0010000000000000004';
        accountData4.balance = 10000000;
        accountData4.openedAt = 'Test Branch';
        accountData4.status = StatusEnum.Blocked;
        await accountRepository.save(accountData4);

        const accountData5 = new Account();
        accountData5.id = 5;
        accountData5.branchId = 1;
        accountData5.customerId = 1;
        accountData5.accountNo = '0010000000000000005';
        accountData5.balance = 10000000;
        accountData5.openedAt = 'Test Branch';
        accountData5.status = StatusEnum.Closed;
        await accountRepository.save(accountData5);

        const accountData6 = new Account();
        accountData6.id = 6;
        accountData6.branchId = 1;
        accountData6.customerId = 1;
        accountData6.accountNo = '0010000000000000006';
        accountData6.balance = 10000000;
        accountData6.openedAt = 'Test Branch';
        accountData6.status = StatusEnum.Live;
        await accountRepository.save(accountData6);

        const branch: Branch = new Branch();
        branch.id = 1;
        branch.branchCode = '001';
        branch.name = 'Test Branch';
        branch.address = 'Test Address';
        await branchRepository.save(branch);

        await app.init();
    });

    describe('GET /api/account/:accountNo/balance endpoint', () => {
        it('Positive test', () => {
            return request(app.getHttpServer())
                .get('/api/account/0010000000000000001/balance')
                .expect(200)
                .expect({
                    accountNo: '0010000000000000001',
                    balance: 10000000
                });
        })
        it('Negative test, account not found', () => {
            return request(app.getHttpServer())
                .get('/api/account/0010000000000000002/balance')
                .expect(400);
        })
    })

    describe('GET /api/account/search endpoint', () => {
        it('Positive test', () => {
            return request(app.getHttpServer())
                .get('/api/account/search?accountNo=006')
                .expect(200)
                .expect([
                    {
                        id: 6,
                        branchId: 1,
                        customerId: 1,
                        accountNo: '0010000000000000006',
                        balance: 10000000,
                        openedAt: 'Test Branch',
                        status: StatusEnum.Live
                    }
                ]);
        })
        it('Positive test, empty data', () => {
            return request(app.getHttpServer())
                .get('/api/account/search?accountNo=002')
                .expect(200)
                .expect([])
        })
    })

    describe('POST /api/account/:accountNo/block endpoint', () => {
        it('Positive test', () => {
            return request(app.getHttpServer())
                .post('/api/account/0010000000000000003/block')
                .expect(201)
                .expect((data: Account) => data.status === StatusEnum.Blocked)
        })
        it('Negative test, account already blocked', () => {
            return request(app.getHttpServer())
                .post('/api/account/0010000000000000004/block')
                .expect(400);
        })
    })

    describe('POST /api/account/:accountNo/unblock endpoint', () => {
        it('Positive test', () => {
            return request(app.getHttpServer())
                .post('/api/account/0010000000000000004/unblock')
                .expect(201)
                .expect((data: Account) => data.status === StatusEnum.Live);
        });
        it('Negative test, account is not blocked', () => {
            return request(app.getHttpServer())
                .post('/api/account/0010000000000000001/unblock')
                .expect(400);
        })
    })

    describe('POST /api/account/:accountNo/close endpoint', () => {
        it('Positive test', () => {
            return request(app.getHttpServer())
                .post('/api/account/0010000000000000006/close')
                .expect(201)
                .expect((data: Account) => data.status === StatusEnum.Closed);
        })
        it('Negative test, account already closed', () => {
            return request(app.getHttpServer())
                .post('/api/account/0010000000000000005/close')
                .expect(400);
        });
        it('Negative test, account is blocked', () => {
            return request(app.getHttpServer())
                .post('/api/account/0010000000000000003/close')
                .expect(400);
        })
    })

    afterAll(async () => {
        await app.close();
    });
})