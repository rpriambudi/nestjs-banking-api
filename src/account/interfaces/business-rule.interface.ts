import { Account } from "./../entities/account.entity";

export interface BusinessRule {
    validate(parameter: Account): void;
}