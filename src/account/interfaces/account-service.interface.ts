import { Account } from './../entities/account.entity';
import { CreateAccountDto } from "../dto/create-account.dto";
import { UpdateAccountDto } from './../dto/update-account.dto';
import { SearchAccountDto } from './../dto/search-account.dto';
import { CreateRuleContext } from './../context/create-rule.context';

export interface AccountService {
    createNewAccount(createAccountDto : CreateAccountDto): Promise<Account>;
    updateExistingAccount(accountNo: string, updateAccountDto: UpdateAccountDto): Promise<Account>;
    blockAccount(accountNo: string, ruleObj?: CreateRuleContext): Promise<Account>;
    unblockAccount(accountNo: string): Promise<Account>;
    closeAccount(accountNo: string): Promise<Account>;
    findAccountByAccountNo(accountNo: string): Promise<Account>;
    findAccountByCustomerId(customerId: number): Promise<Account[]>;
    searchAccount(queryParam: SearchAccountDto): Promise<Account[]>;
}