export enum StatusEnum {
    Live = 'LIVE',
    Closed = 'CLOSED',
    Blocked = 'BLOCKED'
}