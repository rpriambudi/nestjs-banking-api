import { BusinessRule } from "../interfaces/business-rule.interface";
import { Injectable } from "@nestjs/common";
import { Account } from './../entities/account.entity';
import { ClosedStatusRule, BlockedStatusRule } from "./../services/status-rule.service";

@Injectable()
export class CreateRuleContext implements BusinessRule {
    private ruleList: BusinessRule[];

    constructor(ruleList: BusinessRule[]) {
        this.ruleList = ruleList;
    }

    async validate(accountData: Account) {
        for (const ruleObj of this.ruleList) {
            await ruleObj.validate(accountData);
        }
    }
}