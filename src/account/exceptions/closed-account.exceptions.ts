import { GenericException } from './../../exceptions/generic-exception.abstract';

export class ClosedAccountException extends GenericException {
    getDisplayCode(): string {
        return 'CLOSED_ACCOUNT';
    }

    getErrorCode(): string {
        return '400305';
    }
    
    constructor(message: string) {
        super(message);
    }
}