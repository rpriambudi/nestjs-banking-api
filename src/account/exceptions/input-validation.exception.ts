import { GenericException } from './../../exceptions/generic-exception.abstract';

export class InputValidationException extends GenericException {
    getDisplayCode(): string {
        return 'INPUT_VALIDATION';
    }

    getErrorCode(): string {
        return '400304';
    }

    constructor(message: string) {
        super(message);
    }
}