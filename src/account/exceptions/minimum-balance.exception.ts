import { GenericException } from './../../exceptions/generic-exception.abstract';

export class MinimumBalanceException extends GenericException {
    getDisplayCode(): string {
        return 'MINIMUM_BALANCE';
    }

    getErrorCode(): string {
        return '400303';
    }
    
    constructor(message: string) {
        super(message);
    }
}