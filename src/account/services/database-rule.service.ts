import { Injectable, Inject } from '@nestjs/common';
import { BusinessRule } from './../interfaces/business-rule.interface';
import { Account } from './../entities/account.entity';
import { RuleService } from './../../rule/interfaces/rule-service.interface';
import { RuleTypeEnum } from './../../rule/enums/rule-type.enum';

@Injectable()
export class DatabaseRuleService implements BusinessRule {
    constructor(private readonly ruleService: RuleService) {}

    async validate(parameter: Account): Promise<void> {
        await this.ruleService.executeRules(RuleTypeEnum.AccountCreate, parameter);
    }
    
}