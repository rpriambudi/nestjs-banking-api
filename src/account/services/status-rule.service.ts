import { BusinessRule } from '../interfaces/business-rule.interface';
import { StatusEnum } from '../enums/status.enum';
import { Account } from '../entities/account.entity';
import { ClosedAccountException } from '../exceptions/closed-account.exceptions';
import { BlockedAccountException } from '../exceptions/blocked-account.exception';

export class ClosedStatusRule implements BusinessRule {
    validate(parameter: Account): void {
        if (parameter.status === StatusEnum.Closed)
            throw new ClosedAccountException('Operation is not permitted. Account is closed.');

        if (parameter.status === StatusEnum.Blocked)
            throw new BlockedAccountException('Operation is not permitted. Account is already blocked.');
    }
}

export class BlockedStatusRule implements BusinessRule {
    validate(parameter: Account): void {
        if (parameter.status === StatusEnum.Blocked)
            throw new BlockedAccountException('Operation is not permitted. Account is blocked.');

        if (parameter.status === StatusEnum.Closed)
            throw new ClosedAccountException('Operation is not permitted. Account already closed');
    }
}

export class UnblockStatusRule implements BusinessRule {
    validate(parameter: Account): void {
        if (parameter.status === StatusEnum.Live)
            throw new BlockedAccountException('Operation is not permitted. Account is not blocked.')

        if (parameter.status === StatusEnum.Closed)
            throw new ClosedAccountException('Operation is not permitted. Account is already closed.')
    }
}