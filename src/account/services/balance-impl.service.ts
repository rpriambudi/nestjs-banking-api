import { Inject } from '@nestjs/common';
import { Account } from './../entities/account.entity';
import { AccountRepository } from './../repositories/account.repository';
import { AccountService } from './../interfaces/account-service.interface';
import { BalanceService } from './../interfaces/balance-service.interface';
import { CheckBalanceDto } from './../dto/check-balance.dto';
import { CreateRuleContext } from './../context/create-rule.context';

export class BalanceServiceImpl implements BalanceService {
    constructor(
        @Inject('AccountRepository') private readonly accountRepository: AccountRepository,
        @Inject('AccountService') private readonly accountService: AccountService,
        private readonly ruleValidation: CreateRuleContext
    ) {}

    async checkBalanceByAccountNo(accountNo: string): Promise<CheckBalanceDto> {
        const accountData = await this.accountService.findAccountByAccountNo(accountNo);
        return new CheckBalanceDto(accountData.accountNo, Number(accountData.balance));
    }

    async checkBalanceByCustomerId(customerId: number): Promise<CheckBalanceDto[]> {
        const result: CheckBalanceDto[] = []
        const accountData = await this.accountService.findAccountByCustomerId(customerId);

        if (!accountData || accountData.length === 0)
            return [];

        for (const account of accountData) {
            result.push(new CheckBalanceDto(account.accountNo, Number(account.balance)));
        }
        return result;
    }

    async addAccountBalance(accountNo: string, amount: number): Promise<Account> {
        return await this.adjustAccountBalance(accountNo, amount, '+');
    }

    async substractAccountBalance(accountNo: string, amount: number): Promise<Account> {
        return await this.adjustAccountBalance(accountNo, amount, '-');
    }

    async updateAccountBalance(accountNo: string, amount: number): Promise<Account> {
        return await this.adjustAccountBalance(accountNo, amount, '=');
    }

    private async adjustAccountBalance(accountNo: string, amount: number, operand: string): Promise<Account> {
        const accountData = await this.accountService.findAccountByAccountNo(accountNo);
        const operandMapper = {
            '-': this.substractBalanceAmount,
            '+': this.addBalanceAmount,
            '=': this.updateBalanceAmount
        };

        accountData.balance = operandMapper[operand](amount, Number(accountData.balance));
        await this.ruleValidation.validate(accountData);
        await this.accountRepository.save(accountData);
        return accountData;
    }

    private substractBalanceAmount(amount: number, balance: number) {
        return balance - amount;
    }

    private addBalanceAmount(amount: number, balance: number) {
        return balance + amount;
    }

    private updateBalanceAmount(amount: number) {
        return amount;
    }
}