import { Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { ConfigService, ConfigModule } from '@nestjs/config';
import { AccountRepository } from './../repositories/account.repository';
import { Account } from '../entities/account.entity';
import { CreateAccountDto } from '../dto/create-account.dto';
import { AccountServiceImpl } from './../services/account-impl.service';
import { BranchService } from '../../branch/interfaces/branch-service.interface';
import { BranchServiceImpl } from '../../branch/services/branch-impl.service';
import { StatusEnum } from '../enums/status.enum';
import { NegativeBalanceException } from '../exceptions/negative-balance.exception';
import { Branch } from 'src/branch/entities/branch.entity';
import { AccountNotFoundException } from '../exceptions/account-not-found.exception';
import { CreateRuleContext } from './../context/create-rule.context';
import { MinimumBalanceException } from '../exceptions/minimum-balance.exception';
import { AccountNoGenerator } from './../interfaces/account-no-generator.interface';
import { SequenceAccountNoGeneratorImpl } from './sequence-account-no-generator.impl';
jest.mock('./../../branch/services/branch-impl.service');
jest.mock('./../context/create-rule.context');

describe('AccountService', () => {
    let accountService: AccountServiceImpl;
    let repository: AccountRepository;
    let branchService: BranchService;
    let validationRule: CreateRuleContext;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [ConfigModule],
            providers: [
                {
                    provide: 'AccountService',
                    useFactory: (accountRepository, branchService, validationRule, accountNoGenerator) => {
                        return new AccountServiceImpl(accountRepository, branchService, validationRule, validationRule, validationRule, validationRule, accountNoGenerator);
                    },
                    inject: [AccountRepository, 'BranchService', 'ValidationRule', 'AccountNoGenerator']
                },
                {
                    provide: getRepositoryToken(Account),
                    useClass: AccountRepository
                },
                {
                    provide: 'BranchService',
                    useClass: BranchServiceImpl
                },
                {
                    provide: 'ValidationRule',
                    useClass: CreateRuleContext
                },
                {
                    provide: 'AccountNoGenerator',
                    useFactory: (accountRepository, configService) => {
                        return new SequenceAccountNoGeneratorImpl(accountRepository, configService);
                    },
                    inject: [AccountRepository, ConfigService]
                }
            ]
        }).compile();
        
        accountService = moduleRef.get<AccountServiceImpl>('AccountService');
        repository = moduleRef.get<AccountRepository>(getRepositoryToken(Account));
        branchService = moduleRef.get<BranchService>('BranchService');
        validationRule = moduleRef.get<CreateRuleContext>('ValidationRule');
    })

    describe('create method', () => {
        it('positive test. should return one Account object.', async () => {
            const createAccountDto: CreateAccountDto = {
                balance: 500000,
                branchCode: '001',
                customerId: 1
            }

            const testAccount: Account = {
                id: 1,
                accountNo: '0100000000001',
                openedAt: 'KCP Thamrin',
                balance: 500000,
                status: StatusEnum.Live,
                branchId: 1,
                customerId: 1
            }

            const testBranch: Branch = {
                id: 1,
                branchCode: '001',
                name: 'Test Branch',
                address: 'Test Branch'
            }

            jest.spyOn(branchService, 'findBranchByCode').mockResolvedValueOnce(testBranch);
            jest.spyOn(validationRule, 'validate').mockImplementationOnce(async () => {
                return null;
            })
            jest.spyOn(repository, 'create').mockReturnValueOnce(testAccount);
            jest.spyOn(repository, 'save').mockResolvedValueOnce(testAccount);
            jest.spyOn(repository, 'getSequence').mockResolvedValueOnce('1');
            expect(await accountService.createNewAccount(createAccountDto)).toBe(testAccount);
        })

        it ('negative test, balance below than zero. should throw NegativeBalanceException', async () => {
            const createAccountDto: CreateAccountDto = {
                balance: -100000,
                branchCode: '001',
                customerId: 1
            }

            const testAccount: Account = {
                id: 1,
                accountNo: '0100000000001',
                openedAt: 'KCP Thamrin',
                balance: -1000000,
                status: StatusEnum.Live,
                branchId: 1,
                customerId: 1
            }

            jest.spyOn(validationRule, 'validate').mockImplementationOnce(async () => {
                throw new NegativeBalanceException('Balance insufficient. Balance is negative.');
            })
            jest.spyOn(repository, 'create').mockReturnValueOnce(testAccount);

            try {
                expect(await accountService.createNewAccount(createAccountDto)).toBeCalled();
            } catch (error) {
                expect(error instanceof NegativeBalanceException).toBe(true);
            }
        })

        it('Negative test, balance is not within rule range. should throw RuleExaminationException', async () => {
            const createAccountDto: CreateAccountDto = {
                balance: 150000,
                branchCode: '001',
                customerId: 1
            }

            const testAccount: Account = {
                id: 1,
                accountNo: '0100000000001',
                openedAt: 'KCP Thamrin',
                balance: 100000,
                status: StatusEnum.Live,
                branchId: 1,
                customerId: 1
            }

            jest.spyOn(validationRule, 'validate').mockImplementationOnce(async () => {
                throw new MinimumBalanceException('Balance insufficient. Balance is less than minimum balance.')
            })
            jest.spyOn(repository, 'create').mockReturnValueOnce(testAccount);

            try {
                expect(await accountService.createNewAccount(createAccountDto)).toBeCalled();
            } catch(error) {
                expect(error instanceof MinimumBalanceException).toBe(true);
            }
        })
    })

    describe('findByAccountNo method', () => {
        it('Positive test. should return one account data', async () => {
            const testAccount: Account = {
                id: 1,
                accountNo: '0100000000001',
                openedAt: 'KCP Thamrin',
                balance: 500000,
                status: StatusEnum.Live,
                branchId: 1,
                customerId: 1
            }

            jest.spyOn(repository, 'findOne').mockResolvedValueOnce(testAccount);
            expect(await accountService.findAccountByAccountNo('0100000000001')).toBe(testAccount);
        })

        it('Negative test, account no not found. Should catch AccountNotFoundException', async () => {
            jest.spyOn(repository, 'findOne').mockResolvedValueOnce(null);

            try {
                expect(await accountService.findAccountByAccountNo('0100000000002')).toBeCalled();
            } catch(error) {
                expect(error instanceof AccountNotFoundException).toBe(true);
            }
        })
    })
})