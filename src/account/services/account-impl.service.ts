import { Injectable } from '@nestjs/common'
import { CreateAccountDto } from '../dto/create-account.dto'
import { UpdateAccountDto } from './../dto/update-account.dto'
import { SearchAccountDto } from './../dto/search-account.dto'
import { Account } from '../entities/account.entity'
import { AccountService } from '../interfaces/account-service.interface'
import { BranchService } from './../../branch/interfaces/branch-service.interface'
import { AccountNotFoundException } from '../exceptions/account-not-found.exception'
import { AccountRepository } from './../repositories/account.repository'
import { BusinessRule } from '../interfaces/business-rule.interface'
import { AccountNoGenerator } from './../interfaces/account-no-generator.interface';
import { StatusEnum } from '../enums/status.enum'

@Injectable()
export class AccountServiceImpl implements AccountService {

    constructor(
        private readonly accountRepository : AccountRepository,
        private readonly branchService: BranchService,
        private readonly createRule: BusinessRule,
        private readonly blockRule: BusinessRule,
        private readonly unblockRule: BusinessRule,
        private readonly closeRule: BusinessRule,
        private readonly accountNoGenerator: AccountNoGenerator
    ) {}

    async createNewAccount(createAccountDto : CreateAccountDto): Promise<Account> {
        const branchData = await this.branchService.findBranchByCode(createAccountDto.branchCode);
        const accountObj = this.accountRepository.create(createAccountDto);
        await this.createRule.validate(accountObj);

        accountObj.accountNo = await this.accountNoGenerator.generateNewNumber(createAccountDto.branchCode);
        accountObj.branchId = branchData.id;
        accountObj.openedAt = branchData.name;

        return await this.accountRepository.save(accountObj);
    }

    async updateExistingAccount(accountNo: string, updateAccountDto: UpdateAccountDto): Promise<Account> {
        let accountData = await this.findAccountByAccountNo(accountNo);
        await this.createRule.validate(accountData);
        accountData = updateAccountDto.toAccountObject(accountData);

        await this.accountRepository.update(accountData.id, updateAccountDto);

        accountData = await this.findAccountByAccountNo(accountNo);
        return accountData;
    }

    async blockAccount(accountNo: string): Promise<Account> {
        const accountData = await this.findAccountByAccountNo(accountNo);
        await this.blockRule.validate(accountData);

        accountData.status = StatusEnum.Blocked;
        await this.accountRepository.save(accountData);
        return accountData;
    }

    async unblockAccount(accountNo: string): Promise<Account> {
        const accountData = await this.findAccountByAccountNo(accountNo);
        await this.unblockRule.validate(accountData);

        accountData.status = StatusEnum.Live;
        await this.accountRepository.save(accountData);
        return accountData;
    }
    
    async closeAccount(accountNo: string): Promise<Account> {
        const accountData = await this.findAccountByAccountNo(accountNo);
        await this.closeRule.validate(accountData);

        accountData.status = StatusEnum.Closed;
        await this.accountRepository.save(accountData);
        return accountData;
    }

    async findAccountByAccountNo(accountNo: string): Promise<Account> {
        const accountData = await this.accountRepository.findOne({ accountNo: accountNo });
        if (!accountData)
            throw new AccountNotFoundException('Account not found. Account No: ' + accountNo);
        
        return accountData;
    }

    async findAccountByCustomerId(customerId: number): Promise<Account[]> {
        const accountData = await this.accountRepository.find({ customerId: customerId });
        if (!accountData)
            return []
        return accountData;
    }

    async searchAccount(queryParam: SearchAccountDto): Promise<Account[]> {
        if (queryParam.branchCode) {
            const branchData = await this.branchService.findBranchByCode(queryParam['branchCode']);
            queryParam.branchId = branchData.id;
            delete queryParam.branchCode;
        }

        return await this.accountRepository.searchQuery(queryParam);
    }
}