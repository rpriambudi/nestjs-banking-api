import { Injectable, Inject } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { BusinessRule } from '../interfaces/business-rule.interface';
import { NegativeBalanceException } from '../exceptions/negative-balance.exception';
import { MinimumBalanceException } from './../exceptions/minimum-balance.exception';
import { Account } from './../entities/account.entity';

@Injectable()
export class PositiveBalanceRule implements BusinessRule {
    public validate(accountData: Account): void {
        if (accountData.balance <= 0)
            throw new NegativeBalanceException('Balance insufficient. Balance is negative.');
    }
}

@Injectable()
export class MinimumBalanceRule implements BusinessRule {
    constructor(
        @Inject('ConfigService') private readonly configService: ConfigService
    ) {}

    public validate(accountData: Account): void {
        const minimumBalance = this.configService.get<number>('account.minimum');
        if (accountData.balance < minimumBalance)
            throw new MinimumBalanceException('Balance insufficient. Less than minimum balance');
    }
}