import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe, HttpException, HttpStatus } from '@nestjs/common';
import { ValidationError } from 'class-validator';
import { CustomFilterException } from './exceptions/custom-filter.exception';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe({
    exceptionFactory: (errors: ValidationError[]) => {
      const error = errors[0]
      const constraints = Object.values(error.constraints)[0]
      return new HttpException('Input not valid. ' + constraints, HttpStatus.UNPROCESSABLE_ENTITY);
    }
  }));
  app.useGlobalFilters(new CustomFilterException());
  await app.listen(3000);
}
bootstrap();
